---
categories: SpringCloud
tags: [微服务,Netflix]
date: 2021-11-01 13:37:28
---

# 生产消费-父项目配置

1. 新建一个纯净的Maven的项目springcloud，并删除原先的src，需要自己创建module当做子项目

2. 设置总项目pom文件

   <!--more-->
   
   ~~~xml
   <!--打包方式  pom-->
       <packaging>pom</packaging>
   
       <properties>
           <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
           <maven.compiler.source>1.8</maven.compiler.source>
           <maven.compiler.target>1.8</maven.compiler.target>
           <junit.version>4.13</junit.version>
           <lombok.version>1.18.12</lombok.version>
           <log4j.version>1.2.17</log4j.version>
       </properties>
   <!--dependencyManagement:父项目管理容器,子项目可以直接从里面引用-->
   	<dependencyManagement>
       <dependencies>
           <!--SpringCloud的依赖-->
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-dependencies</artifactId>
               <version>Hoxton.SR3</version>
               <type>pom</type>
               <scope>import</scope>
           </dependency>
   
           <!--SpringBoot的依赖-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-dependencies</artifactId>
               <version>2.2.5.RELEASE</version>
               <type>pom</type>
               <scope>import</scope>
           </dependency>
   
           <!--数据库-->
           <dependency>
               <groupId>mysql</groupId>
               <artifactId>mysql-connector-java</artifactId>
               <version>5.1.47</version>
           </dependency>
   
           <!--druid-->
           <dependency>
               <groupId>com.alibaba</groupId>
               <artifactId>druid</artifactId>
               <version>1.1.10</version>
           </dependency>
   
           <!--MyBatis-->
           <dependency>
               <groupId>org.mybatis.spring.boot</groupId>
               <artifactId>mybatis-spring-boot-starter</artifactId>
               <version>2.1.1</version>
           </dependency>
   
           <!--Lombok-->
           <dependency>
               <groupId>org.projectlombok</groupId>
               <artifactId>lombok</artifactId>
               <version>${lombok.version}</version>
           </dependency>
   
           <!--日志和测试的一些包-->
           <!--junit-->
           <dependency>
               <groupId>junit</groupId>
               <artifactId>junit</artifactId>
               <version>${junit.version}</version>
           </dependency>
           <!--Log4j-->
           <dependency>
               <groupId>log4j</groupId>
               <artifactId>log4j</artifactId>
               <version>${log4j.version}</version>
           </dependency>
           <dependency>
               <groupId>ch.qos.logback</groupId>
               <artifactId>logback-core</artifactId>
               <version>1.2.3</version>
           </dependency>
       </dependencies>
   </dependencyManagement>
   ~~~



# 子项目api

1. 新建纯净的module：springcloud-api

2. 设置pom文件

   ~~~xml
    <!--当前module自己需要的依赖，如果父依赖已经配置版本，这里版本就不用写-->
       <dependencies>
           <dependency>
               <groupId>org.projectlombok</groupId>
               <artifactId>lombok</artifactId>
           </dependency>
       </dependencies>
   ~~~

   

3. 写一个Dept实体类：

   ~~~java
   /**
    * 第一步：必须实现Serializable序列化
    * 第二步：写上表对应的字段
    * 第三步：创建dname构造器，因为id主键自增，最后一个数据库函数自动生成
    * 第四步：加上lombok注解
    */
   @Data
   @NoArgsConstructor
   @Accessors(chain = true)//支持链式写法，默认为false
   public class Dept implements Serializable {
   
       private Long deptno;//主键
       private String dname;
       //这个数据存在哪个数据库的字段，
       // 微服务：一个服务对应一个数据库，同一个信息可能存在不同的数据库
       private String db_source;
   
   
       public Dept(String dname) {
           this.dname = dname;
       }
   }
   
   ~~~

   这个api就写完了，微服务这个api就管理实体类



# 子项目provider

1. 新建一个纯净module：**springcloud-provider-dept-8001**，一般带上端口号命名

2. 配置pom文件

   ~~~xml
    <dependencies>
           <!--我们要拿到实体类，所以要配置api module-->
           <dependency>
               <groupId>com.zhb</groupId>
               <artifactId>springcloud-api</artifactId>
               <version>1.0-SNAPSHOT</version>
           </dependency>
           <!--junit-->
           <dependency>
               <groupId>junit</groupId>
               <artifactId>junit</artifactId>
           </dependency>
           <!--Mysql-->
           <dependency>
               <groupId>mysql</groupId>
               <artifactId>mysql-connector-java</artifactId>
           </dependency>
           <!--Druid-->
           <dependency>
               <groupId>com.alibaba</groupId>
               <artifactId>druid</artifactId>
           </dependency>
           <!--Mybatis-->
           <dependency>
               <groupId>org.mybatis.spring.boot</groupId>
               <artifactId>mybatis-spring-boot-starter</artifactId>
           </dependency>
           <dependency>
               <groupId>ch.qos.logback</groupId>
               <artifactId>logback-core</artifactId>
           </dependency>
           <!--test-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-test</artifactId>
           </dependency>
           <!--web-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-web</artifactId>
           </dependency>
           <!--jetty-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-jetty</artifactId>
           </dependency>
           <!--热部署-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-devtools</artifactId>
           </dependency>
       </dependencies>
   
   ~~~

3. 配置application.yaml

   ~~~yaml
   server:
     port: 8001
   #mybatis配置
   mybatis:
     type-aliases-package: com.zhb.springcloud.pojo
     config-location: classpath:mybatis/mybatis-config.xml
     mapper-locations: classpath:mybatis/mapper/*.xml
   
   #spring的配置
   spring:
     application:
       name: springcloud-provider-dept
     datasource:
       type: com.alibaba.druid.pool.DruidDataSource
       driver-class-name: org.gjt.mm.mysql.Driver
       url: jdbc:mysql://localhost:3306/DB01?serverTimezone=UTC&useUnicode=true&characterEncodeing=UTF-8&useSSL=false
       username: root
       password: zhb955926
   
   ~~~

   

4. 配置mybatis-config

   ~~~xml
   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE configuration
           PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
           "http://mybatis.org/dtd/mybatis-3-config.dtd">
   <configuration>
       <settings>
           <setting name="cacheEnabled" value="true"/>
       </settings>
   </configuration>
   ~~~

   

5. 编写正常service，controller，启动类

6. 启动测试



# 子项目Consumer

1. 新建module：**springcloud-consumer-dept-80**

2. yaml只需配置端口号80

3. 因为需要调用远程service所以需要配置一个配置类ConfigBean配置RestTemplate

   ~~~java
   @Configuration
   public class ConfigBean {
   
       @Bean
       public RestTemplate restTemplate(){
           return new RestTemplate();
       }
   }
   
   ~~~

4. controller：

   ~~~java
   @RestController
   public class DeptConsumerController {
   
       //消费者不应该有service层
       //RestTemplate ....供我们直接调用即可
       //(url,map,class<T>,responseType)
       @Autowired
       private RestTemplate restTemplate;
   
       private static final String REST_URL_PREFIX="http://localhost:8001";
   
       @RequestMapping("/consumer/dept/get/{id}")
       public Dept get(@PathVariable("id")Long id){
           return restTemplate.getForObject(REST_URL_PREFIX+"/dept/get/"+id,Dept.class);
   
       }
   
       @RequestMapping("/consumer/dept/add")
       public boolean add( Dept dept){
           return restTemplate.postForObject(REST_URL_PREFIX+"/dept/add",dept,boolean.class);
   
       }
       @RequestMapping("/consumer/dept/list")
       public List getAll(){
           return restTemplate.getForObject(REST_URL_PREFIX+"/dept/list", List.class);
   
       }
   }
   ~~~

   

5. 编写启动类测试：先启动提供者，再启动消费者，输入消费者的请求url测试





# Eureka服务注册与发现

## 什么是Eureka

NetFlix在设计Eureka时，遵循的就是AP原则

Eureka是NetFlix的一个子模块，也是核心模块之一。Eureka是一个基于REST的服务，用于定位服务，以实现云端中间层服务发现和故障转移，服务注册与发现对于微服务来说非常重要，有了服务与注册，只需要使用服务的标识符，就可以访问到服务，而不需要修改服务调用的配置文件了，类似于dubbo的注册中心，比如Zookeeper。



## EurekaService

这个就是类似zookeeper的客户端，用来注册与发现的，zookeeper是下载过来启动即可，而EurekaService是自己配置写的服务





# 项目进阶-使用Eureka

1. 新建module：**springcloud-eureka-7001**

2. 导入eureka依赖

   ~~~xml
    <dependencies>
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
               <version>2.2.2.RELEASE</version>
           </dependency>
   
           <!--热部署-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-devtools</artifactId>
           </dependency>
       </dependencies>
   
   ~~~

   

3. 配置application.yaml

   ~~~yaml
   server:
     port: 7001
   
   #Eureka配置
   eureka:
     instance:
       hostname: localhost #服务端实例名称
     client:
       #表示是否向Eureka注册中心注册自己，默认true，因为现在是在写Eureka的服务器，不需要注册自己
       register-with-eureka: false
       fetch-registry: false #默认true，若为false表示自己为注册中心
       service-url: #监控页面地址，动态配置：http://localhost:7001/eureka/
         defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka/
   ~~~

   

4. 编写启动类，启动后进入Eureka界面  `localhost:7001/`

![image-20200314162513955](1.png)

至此，Eureka的服务端配置完毕





# 为之前的服务提供者注册服务

1. 会到提供者的pom文件添加依赖

   ~~~xml
    <!--Eureka客户端依赖-->
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
               <version>2.2.2.RELEASE</version>
           </dependency>
      <!--完善监控信息，Eureka描述信息跳转的页面-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-actuator</artifactId>
           </dependency>
   
   ~~~

   

2. 在application.yaml里配置

   ~~~yaml
   #Eureka的配置，注册服务到哪
   eureka:
     client:
       service-url:
         defaultZone: http://${eureka.instance.hostname}:7001/eureka/
     instance:
       hostname: localhost
       instance-id: springcloud-procider-8001 #描述信息
   #info信息,描述信息跳转后显示的信息
   info:
     app.name: zhb-springcloud
     company.name: www.baidu.com
   ~~~

   

3. 在启动类加上注解：`@EnableEurekaClient`，在服务启动后自动注册到服务端

4. 测试：先启动服务端，在启动客户端

   ![image-20200314164903699](2.png)

![image-20200314165726154](3.png)

进入后就可以进入查看你配置的info信息

![image-20200314165816324](4.png)

或者可以专门写一个方法获取DiscoveryClient：

（要想使用这个功能，必须在**启动类**加个注解`@EnableDiscoveryClient`）

~~~java
 //获取一些配置的信息
    private DiscoveryClient client;

//获取注册进来的微服务信息
    @GetMapping("/dept/discovery")
    public Object discovery(){
        //获取微服务列表的清单
        List<String> services = client.getServices();
        System.out.println("discovery==>"+services);
        //通过具体微服务名字：applicationName 得到一个具体的微服务信息
        List<ServiceInstance> instances = client.getInstances("SPRINGCLOUD-PRIVIDER-DEPT");
        for (ServiceInstance instance : instances) {
            System.out.println(
                    instance.getHost()+"/t"+
                    instance.getPort()+"/t"+
                    instance.getUri()+"/t"+
                    instance.getServiceId()
            );
        }
        return this.client;
    }
~~~







# 配置集群

1. 先再新建两个module导入eureka的服务依赖

2. 把7001中的application.yaml和启动类复制到两个module里，并修改相应的端口改成7002和7003

3. 修改三个yaml文件使三个服务互相绑定，7001绑定7002和7003，再依次给他们相互绑定，令他们互相依赖

   ~~~yaml
   #Eureka配置
   eureka:
     instance:
       hostname: localhost #服务端实例名称
     client:
       #表示是否向Eureka注册中心注册自己，默认true，因为现在是在写Eureka的服务器，不需要注册自己
       register-with-eureka: false
       fetch-registry: false  #默认true，若为false表示自己为注册中心
       service-url: #监控页面
         #单个Eureka服务配置自己的端口名称
         #集群只要用逗号把他们都关联起来，不用写自己的端口，因为他们互相写了
         defaultZone: http://localhost:7002/eureka/,http://localhost:7003/eureka/
         
   ~~~

   在8001端口服务提供者的配置文件中`defaultZone:`中需要把三个端口都写上去，可以注册到多个服务上





# springcloud - ribbon

**ribbon是什么？**

ribbon是基于NetFlix Ribbon实现的一套客户端**负载均衡工具**



**ribbon能干吗？**

- LB，即负载均衡（Load Balance），在微服务或分布式集群中经常用的一种应用
- 负载均衡简单的说就是将用户的请求平摊的分配到多个服务器上，从而达到系统的HA（高可用）
- 常见的负载均衡有Nginx，Lvs等等
- dubbo，springcloud中均给我提供了负载均衡，**springcloud的负载均衡算法可以自定义**
- 负载均衡简单分类：
  - 集中式LB
    - 即在服务的消费方和提供方之间使用独立的LB设施，如Nginx，由该设施负责把访问请求通过某种策略转发至服务的提供方！
  - 进程式LB
    - 将LB逻辑集成到消费方，消费方从服务注册中心获知有哪些地址可用，然后自己再从这些地址选出一个合适的服务器
    - Ribbon就属于进程内LB，它只是一个类库，集成于消费方进程，消费方通过它来获取带服务的提供方的地址！

**使用ribbon**

1. 添加依赖

   ~~~xml
     <!--Ribbon-->
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-starter-ribbon</artifactId>
               <version>1.4.7.RELEASE</version>
           </dependency>
   
           <!--Eureka客户端依赖-->
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
               <version>2.2.2.RELEASE</version>
           </dependency>
   ~~~

   

2. 配置80端口客户端

   ~~~yaml
   #eureka配置
   eureka:
     client:
       register-with-eureka: false #不向Eureka注册自己
       service-url:
       #写上三个地址，服务就可以去任意地方查询使用，简化了controller里地址的写法
         defaultZone: http://localhost:7001/eureka/,http://localhost:7002/eureka/,http://localhost:7003/eureka/
   
   ~~~

   

3. 加注解和修改地址写法

   ConfigBean.class

   ![image-20200315190101089](5.png)

   DeptConsumerController.class

   ![image-20200315190156767](6.png)

   

   

# 修改Ribbon的默认算法

**在==com.netflix.loadbalancer;==下有一个重要的类：==IRule==   路由网关**

他有很多实现类，都是负载均衡的各种算法：

![image-20200315200458801](7.png)

AvailabilityFilteringRule()：会先过滤掉的跳闸的服务所以访问可能慢，对剩下的服务进行轮询，本质还是轮询

RandomRule()：随机

RoundRobinRule()：轮询，默认的算法

WeightedResponseTimeRule()：根据权重值进行分配

RetryRule()：重试，会先按照轮询，如果获取失败，会在指定的时间内进行重试

这些方法都会继承第一个**AbstractLoadBalancerRule()**这个抽象类，所以自己写算法的话也需要继承这个类



**注意：写自己的Rule类时不能写在application同级目录下，不能被@SpringBootApplication扫描到，所以向上退一集，再添加@RibbonClient()注解加载即可**

~~~java
package com.zhb.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZHBRule {


    @Bean
    public IRule myRule(){
      //可以自己再写一个类，编写自己的算法，并这里进行new再返回（现在返回系统直接写好的随机算法）
        return new RandomRule();
    }

}


//启动类需要加一个注解
//加载自定义的Ribbon类
@RibbonClient(name="springcloud-provider-dept",configuration = ZHBRule.class)
public class DeptConsumer_80 {

    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer_80.class,args);
    }
}

~~~



还有一种负载均衡的方式：**Feign**

feign是面对接口和注解的一种方式，加大了代码的可读性，但是加了一层，那么性能会有所降低

feign因为这个是采用面向接口，跟以前的写法很像，只是把service接口写在了别的module下，所以不需要restTemplate。

1. 新建module，复制80端口的内容及依赖，可以删除ribbon的东西

2. 添加feign依赖

   ~~~xml
   <!--feign-->
   <dependency>
     <groupId>org.springframework.cloud</groupId>
     <artifactId>spring-cloud-starter-feign</artifactId>
     <version>1.4.7.RELEASE</version>
   </dependency>
   ~~~

3. 在**springcloud-api**也添加feign依赖，并添加一个service类,新建一个service接口，以供其他module调用，需要为该接口添加注解@FeignClient()（该注解会自动把该接口交给spring bean管理）

   ~~~java
   //value为服务名称即服务提供者yaml文件配置的spring application name
   @FeignClient(value = "springcloud-provider-dept")
   public interface DeptClientService {
   
       @GetMapping("/dept/get/{id}")
       public Dept queryById(@PathVariable("id") Long id);
       
     	@GetMapping("/dept/list")
       public List<Dept> queryAll();
      
     	@PostMapping("/dept/add")
       public boolean addDept(Dept dept);
   
   }
   
   ~~~

4. 接着为客户端写controller即可，因为采用接口写法，那controller跟以前的写法相同

   ~~~java
   @RestController
   public class DeptConsumerController {
   
   
   
       @Autowired
       private DeptClientFeignService feignService;
   
   
       @RequestMapping("/consumer/dept/get/{id}")
       public Dept get(@PathVariable("id")Long id){
           return feignService.queryById(id);
       }
   
       @RequestMapping("/consumer/dept/add")
       public boolean add( Dept dept){
           return feignService.addDept(dept);
       }
       @RequestMapping("/consumer/dept/list")
       public List getAll(){
           return feignService.queryAll();
       }
   }
   
   ~~~

   

5. 最后在启动类上加上注解：`@EnableFeignClients(basePackages = {"com.zhb.springcloud"})`扫描刚刚写得服务即可。



其实Feign本质还是集成Ribbon，只是改写开发方式，改回采用接口调用，这样写会更符合以前的java开发。





# 服务熔断

**分布式系统面临的问题**

复杂的分布式体系结构中的应用程序有数十个依赖关系，每个依赖关系在某些时候将不可免=避免失败！



**服务雪崩**

![image-20200316134932828](8.png)



**什么是Hystrix**

Hystrix是一个用于处理分布式系统的延迟和容错的开源库，在分布式系统里，许多依赖不可避免的会调用失败，比如超时，异常等，Hystrix能够保证在一个依赖出现问题的情况下，不会导致整个服务失败，避免级联故障，以提高分布式系统的弹性。

Hystrix就是服务熔断的一种方法



实例

1. 新建module，复制消费者的相关代码，进行微调，端口改为8004

2. 添加依赖

   ~~~xml
    <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-starter-hystrix</artifactId>
               <version>1.4.7.RELEASE</version>
           </dependency>
   ~~~

   

3. 编写Controller

   ~~~java
   @RestController
   public class DeptController {
   
       @Autowired
       private DeptService deptService;
   
   
       @GetMapping("/dept/get/{id}")
     	//有异常会跳转到错误就会执行的方法
       @HystrixCommand(fallbackMethod = "getHystrix")
       public Dept queryDeptById(@PathVariable("id") Long id){
           Dept dept = deptService.queryById(id);
           if (dept!=null){
               return dept;
           }else {
             //需要抛出异常才会执行到错误方法，若无异常报错贼不会跳转
               throw  new RuntimeException("Fail");
           }
       }
   
       //要是上面这个方法发生错误就会跳到该方法，而不会抛出异常
       public Dept getHystrix(@PathVariable("id") Long id){
           return new Dept()
                   .setDeptno(id)
                   .setDname("无此用户")
                   .setDb_source("no this database in MySQL");
       }
   
   
   }
   ~~~

   

4. 在启动类添加开启断路由注解`@EnableCircuitBreaker `，开启断路由后就可以使用hystrix

# 服务降级

服务降级是针对客户端的一种操作，服务熔断是针对方法的（即该服务器下的单个服务），而服务降级是针对整个服务器崩掉而调用的，所以针对整个类所有方法的（即改服务器下的所有服务），主要是实现FallbackFactory()接口，该接口就是在**feigin.hystrix**包下的，所以想要用这个就是需要采用接口编程 使用feign实现负载均衡，实现接口后，重写方法，方法里返回service类名称即可，使用匿名内部类重写业务方法，返回错误信息（错误信息跟熔断方法类似）。



# Hystrix：Dashboard流监控

1. 新建module：springcloud-consumer-hystrix-dashboard

2. 导入80端口consumer的相同依赖并增加一些

   ~~~xml
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-starter-hystrix</artifactId>
               <version>1.4.7.RELEASE</version>
           </dependency>
   
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-starter-hystrix-dashboard</artifactId>
               <version>1.4.7.RELEASE</version>
           </dependency>
   
   
   ~~~

   

3. 配置yaml配置文件

   ~~~yaml
   server:
     port: 9001
   eureka:
     client:
       register-with-eureka: false
       fetch-registry: false
       serviceUrl:
         defaultZone: http://localhost:9001/eureka
   ~~~

   

4. 创建启动类

   ~~~java
   @SpringBootApplication
   @EnableHystrixDashboard //开启监控界面
   public class DeptConsumerDashborad_9001 {
   
       public static void main(String[] args) {
           SpringApplication.run(DeptConsumerDashborad_9001.class,args);
       }
   }
   ~~~

5. 注意：生产者必须提供监控的jar包

   ~~~xml
   <!--完善监控信息，Eureka描述信息跳转的页面-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-actuator</artifactId>
           </dependency>
   ~~~

6. 输入网址  ：localhost:9001/hystrix，图片是只豪猪，它的学名就是hystrix

   ![image-20200316205451213](9.png)

7. 想要监控具体的服务需要向服务器导入Hystrix依赖，并向spring注册一个bean，可以卸载启动类里

   

   **注意（大坑）：要想监控界面进的去，服务必须是支持熔段的，启动类也要加上熔断支持**
   
   ~~~java
   @SpringBootApplication
   @EnableEurekaClient    //开启Eureka客户端
   @EnableDiscoveryClient //开启获取服务信息的功能
   @EnableCircuitBreaker //添加对熔断的支持
   public class DeptProvider_8001 {
   
       public static void main(String[] args) {
           SpringApplication.run(DeptProvider_8001.class,args);
       }
   //固定代码
       @Bean
       public ServletRegistrationBean hystrixMetricsStreamServlet(){
           ServletRegistrationBean registrationBean = new ServletRegistrationBean(new HystrixMetricsStreamServlet());
           registrationBean.addUrlMappings("/actuator/hystrix.stream");
           return registrationBean;
       }
   }
   ~~~
   
   ~~~java
   @GetMapping("/dept/get/{id}")
   @HystrixCommand(fallbackMethod = "getID")//熔断返回抛出异常跳到自己写的方法
   public Dept queryDeptById(@PathVariable("id") Long id){
     Dept dept = deptService.queryById(id);
     if (dept==null){
       throw new RuntimeException("用户不存在");
     }
     return dept;
   }
   //抛出异常后返回的方法
   public Dept getID(@PathVariable("id") Long id){
     return new Dept().setDname("用户不存在");
   }
   ~~~

   **输入的监控地址为：`http://localhost:port/actuator/hystrix.stream`**
   
   



# 网关Zuul

1. 导入依赖

   ~~~xml
   <dependency>
     <groupId>org.springframework.cloud</groupId>
     <artifactId>spring-cloud-starter-zuul</artifactId>
     <version>1.4.7.RELEASE</version>
   </dependency>
   ~~~

   

2. 配置yaml

   ~~~yaml
   server:
     port: 9527
   
   spring:
     application:
       name: springcloud-zuul
   eureka:
     client:
       service-url:
         defaultZone: http://localhost:7001/eureka/,http://localhost:7002/eureka/,http://localhost:7003/eureka/
     instance:
       instance-id: zuul9527.com
   ~~~

   

3. 添加注解启动

   ~~~java
   @SpringBootApplication
   @EnableZuulProxy //开启网关代理
   public class ZuulApplication_9527 {
   
   
       public static void main(String[] args) {
   
           SpringApplication.run(ZuulApplication_9527.class,args);
       }
   }
   ~~~

4. 测试

   可以修改电脑hosts文件，模拟一个假的地址，比如在里面添加一行：**127.0.0.1      zhb.com**

这样访问zhb.com就是访问本地。

这样测试网关时就可以输入 zhb.com:9527/springcloud-provider-dept/dept/list  隐藏了真的端口

springcloud-provider-dept是服务名真实中我们也应该隐藏掉，所以去yaml中配置zuul

~~~yaml
zuul:
  routes:
    mydept.serverId: springcloud-provider-dept
    mydept.path: /mydept/**  #把springcloud-provider-dept改成mydept
  ignored-services: springcloud-provider-dept  #关闭这个路径的访问  
  #这里还可以写"*"代表通过所有微服务名称访问的路径都关闭，这个参数要带双引号
  prefix: /zhb  #设置公共的前缀
~~~



# 连接Git仓库配置服务端

1. 主要的依赖

   ~~~xml
   <dependencies>
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-config-server</artifactId>
               <version>2.2.2.RELEASE</version>
           </dependency>      
       </dependencies>
   ~~~

2. 主要的配置文件

   ~~~yaml
   server:
     port: 9002
   
   spring:
     application:
       name: springcloud-config-server
       #连接远程仓库
     cloud:
       config:
         server:
           git:
             uri: https://gitee.com/zhb520/springcloud-config.git #仓库https的路径
   ~~~

   

3. 启动类添加注解`@EnableConfigServer`

4. 访问路径：localhost:9002/application-dev.yml，一共以下几种访问方式

   ~~~test
   /{application}/{profile}[/{label}]  
   /{application}-{profile}.yml
   /{label}/{application}-{profile}.yml
   /{application}-{profile}.properties
   /{label}/{application}-{profile}.properties
   ~~~

   

# 连接Git仓库配置客服端

## 使用Sourcetree连接提示ssh这是一个无效的源路径解决办法

~~~shell
#解决办法：
#生成的秘钥,终端命令如下
$ssh-keygen -t rsa -C "xxxxxx@xxxx.com"
~~~

需要把邮件地址换成你自己的邮件地址，然后一路回车，使用默认值即可
完成后会有如下显示

~~~text
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /Users/你的电脑用户名/.ssh/id_rsa.
Your public key has been saved in
~~~

进入到该地址会生成下面两个文件

![image-20200328163446792](13.png)

用文本编辑打开（id_rsa.pub）这个文件  复制贴到码云个人设置里的安全设置SSH公钥即可

![image-20200328163752041](12.png)

1. 主要的依赖

   ~~~xml
   <dependencies>
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-config-client</artifactId>
               <version>2.2.2.RELEASE</version>
           </dependency>
     			<dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-web</artifactId>
           </dependency>
           <!--完善监控信息-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-actuator</artifactId>
           </dependency>
       </dependencies>
   </dependencies>
   ~~~

   

2. 配置文件：bootstrap.yml

   ~~~yml
   #系统级别的配置spring:
   spring:
     cloud:
       config:
         uri: http://localhost:9002 #利用9002中转站读取到git上的配置
         name: config-client #从git读取的配置名称
         label: master #从master分支上获取
         profile: dev
         
   ~~~

   

3. git仓库配置

   ![image-20200318205751774](10.png)

   

4. 写controller代码测试能否拿到云端配置

   ~~~java
   @RestController
   public class ConfigClientController {
   
       @Value("${spring.application.name}")
       private String applicationName;
       @Value("${eureka.client.service-url.defaultZone}")
       private String eurekaServer;
       @Value("${server.port}")
       private String port;
   
       @RequestMapping("/config")
       public String getConfig(){
           return "applicationName："+applicationName+
                   "eurekaServer："+eurekaServer+
                   "port："+port;
       }
   
   }
   ~~~

5. 因为云端设置server.port为8021，所以访问路径是：localhost:8201/config，成功输出三个参数内容即可

# 总结

![image-20200319185043806](11.png)



