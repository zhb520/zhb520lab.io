---
categories: SpringBoot
tags: spring
date: 2020-02-06 11:20:21
---
# 异步任务

在正常的数据请求中，往往需要一点时间去处理数据，比如发送邮件需要三秒才能发送出去，但当用户点击发送后却需要三秒等待时间才能显示发送成功，这是很影响用户体验的，所以可以通过开启异步任务来直接显示发送成功，后台再慢慢的发送邮件,那需要怎么设置呢？

<!--more-->

## 例子

1. AsyncService.class

   ~~~java
   @Service
   public class AsyncService {
        public void hello(){
   
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("正在处理数据中。。");
        }
   }
   ~~~

   

2. AsyncController.class

   ~~~java
   @RestController
   public class AsyncController {
   
   
       @Autowired
       AsyncService asyncService;
   
       @RequestMapping("/hello")
       public String hello(){
           asyncService.hello();
           return "haha";
       }
   
   }
   ~~~

   

3. 如果运行上述controller就会让用户等待5秒钟才会显示“haha”,所以需要添加异步任务，在springboot中，已经用注解可以完成该功能，在service类上加上**@Async**表示该类是异步的，在springboot启动类上加上**@EnableAsync**来开启异步功能即可。







# 邮箱任务

1. 导入mail依赖

   ~~~java
   <dependency>
     <!--javax.mail-->
     <groupId>org.springframework.boot</groupId>
     <artifactId>spring-boot-starter-mail</artifactId>
    </dependency>
   ~~~

   

2. 测试

   ~~~java
    @Autowired
    JavaMailSenderImpl mailSender;
   
   
   @Test
   void contextLoads() {
     //一个简单的邮件
     SimpleMailMessage mailMessage=new SimpleMailMessage();
     mailMessage.setSubject("主题栏");
     mailMessage.setText("正文内容");
     mailMessage.setTo("zhanghaibozero@qq.com");
     mailMessage.setFrom("zhanghaibozero@qq.com");
     mailSender.send(mailMessage);
   }
   
   @Test
   void contextLoads2() throws MessagingException {
     //一个复杂的邮件
     MimeMessage mimeMessage = mailSender.createMimeMessage();
     //组装
     MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);
     helper.setSubject("主题栏~Plus");
     //第二个参数为布尔值可有可无，true代表能自动解析html标签，否则为纯文本
     helper.setText("<p style='color:red'>hello~~~</p>",true);
     //附件，这个文件必须存在，否则会报错
     helper.addAttachment("1.jpg",new File("C:\\Users\\Administrator\\Desktop\\1.jpg"));
     helper.setFrom("zhanghaibozero@qq.com");
     helper.setTo("zhanghaibozero@qq.com");
     mailSender.send(mimeMessage);
   
   }
   ~~~

   

   # 定时任务

   1. 两个核心的接口：

      **TaskScheduler.java**:任务调度

      **TaskExecutor.java**: 任务执行	

   2. 在springboot启动类上加上注解**@EnableScheduling**开启定时任务

   3. 在service层直接写定时任务即可，需要添加**@Scheduled()**这个注解，因为这是异步，启动类开启定时任务后，只要是添加这个注解的方法就会被直接调用，参数是cron表达式

      ~~~java
      @Service
      public class ScheduledService {
      
          //在一个特定的时间执行这个代码
          //秒 分 时 日 月 周
          //30 20 5  * * ？：每天的5时20分30秒被执行一次
          //30 0/5 15,18  * * ？：每天的15电点和18点每隔5分钟执行一次
          @Scheduled(cron = "* 36 * * * 0-7")
          public void hello(){
              System.out.println("hello,你被执行了");
          }
      }
      
      ~~~

      

      

   4. ![cron表达式结构](1.png)

   5. 参数详解

   ~~~text
   ，：枚举
   -：代表范围取值；
   *：代表任意时间段；
   /：表示起始时间开始触发，然后每隔固定时间触发一次，比如5/20，第五分钟触发，然后每隔20分钟触发；
   ？：表示不确定的时间，也是任意时间都可以触发，用来满足其他时间的设定，避免周和月发生冲突；
   L：表示最后，只能用在周和月上，比如在周上用5L代表最后一个星期四；
   W: 表示有效工作日(周一到周五),只能出现在DayofMonth域，系统将在离指定日期的最近的有效工作日触发事件。例如：在 DayofMonth使用5W，如果5日是星期六，则将在最近的工作日：星期五，即4日触发。如果5日是星期天，则在6日(周一)触发；如果5日在星期一 到星期五中的一天，则就在5日触发。另外一点，W的最近寻找不会跨过月份
   #:用于确定每个月第几个星期几，只能出现在DayofMonth域。例如在4#2，表示某月的第二个星期三；
   C:该字符只在日期域和星期域中使用，代表Calendar的意思。意义为计划所关联的日期。如果日期没有被关联，则相当于日历中的所有日期。如。5C在日期域中就相当于日历5日以后的第一天，1C在星期域中相当于星期日后的第一天；
   ~~~



