---
categories: Docker
tags: 镜像程序
date: 2021-04-05 11:31:06
description: Docker镜像是现在企业中常用的一种技术，利用镜像打包项目和环境，避免运维的麻烦，以最小消耗启动程序，减小服务器的压力，本篇文章从Docker的入门到精通，企业级Docker操作，后续持续更新。
---

#  Docker学习大纲

**只要学不死，就往死里学**

单击模式学习：

- Docker学习概述
- Docker安装
- Docker命令
  - 镜像命令
  - 容器命令
  - 操作命令
  - ......
- Docker镜像
- 容器数据卷
- DockerFile
- Docker网络原理
- IDEA整合Docker

企业级学习：

- Docker Compose
- Docker Swarm
- CI\CD Jenkins



# Docker概述

Docker 是一个开源的应用容器引擎，基于go语言开发，让开发者可以打包他们的应用以及依赖包到一个可移植的镜像中，然后发布到任何流行的 Linux或Windows 机器上，也可以实现虚拟化。容器是完全使用沙箱机制，相互之间不会有任何接口。

发布项目是jar包+开发环境进行一起打包。



文档地址：https://docs.docker.com/

仓库地址：https://hub.docker.com/





# Docker安装

> linux

1. 卸载旧的docker

   ~~~shell
   sudo yum remove docker \
   docker-client \
   docker-client-latest \
   docker-common \
   docker-latest \
   docker-latest-logrotate \
   docker-logrotate \
   docker-engine
   ~~~

2. 下载安装包

   ~~~shell
   sudo yum install -y yum-utils
   
   #安装镜像地址
   sudo yum-config-manager \
       --add-repo \
       https://download.docker.com/linux/centos/docker-ce.repo  #这个地址是国外的比较慢
       
   #建议使用阿里云的镜像地址
   sudo yum-config-manager \
       --add-repo \
       http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
   
   #先进行一步更新yum的软件包索引
   yum makecache fast
   #安装docker引擎，一路输入y确认即可
   sudo yum install docker-ce docker-ce-cli containerd.io
   
   #启动
   systemctl start docker
   ~~~

3. 启动成功截图

   ![image-20200518221024947](image-20200518221024947.png)

4. 测试

   ![image-20200518221448753](image-20200518221448753.png)

5. 查看镜像

   ~~~shell
   [root@iZ2ze765f19edd55b0qo32Z yum.repos.d]# docker images
   REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
   hello-world         latest              bf756fb1ae65        4 months ago        13.3kB
   ~~~

   

6. 卸载

   ~~~shell
   yum remove docker-ce docker-ce-cli containerd,io
   ~~~

   

# 阿里云镜像加速

![image-20200519114259493](image-20200519114259493.png)

通过修改daemon配置文件/etc/docker/daemon.json来使用加速器

~~~shell
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://3cuklruw.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
~~~

**服务配置完毕！**



# 回顾helloworld流程

![image-20200519123513735](image-20200519123513735.png)

## 底层原理

Docker是一个c/s结构的系统，Docker的守护进程运行在主机上，通过socket通过客户端访问，DockerServer接收到client的指令，就会执行命令。



![image-20200520122058968](image-20200520122058968.png)



# Docker的命令

## 帮助命令

~~~shell
docker version   #版本信息
docker info			 #docker系统信息，包括镜像和容器的数量
docker 命令 --help   #帮助命令
~~~



## 镜像命令

**docker images** 查看所有本地主机上的镜像

~~~SHELL
[root@iZ2ze765f19edd55b0qo32Z ~]# docker images -a
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
hello-world         latest              bf756fb1ae65        4 months ago        13.3kB

#解释
REPOSITORY  镜像的仓库源
TAG				  镜像的标签
IMAGE ID    镜像的id
CREATED     镜像的创建时间
SIZE        镜像的大小

#可选项
  -a, --all            #列出所有镜像   
  -f, --filter filter   #过滤 
  -q, --quiet           #只显示镜像ID

~~~

**docker search **搜索镜像

![image-20200520131910777](image-20200520131910777.png)

~~~shell
#跟上参数可以通过收藏数STARS过滤
-f=STARS=3000   #搜索收藏数3000以上
~~~

**docker pull**下载镜像

~~~shell
[root@iZ2ze765f19edd55b0qo32Z ~]# docker pull mysql:TAG
Using default tag: latest   #不写TAG参数的话，默认下	载最新版本 latest
latest: Pulling from library/mysql
afb6ec6fdc1c: Pull complete  #分层下载
0bdc5971ba40: Pull complete 
97ae94a2c729: Pull complete 
f777521d340e: Pull complete 
1393ff7fc871: Pull complete 
a499b89994d9: Pull complete 
7ebe8eefbafe: Pull complete 
597069368ef1: Pull complete 
ce39a5501878: Pull complete 
7d545bca14bf: Pull complete 
0f5f78cccacb: Pull complete 
623a5dae2b42: Pull complete 
Digest: sha256:beba993cc5720da07129078d13441745c02560a2a0181071143e599ad9c497fa
Status: Downloaded newer image for mysql:latest
docker.io/library/mysql:latest
#默认下载最新版
~~~

**docker rmi**删除镜像

~~~shell
#删除单个镜像
docker rmi -f 镜像id或name
#删除多个镜像
docker rmi -f 镜像id 镜像id ...
#删除所有镜像
docker rmi -f $(docker images -aq)
~~~

## 基本容器命令

 **说明：有了镜像才可以创建容器，下面下载一个centos镜像来测试学习**

~~~shell
docker pull centos #下载镜像
~~~

**新建容器并启动**

~~~shell
docker run [可选参数] image
#参数说明
--name="Name"  #自定义名字区分容器
-d             #后台运行
-it						 #使用交互方式运行，进入容器查看内容
-p						 #指定容器的端口  
	-p ip:主机端口:容器端口
	-p 主机端口:容器端口  #常用
	-p 容器端口
-P(大写p)			  #随机指定端口


#测试  启动并进入容器
[root@iZ2ze765f19edd55b0qo32Z ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
centos              latest              470671670cac        4 months ago        237MB
[root@iZ2ze765f19edd55b0qo32Z ~]# docker run -it  470671670cac /bin/bash
[root@41772ee8ee7d /]# exit   #退出命令
~~~

**列出所有运行的容器**

~~~shell
docker ps
-a 			#还会列出历史运行过的容器
-n=? 		#显示？个最近创建的容器
-q  		#只显示容器的编号
~~~

**启动和停止容器**

~~~shell
docker start 容器id     #启动
docker restart 容器id   #重启
docker stop 容器id      #停止
docker kill 容器id      #强制停止
~~~

**退出容器（进入容器后的操作）**

~~~shell
exit #直接退出容器并停止
ctrl（control） + p + q   #容器不停止退出
~~~

**删除(清理)容器**

~~~shell
docker rm 容器id   #删除指定容器 ，不能删除正在运行的容器，强制删除要用rm -f
docker rm -f $(docker ps -aq)容器id  #删除所有容器，包括在运行的
docker ps -a -q|xargs docker rm    #通过管道删除所有容器，包括在运行的
~~~

## 常用其他命令

**后台启动容器**

~~~shell
#通过 -d参数后台启动
docker run -d centos

#问题发现 通过docker ps 发现centos停止了
#docker容器使用后台运行，就必须有一个前台进程，docker发现没有应用，就会自动停止

~~~

**查看日志命令**

~~~shell
docker logs -f -t --tail N 容器id  #查看最新的N条日志
-f 实时显示，不断进行更新
-t 显示时间戳

#可以编辑一个shell脚本，用logs来参看日志
#无限输出hello
docker run -d centos /bin/bash -c "while true;do echo hello;sleep 1;done"
~~~

**查看容器中的进程信息**

~~~shell
docker top 容器id
~~~

**查看容器元数据**

~~~shell
#可以看到容器所有信息，运行的一些程序，一些配置等
docker inspect 
~~~

**进入当前正在运行的容器**

~~~shell
#我们通常容器都是使用后台运行方式，需要进入容器，修改一些配置
docker exec -it 容器id /bin/bash   #进入容器开启新的终端
docker attach 容器id  #进入容器正在运行的终端
~~~

**把容器里的东西拷贝出来**

~~~shell
#在主机中运行该命令，无论容器是否启动 只要docker ps -a发现容器存在就可以拷贝数据
docker cp 容器id:容器内的路径 目的主机路径
~~~

# 使用容器例子

# nginx

~~~shell
docker pull nginx    #下载nginx
docker run -d --name nginx01 -p 6380:80  nginx  #运行nginx 令nginx的默认端口80映射到主机6380
curl localhost:6380  #访问主机的6380端口即可进入容器的nginx
~~~

## tomcat

~~~shell
#之前启动都是后台，停止容器后还是查的到，加上--rm参数一般用来测试，用完就删除
docker run  -it --rm tomcat:9.0

~~~

# elasticsearch

~~~shell
docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.6.2

docker stats  #查看cpu状态
#因为es需要较大的内存消耗，可以通过参数-e进行限制
-e ES_JAVA_OPTS="-Xms64m -Xmx512m"  #分配了64兆，最大512兆
~~~

# 可视化

- portainer（先用这个）

  ~~~shell
  docker run -d -p 9201:9000 \
  --restart=always -v /var/run/docker.sock:/var/run/docker.sock --privileged=true portainer/portainer
  ~~~

  运行之后通过阿里云服务器或者虚拟机的ip地址即可访问：**公网ip:9201**

  ![image-20200521194855992](image-20200521194855992.png)

  第一次使用需要密码，这里设置好自己的密码

  进去后仓库选择本地的就可以了

  ![image-20200521195343594](image-20200521195343594.png)

  

- Rancher



# 提交镜像

~~~shell 
#docker commit 提交容器成为一个新的副本
docker commit -m="提交的描述信息" -a="作者" 容器id 新的镜像名:[TAG版本号标签]

~~~

## 实战测试（Tomcat）

~~~shell
#启动一个默认的tomcat
docker run -d -p 8080:8080 tomcat
#以bash命令进入tomcat
docker exec -it 容器id /bin/bash
#启动后 通过ls发现webapps下是没有文件的，启动文件都在webapps.dist文件下,复制一下文件
cp -r webapps.dist/* webapps
#这样我们就对原有容器进行了修改，加了一层操作进去，我们需要提交新的镜像，以后就可以用这个
docker commit -a="zhb" -m="add webapps" e26cb88702f0 tomcat02:1.0
#使用docker images就会发现有一个新的容器，就是我们自己修改的新容器
~~~

# 容器数据卷

docker是将环境和应用打包成一个镜像！

如果数据都在镜像容器中，比如数据在镜像里的mysql容器中，如果删除容器，数据就会丢失！

这样就有了新的需求：==数据持久化==

容器之间需要有一个数据共享的技术！Docker容器中产生的数据，同步到本地！

这就是卷技术要做的事！原理就是目录的挂载，将容器内的目录挂载到Linux上面！

## 使用数据卷

> 方式一：直接使用命令来挂载 -v

~~~shell
docker run -it -v 主机目录:容器内目录
#查看容器是否挂载成功
docker inspect 容器id
#进入后找到"Mounts":[{...}] 即可看到挂载的目录
~~~

## 匿名和具名挂载

~~~shell
#匿名挂载 不指定主机路径
docker run -d -P --name nginx01 -v /etc/nginx nginx   #-P 大写p随机映射端口
#查看所有卷的情况
docker volume ls  
#这里发现很多volume name参数全是一长串字母加数字，这就是没有命名的原因

#具名挂载，取了个名字
docker run -d -P --name nginx01 -v juming:/etc/nginx nginx
#用volume查看，发现一个叫juming的名字

#查看路径
docker volume inspect juming
~~~

使用具名挂载比较好，能够方便找到我们的卷，能够找到我们的配置

~~~shell
#挂载的三种方式
-v 容器内文件名   #匿名挂载
-v 卷名:容器内路径 #具名挂载
-v /宿主机路径:容器内路径  #指定路径挂载（最前面要加斜杠，表示绝对路径）
~~~



## 初识Dockerfile

Dockerfile就是用来构建docker镜像的构造文件！是命令脚本

> 方式2

~~~shell
#宿主机进行操作，写一个脚本
cd /home
mkdir docker_test_volume
cd docker_test_volume
vim dockerfile1
~~~

dockerfile1：

通过这个脚本可以生成镜像，镜像是一层一层的，脚本一个个的命令，每个命令都是一层！

~~~shell
FROM centos  #用centos为基础
VOLUME ["volume01","volume02"]  #挂载卷

CMD echo "====END===="
CMD /bin/bash
~~~

建造一个镜像 ， -f：脚本路径名 ， -t ：镜像名加版本号  ，最后还有一个英文的点

~~~shell
docker build -f /home/docker_test_volume/dockerfile1 -t zhb/centos:1.0 .
~~~

进入容器

![image-20200522153427753](image-20200522153427753.png)

发现了挂载的文件，我们只定义了容器内部挂载名，所以是匿名挂载

## 容器与容器数据共享

~~~shell
#新建一个容器
docker run -it --name docker01 -v /home/volume:/home/volume01 centos
#再建一个容器，通过--volumes-from参数继承docker01的挂载方式
docker run -it --name docker02 --volumes-from docker01 centos
~~~

# Dockerfile

在初识中已经了解了他是一个脚本文件，用来生成镜像的。

> 构建步骤

1. 编写一个dockerfile
2. docker build 构建成为一个镜像
3. docker run 运行镜像
4. docker push 发布镜像（DockerHub、阿里云镜像仓库）

##  DockerFile构建过程

**基础知识**

1. 每个关键字（指令）都必须是大写字母
2. 执行从上到下顺序执行
3. #表示注释
4. 每一个指令都会创建提交一个镜像层，并提交！

![image-20200522212334291](image-20200522212334291.png)

dockerfile是面向开发的，我们以后要发布项目，做镜像，就需要编写dockerfile文件



## DockerFile指令

~~~shell
FROM            #基础镜像，一起从这开始构建
MAINTAINER      #镜像创建者，一般留姓名和邮箱
RUN						  #镜像构建的时候需要运行的命令
ADD    					#比如添加tomcat压缩包，他会自动解压
COPY						#类似ADD，将我们的文件拷贝到镜像中，但不会自动解压缩
WORKDIR 				#容器运行后当前处于的工作目录
VOLUME					#挂载的目录位置
EXPOSE					#与外界暴露的端口
CMD							#指定这个容器启动要运行的命令，只有最后一个会生效，可被替代
ENTRYPOINT			#指定这个容器启动要运行的命令，可追加命令
ONBUILD					#当构建一个被继承DockerFile这个时候就会运行ONBUILD的指令(触发指令)
ENV							#构建的时候设置环境变量

~~~

参考博客：https://www.cnblogs.com/panwenbin-logs/p/8007348.html

## 实战测试

构建自己的centos，编写dockerfile文件

~~~shell
FROM centos
MAINTAINER zhb<973656795@qq.com>
ENV MYPATH /usr/local
WORKDIR $MYPATH
RUN yum -y install vim
RUN yum -y install net-tools

EXPOSE 80
CMD echo $MYPATH
CMD echo "---end---"
CMD /bin/bash
~~~

build:

~~~shell
docker build -f mydockerfile -t mycentos:1.0 .
~~~

# 发布镜像

> DokcerHub

~~~shell
#登录
docker login -u 用户名
#输入密码后用push发布
docker push 镜像名:版本号
~~~

> 阿里云镜像

1. 登录阿里云

2. 进入容器镜像服务

3. 创建命名空间

   ![image-20200524123808904](image-20200524123808904.png)

4. 创建仓库，选择本地仓库

   ![image-20200524124029033](image-20200524124029033.png)

5. 点击管理仓库，查看手册

   ![image-20200524124212625](image-20200524124212625.png)

   



# Docker网络

![image-20200524150010750](image-20200524150010750.png)

**思考：docker是如何处理容器的网络访问**

![image-20200524150451779](image-20200524150451779.png)

> 测试

~~~shell
#运行tomcat
docker run -d -P --name tomcat01 tomcat
~~~

**查看容器内部网络地址，ip addr**

![image-20200524151223853](image-20200524151223853.png)

发现容器会得到一个`56：eth0@if57`这样的地址，他的ip地址就是`172.18.0.2`，这就是docker分配的，我们再去Linux上查看网卡

![image-20200524152734989](image-20200524152734989.png)

发现多了这么一个网卡，他是57对应56。这就是Linux的veth-pair**虚拟设备接口**技术，他是一对一对成立的，彼此相连，有这特性在，**Linux主机直接可以ping通docker容器内部**，这也是桥接技术。



> 这里又有一个新的问题，那容器和容器之间能ping通吗？



![image-20200524152450366](image-20200524152450366.png)

答案是可以连通的，但这个时候不是直连的，Linux一安装Docker就会生成一个Docker0网络端口，也就是说tomcat01不能直接与tomcat02进行连接，而是都通过veth-pair技术与Linux的Docker0进行连接，再通过转发才能传输数据。

这里面的传输机制可以采用主机去注册容器地址，通过特定的转发方式去传输数据，还有一种是通过广播。

## --link

> 又产生一个新的问题，既然他们是通过ip来连接，这时可以做什么优化？

> 测试

~~~shell
#正常启动两个tomcat，对他们用服务名进行ping
docker run -d -P --name tomcat01 tomcat
docker run -d -P --name tomcat02 tomcat
~~~

![image-20200524160458840](image-20200524160458840.png)

发现通过服务名是ping不通的

接着我们启动tomcat03，使用`--link`参数与tomcat02进行连接，发现两个容器就可以通过服务名进行ping通了

![image-20200524160812767](/Users/zhb/Desktop/笔记/Docker/image-20200524160812767.png)

**但这里有个坑！**

我们用tomcat03可以ping通tomcat02，但不能用tomcat02去ping通tomcay03，反向是不通的

我们可以查看一下tomcat03的hosts探究一下原理！

![image-20200524162027544](image-20200524162027544.png)

这里发现他直接把名字在里面写死了，所以输入tomcat02也可以ping通了，而tomcat02的hosts文件中却没有tomcat03的名字，所以反向ping不通。

--link的操作其实就是在hosts文件中添加了名字进行了映射。



**现在玩Docker已经不推荐使用--link！需要自定义网络，也不再用docker0网桥，因为这里面有很多局限性！**



## 自定义网络

~~~shell
#查看docker的所有网络配置
docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
6021da7f1b00        bridge              bridge              local
98b280cf8101        host                host                local
101f7affb7f3        none                null                local
~~~

> 网络模式

bridge：桥接模式（默认）

host：和宿主机共享网络

none：不配置网络

container：容器网络连通（用的少，局限大）

> network的参数

![image-20200524204914361](image-20200524204914361.png)

> 测试

~~~shell
#之前启动时其实有一个默认的网络参数，等同于下的 --net
docker run -d -P --name tomcat01 --net bridge tomcat
#docker0特点：默认，域名不能访问，--link可以打通连接

#通过create参数创建一个新的网络配置,设置为桥接模式，再设置子网掩码和网关
docker network create --driver bridge --subnet 192.168.0.0/16 --gateway 192.168.0.1 mynet

~~~

启动两个tomcat，直接使用名字来ping

![image-20200524211126469](image-20200524211126469.png)

结果发现是可以ping通的，所以现在推荐使用自建网络桥接，而不是使用默认的。



## 网络连通

需求：但有容器在docker0默认网段下（虚拟机处于的网段），还有其他容器处于自己新建的网段下，比如`192.168.0.1`，因为处于不通的网段，这是不可能直接连接成功的，这个时候就需要打通网络

在network的参数下还有一个叫`connect`的参数，这个参数是关键！

~~~shell
#tomcat03是默认网段，把他与mynet连接
docker network connect mynet tomcat03
#查看mynet网络情况
docker network inspect mynet
~~~

![image-20200524214711459](image-20200524214711459.png)

发现他是直接把tomcat03加到了mynet里面，这样就可以直接用名字ping通了，这样tomcat03就是一个容器两个ip，比如阿里云就有公网ip和私网ip。

**结论：假设要跨网络，就需要 `docker network connect 网络名 其他网络段的容器名`去连通**



## redis集群搭建实例

shell脚本！

~~~shell
#创建网卡，redis采用自己的网卡
docker network create redis --subnet 172.38.0.0/16
#使用shell脚本编写redis的配置文件，然后通过挂载，使用自己的配置文件启动redis
for port in $(seq 1 6); \
do \
mkdir -p /mydata/redis/node-${port}/conf
touch /mydata/redis/node-${port}/conf/redis.conf
cat << EOF >/mydata/redis/node-${port}/conf/redis.conf
port 6379
bind 0.0.0.0
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
cluster-announce-ip 172.38.0.1${port}
cluster-announce-port 6379
cluster-announce-bus-port 16379
appendonly yes
EOF
done
~~~

六个redis配置写好后，先启动6个redis，再配置集群

~~~shell
#单个启动redis方式（修改参数运行6次）
docker run -p 6371:6379 -p 16371:16379 --name redis-1 \
-v /mydata/redis/node-1/data:/data \
-v /mydata/redis/node-1/conf/redis.conf:/etc/redis/redis.conf \
-d --net redis --ip 172.38.0.11 redis:5.0.9-alpine3.11 redis-server /etc/redis/redis.conf
#shell脚本启动方式（直接启动6个）
for port in $(seq 1 6); \
do \
docker run -p 637${port}:6379 -p 1637${port}:16379 --name redis-${port} \
-v /mydata/redis/node-${port}/data:/data \
-v /mydata/redis/node-${port}/conf/redis.conf:/etc/redis/redis.conf \
-d --net redis --ip 172.38.0.1${port} redis:5.0.9-alpine3.11 redis-server /etc/redis/redis.conf
done

#创建集群
redis-cli --cluster create --cluster-replicas 1 172.38.0.11:6379 172.38.0.12:6379 172.38.0.13:6379 172.38.0.14:6379 172.38.0.15:6379 172.38.0.16:6379

~~~

启动

~~~shell
#首先进入一个redis容器中，他没有bash命令，只要sh
docker exec -it redis-1 /bin/sh
#进入集群模式
redis-cli -c
#查看集群信息
cluster info
cluster nodes

~~~

至此配置完毕。

> 测试

我们随便设置一个值进去

~~~shell
set a b
-> Redirected to slot [15495] located at 172.38.0.13:6379
OK
#这个值自动分配到15495插槽，ip为172.38.0.13的主机
~~~

从集群配置中，可以看出172.38.0.14是172.38.0.13的从机

![image-20200525143028664](image-20200525143028664.png)

模拟172.38.0.13的主机宕机，即把redis-3停掉

![image-20200525143546147](image-20200525143546147.png)

发现结果是从172.38.0.14找到的

![image-20200525143704741](image-20200525143704741.png)

**这样我们就完整的使用Docker镜像搭建了redis集群，实现了高可用！**

# SpringBoot项目发布

1. 首先编写helloword的springboot项目，并把项目打成jar包

2. 接着编写Dockerfile文件

   ~~~dockerfile
   FROM java:8				#java8的环境
   COPY *.jar /app.jar  #把jar包拷贝进来并命名为app.jar
   CMD ["--server.port=8080"] 			#使用8080端口
   EXPOSE 8080					#与外界暴露8080端口
   ENTRYPOINT ["java","-jar","/app.jar"]   #运行java -jar命令
   ~~~

3. 上传到Linux虚拟机上

4. 运行dockerfile文件

   ~~~shell
   docker build -t APP
   ~~~

5. 运行

   ~~~shell
   docker run -d -P --name SpringBoot APP
   docker ps
   #从ps的参数PORTS中获取到端口映射
   curl localhost:端口号/hello   #发送hello请求，返回hello world即代表成功
   ~~~

   

# 结语

**预告**

真正在企业中会有很多的镜像，不可能一个一个启动，那这些镜像怎么进行运行维护呢？

所以还得学以下三个企业级的知识

- Docker Compose
- Docker Swarm
- CI\CD Jenkins