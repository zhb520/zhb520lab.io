---
categories: SpringBoot
tags: springboot底层详解
date: 2020-02-23 20:45:24
---

# @SpringBootApplication详解

~~~java
//@SpringBootApplication 来标注一个主程序类 ， 说明这是一个Spring Boot应用
@SpringBootApplication
public class Springboot01HelloworldApplication {

   public static void main(String[] args) {
     //将SpringBoot应用启动起来
      SpringApplication.run(Springboot01HelloworldApplication.class, args);
   }

}
~~~

<!--more-->

进入@SpringBootApplication后发现：

![image-20200221123822542](1.png)

四个元注解：

![image-20200221124109755](2.png)

![image-20200221124202621](3.png)

![image-20200221124216840](4.png)

![image-20200221124226747](5.png)

## 重要注解之：@SpringBootConfiguration注解

SpringBoot的配置类 ；标注在某个类上 ， 表示这是一个SpringBoot的配置类；

进入该注解：

![image-20200221125825891](6.png)

继续进入，发现这个只是一个普通的@Component注解，这就说明，启动类本身也是Spring中的一个组件而已，负责启动应用！

![image-20200221125915339](7.png)

## 重要注解之：@EnableAutoConfiguration注解

该注解的作用是开启自动配置功能，进入该注解：

![image-20200221131303215](8.png)

**@AutoConfigurationPackage**：自动配置包，可以扫描与当前类同级的包即controller这些包

**@import** ：Spring底层注解@import ， 给容器中导入一个组件

**AutoConfigurationImportSelector.class** ： 自动配置导入选择器，那么它会导入哪些组件的选择器呢？

进入该类在里面，看到一个方法：**getAutoConfigurationEntry**，获得自动配置实体，在这方法中看到配置是从一个方法叫**getCandidateConfigurations**中获得的

![image-20200221142706985](/9.png)

点击跳转进入**getCandidateConfigurations**方法：，方法调用了一个叫**loadFactoryNames**的方法，传入两个参数：第一个参数返回的是标注了@EnableAutoConfiguration这个注解的类，即本类，第二个经过跳转发现返回的是this.beanClassLoader，当前类加载器

![image-20200221135238349](10.png)

```java
//获得候选的配置
protected List<String> getCandidateConfigurations(AnnotationMetadata metadata，AnnotationAttributes attributes) {
    //这里的getSpringFactoriesLoaderFactoryClass（）方法
    //返回的就是我们最开始看的启动自动导入配置文件的注解类；EnableAutoConfiguration
   List<String> configurations = SpringFactoriesLoader.loadFactoryNames(getSpringFactoriesLoaderFactoryClass(),
         getBeanClassLoader());
   Assert.notEmpty(configurations, "No auto configuration classes found in META-INF/spring.factories. If you "
         + "are using a custom packaging, make sure that file is correct.");
   return configurations;
}
```

在下面一句Assert.notEmpty（）非空判断中，发现一个文件叫 **META-INF/spring.factories**

META-INF/spring.factories：自动配置的核心文件

![image-20200221135514751](11.png)

进入该文件发现全是各种自动配置文件

![image-20200221135753377](12.png)

从错误输出语句中说该文件未被找到，那说明上面的方法肯定是去调用该文件，所以进入loadFactoryNames这个方法：

~~~java
//该方法返回了loadSpringFactories方法的内容
public static List<String> loadFactoryNames(Class<?> factoryType, @Nullable ClassLoader classLoader) {
        String factoryTypeName = factoryType.getName();
        return (List)loadSpringFactories(classLoader).getOrDefault(factoryTypeName, Collections.emptyList());
    }
//查看loadSpringFactories方法
private static Map<String, List<String>> loadSpringFactories(@Nullable ClassLoader classLoader) {
    //获得classLoader ， 我们返回可以看到这里得到的就是EnableAutoConfiguration标注的类本身
    MultiValueMap<String, String> result = (MultiValueMap)cache.get(classLoader);
    if (result != null) {
        return result;
    } else {
        try {
            //去获取一个资源 "META-INF/spring.factories"
            Enumeration<URL> urls = classLoader != null ? 
            //从这里获取项目资源和系统资源
            classLoader.getResources("META-INF/spring.factories") : 
            ClassLoader.getSystemResources("META-INF/spring.factories");
            LinkedMultiValueMap result = new LinkedMultiValueMap();

            //将读取到的资源遍历，封装成为一个Properties
            while(urls.hasMoreElements()) {
                URL url = (URL)urls.nextElement();
                UrlResource resource = new UrlResource(url);
                //从这些资源中获取了所有的自动配置并封装成properties供我们使用
                Properties properties=PropertiesLoaderUtils.loadProperties(resource);
                Iterator var6 = properties.entrySet().iterator();

                while(var6.hasNext()) {
                    Entry<?, ?> entry = (Entry)var6.next();
                    String factoryClassName = ((String)entry.getKey()).trim();
                    String[] var9 = StringUtils.commaDelimitedListToStringArray((String)entry.getValue());
                    int var10 = var9.length;

                    for(int var11 = 0; var11 < var10; ++var11) {
                        String factoryName = var9[var11];
                        result.add(factoryClassName, factoryName.trim());
                    }
                }
            }

            cache.put(classLoader, result);
            return result;
        } catch (IOException var13) {
            throw new IllegalArgumentException("Unable to load factories from location [META-INF/spring.factories]", var13);
        }
    }
}
~~~

从这里就可以看出**@Import(AutoConfigurationImportSelector.class)**这个注解的作用就是为了给当前类导入

**META-INF/spring.factories**这个配置文件，所以一些基本的配置都不需要我们来配置。

还有一个小问题：

既然加载了这么多自动配置，为什么有的没有生效，需要导入对应的start才能生效？

进入自动配置**spring.factories**里面，发现了一个核心注解：**@ConditionalOnXXX**

如果这里面的条件都满足，该配置才会生效

**spring.factories**里面有着各种**XXXAutoConfiguration**配置文件

**XXXAutoConfiguration**：默认绑定**XXXProperties** 配置文件

而在yml文件可以修改的参数配置就是跟**XXXProperties**的内置方法参数一一对应

