---
categories: SpringBoot
tags: 接口测试工具
date: 2021-12-04 10:06:15
---

# swagger的集成

在项目中使用Swagger需要springfox：

- swagger
- ui

### SpringBoot集成Swagger

<!--more-->

1. 新建一个springboot-web项目

2. 导入相关依赖

   ~~~xml
   <!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger2 -->
   <dependency>
       <groupId>io.springfox</groupId>
       <artifactId>springfox-swagger2</artifactId>
       <version>2.9.2</version>
   </dependency>
   <!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger-ui -->
   <dependency>
       <groupId>io.springfox</groupId>
       <artifactId>springfox-swagger-ui</artifactId>
       <version>2.9.2</version>
   </dependency>
   
   ~~~
   
3. 编写一个 config文件SwaggerConfig.java，进行集成

   ~~~java
   @Configuration
   @EnableSwagger2//开启seagger2
   public class SwaggerConfig {
   }
   ~~~

   

4. 访问http://localhost:8080/swagger-ui.html即可

   ![image-20200229151348415](1.png)

5. 基本配置swagger

   ![image-20200229154330102](2.png)

   ![image-20200229154408071](3.png)

6. 配置扫描接口select-->build形成完整的一套过滤

   ![image-20200229162706380](4.png)

7. enable参数，默认为true,可以改为false就不启动，所以可以配置只有在生产环境中使用，发布后关闭，

   配置多个yaml设置不同的环境，在主yaml文件中通过spring.profiles.active=dev这个属性来选择是否启动那个环境配置

   ![image-20200229181835670](5.png)

8. 配置API文档分组

   通过参数**groupName("zhb")***来设置组名，如果想要多个组的话只要设置多个Docket返回不同的名字即可

   ~~~java
   @Bean
   public Docket docket1(){
       return new Docket(DocumentationType.SWAGGER_2).groupName("zhb2号");
   }
   ~~~

   

9. 对实体类信息进行操作

   只要我们的接口中返回值中存在实体类，他就会被扫描到models中，接着可以通过在实体类上面加上注解**@ApiModel(“用户实体类”)**就可以让生成的文档添加注释，能让人清楚这是什么实体类，还有针对字段的注解**@ApiModelProperty("用户名")**，还可以给接口方法添加注释**@ApiOperation("hello接口")**

