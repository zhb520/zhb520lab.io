---
categories: JUC
tags: java
date: 2020-02-05 09:32:27
description: 本文是对java.util.concurrent工具类的详解
---

# 配置java环境和版本

首先都得调成java8版本，可以支持lambda表达式

<!--more-->

![image-20200306153121343](1.png)

![image-20200306153209882](2.png)

![image-20200306153340498](3.png)





# 什么是JUC

![image-20200306160146922](4.png)

concurrent：并发

atomic：原子性

locaks：锁



# 进程和线程

进程：一个程序的运行就是一个进程

一个进程往往拥有多个线程，至少一个

java默认有两个线程：main，GC

线程：一个进程可能有多个线程，比如读线程，写线程



> Java真的可以开启线程吗？

开不了，线程的方法`.start()`实	际是通过native本地方法调用底层c++



> 线程有几个状态          Thread下有个State枚举类

~~~java
public enum State {
//新生
  NEW,
//运行
  RUNNABLE,
//阻塞
  BLOCKED,
//等待，一直等
  WAITING,
//超时等待，需设置具体等待的时间
  TIMED_WAITING,
//终止
  TERMINATED;
}
~~~

> wait/sleep的区别

1. 来自不同的类

`wait=>Object类`

`sleep=>Thread类`

2. 锁的释放

   wait是会释放锁，而sleep不会

3. 使用范围

   wait使用范围在同步代码块中

   sleep可以在任何地方睡

4. 是否需要捕获异常

   wait不需要捕获异常

   sleep需要捕获

在线程中实际开发睡眠一般也不用Thread类下的sleep，而是使用java.util.concurrent.TimeUnit类下的方法

如 TimeUnit.DAYS.sleep(1);睡一天    TimeUnit.SECONDS.sleep(1);睡一秒  





# lock锁（重点）

Java的JUC(java.util.concurrent)包中的锁包括"独占锁"和"共享锁"。JUC中的独占锁是ReentrantLock，共享锁有CountDownLatch, CyclicBarrier, Semaphore, ReentrantReadWriteLock等

> 传统：Synchronized

Synchronized的本质就是**队列加锁**

~~~java
public class saleTicketDemo01 {
    public static void main(String[] args) {

        Ticket ticket=new Ticket();
        //lambda表达式  (参数)->{代码}
				new Thread(()->{ for (int i=0;i<40;i++) ticket.sale();},"a线程").start();
        new Thread(()->{ for (int i=0;i<40;i++) ticket.sale();},"b线程").start();
        new Thread(()->{ for (int i=0;i<40;i++) ticket.sale();},"c线程").start();
}
//在实际开发中要降低耦合性，线程是一个单独的资源类，没有任何属性，
// 所以在写业务逻辑代码不要继承Runable接口,业务逻辑只是个单纯的方法
class Ticket{
    //30张票
    private int ticket =30;

    //买票的方式
    public synchronized void sale(){
        if (ticket>0){
            System.out.println(Thread.currentThread().getName()+"卖出了第"+(ticket--)+"张票，剩余："+ticket);
        }
    }


}

~~~



在新版本中建议使用JUC的java.util.concurrent.locks

该类下有三个接口：**Condition ，lock ，ReadWriteLock**

> Lock

Lock是一个接口，无法直接new 所以要new他的实现类，他有三个实现类

![image-20200307115628040](5.png)

使用方法：

![image-20200307115339517](6.png)

可重入锁一般就可以是实现功能，所以是常用的

进入源码发现他默认是非公平锁，里面还可以传入一个布尔参数，当为true时，会开启公平锁

~~~java
public ReentrantLock() {
        sync = new NonfairSync();
    }

public ReentrantLock(boolean fair) {
  sync = fair ? new FairSync() : new NonfairSync();
}
~~~



锁的问题在第**[22.各种锁的理解](#各种锁的理解)**也有提及



lock锁的固定格式：

~~~java
//Lock锁
class Ticket2{
    //30张票
    private int ticket =30;
    Lock lock=new ReentrantLock();//创建锁
    //买票的方式
    public  void sale(){
        lock.lock();//加锁
        try {
            if (ticket>0){
                System.out.println(Thread.currentThread().getName()+"卖出了第"+(ticket--)+"张票，剩余："+ticket);
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            lock.unlock();//解锁
        }

    }
~~~

> Synchronized和lock的区别

1. Synchronized是内置的关键字，Lock是一个java类

2. Synchronized是无法判断锁的状态，Lock是可以判断是否获取到了锁

3. Synchronized是全自动的，会自动释放锁，Lock必须手动释放锁，如果不释放就会变**死锁**

4. Synchronized：线程1（获得锁，阻塞），线程2（等待，继续等待。

   Lock：有一个特殊的方法 lock.tryLock();会去获取锁

5. Synchronized是可从重入锁，不可以中断，非公平锁

   Lock是可从重入锁，可以判断锁，非公平锁（可以进行更改）

6. Synchronized适合锁少量的代码同步问题，Lock适合锁大量代码







# 生产者消费者

主要用到java.util.concurrent.locks包下的Condition

见另一份笔记：**[面试四大代码相关的问题]**







# 8锁现象

> 锁是什么，锁的是谁？

这个锁主要锁两个东西：**对象(可以多个)**，**class(唯一)**



~~~java
import java.util.concurrent.TimeUnit;
/**
 * 8锁就是关于锁的8个问题
 */
public class Test1 {
    public static void main(String[] args) {
        Phone phone=new Phone();
        new Thread(()->{phone.sendSms();},"A").start();
      	//捕获
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(()->{phone.call();},"B").start();      
    } 
}
class Phone{
    
    public synchronized void sendSms(){
        System.out.println("发短信");
    }
    public synchronized void call(){
        System.out.println("打电话");
    }
}
~~~

注：以下每一题都在原代码上进行修改

1. 上述代码运行先执行发短信还是打电话？

   Synchronized 锁的对象就是方法的调用者！打电话和发短信都用了这个关键字，而上面`Phone phone=new Phone();`就只有一个，那么`phone`就是这个对象锁，谁先拿到就谁先执行，程序从上而下，那么永远都是发短信先执行

   

2. 当sendSms()方法睡眠四秒，先执行发短信还是打电话？

   ~~~java
   public synchronized void sendSms(){
     try {
       TimeUnit.SECONDS.sleep(4);
     } catch (InterruptedException e) {
       e.printStackTrace();
     }
     System.out.println("发短信");
   }
   ~~~

   因为锁依旧在A线程手上，所以依旧是等待四秒后发短信先执行

   

3. 当sendSms()方法依旧睡眠四秒，给Phone类添加一个普通方法hello()，B线程不执行打电话，执行hello()，谁先执行？

   ~~~java
    public void hello(){
           System.out.println("hello");
       }
   ~~~

   因为hello()没有锁，所以在等待1秒后直接打印出hello

   

4. 当有两个对象，一个执行发短信，一个执行打电话，谁会先执行？

   ~~~java
   Phone phone1=new Phone();
   Phone phone2=new Phone();
   phone1.sendSms();
   phone2.call();
   ~~~

   因为有两个实例对象，有两个调用者，所以有两把锁，所以先执行发短信，但当sendSms()方法睡眠四秒，过一秒后打电话就会直接执行，因为他用的是第二把锁

   

5. 当sendSms()和call都是静态方法时谁先执行？

   ~~~java
    public static synchronized void sendSms(){
           System.out.println("发短信");
       }
    public static synchronized void call(){
           System.out.println("打电话");
       }
   ~~~

   

   依旧是发短信先执行，但这个时候锁的对象是class，而不是同一个对象`phone`，因为静态类在程序一加载就会生成class文件

6. 当sendSms()和call()都是静态方法时，同样有两个对象，一个执行发短信，一个执行打电话，并且让sendSms()方法睡眠四秒，谁会先执行？

   发短信先执行，这个问题就可以完全看出问题五的蹊跷之处，如果锁的是对象，那么phone1执行发短信睡眠的时候，与phone2无关，应该打电话就会直接执行，但实际结果是依旧发短信先执行，因为他们用的是同一个class

7. 当sendSms()是静态方法，并且睡四秒，call()是普通同步方法，一个对象，谁先执行？

   打电话先执行，因为sendSms()锁的是class，而打电话锁的是对象，是不同的锁

   

8. 当sendSms()是静态方法，并且睡四秒，call()是普通同步方法，二个对象，谁先执行？

   依旧是打电话先执行，首先锁的对象就不是同一个，更别说再加了另一把锁，打电话更不会被发信息牵扯住

   

> 小结

锁无非就是new出来的对象，和被static修饰后产生的class锁这两种





# 集合类不安全

![image-20200307202135884](7.png)

**list集合**在多线程下是不安全的

~~~java
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ListTest {
    public static void main(String[] args) {
        List<String> list= new ArrayList<>();
        for (int i = 1; i <=10 ; i++) {
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(list);
            },String.valueOf(i)).start();
        }

    }
}
~~~

运行结果：

![image-20200307194424009](8.png)

一个叫**java.util.ConcurrentModificationException**的错误，并发修改异常

解决方案：

1. 改用`List<String> list= new Vector<>();`因为Vector底层add方法就是加了Synchronized，但是加了Synchronized的效率就会比较低，而且Vector是JDK1.0就有的，几乎被弃用，有更好的替代它

   ![image-20200307195531649](9.png)

2. 调用集合的顶层方法Collections的方法：

   ~~~java
   List<String> list= Collections.synchronizedList(new ArrayList<>());
   ~~~

3. Juc中也有一个解决方案：CopyOnWrite 写入时复制 简称COW ，计算机程序设计领域的一种优化策略，写入时复制add方法调用的是lock锁，效率高

   ~~~java
   List<String> list= new CopyOnWriteArrayList<>();
   ~~~



**set集合**在多线程下也是不安全的

~~~java
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


public class SetTset {

    public static void main(String[] args) {
        Set<String> set= new HashSet<>();
        for (int i = 1; i <=10 ; i++) {
            new Thread(()->{
                set.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(set);
            },String.valueOf(i)).start();
        }

    }
}
~~~

同样的也会报**java.util.ConcurrentModificationException**的错误

解决方案：

1. 用集合工具类转成安全的

   ~~~java
   Set<String> set= Collections.synchronizedSet(new HashSet<>());
   ~~~

2. JUC方法

   ~~~java
   Set<String> set= new CopyOnWriteArraySet<>();
   ~~~

   

HashSet底层是什么：**hashmap**

~~~java
public HashSet() {
        map = new HashMap<>();
    }

//add方法，就是用map put一个值
public boolean add(E e) {
  return map.put(e, PRESENT)==null;
}
~~~



**HashMap集合**在多线程下也是不安全的

解决方案：

1. 集合工具类

   ~~~java
   Map<String,String> map = Collections.synchronizedMap(new HashMap<>());
   ~~~

2. JUC,map已近不能用CopyOnWrite了，他有自己独特的方法

   ~~~java
   Map<String,String> map = new ConcurrentHashMap<>();
   ~~~

   

# 开启线程之Callable

我们都知道开启线程可以继承Thread类或者用Runnable接口：`new Thread(new Runnable()).start()`，也知道Callable也可以创建线程，但Callable具体怎么创建，底层是什么原理？

先看一下什么是callable():

![image-20200308135656455](10.png)

发现他是一个 范型是什么返回值就是什么的一个接口

去文档看一下：

![image-20200308134613993](11.png)

发现Thread只能实现Runnable接口，无法跟Callable挂上钩，那么得进入Runnable接口中

在已知实现类找到实现类只有这么几个，有一个叫FutureTask,还有一个叫ForkJoinWorkerThread(后面会用到)的是跟Thread同级：

![image-20200308134901381](12.png)

进入FutureTask查看他的构造方法：

![image-20200308135106884](13.png)

进入这个实现类发现他的构造方法中可以跟Callable挂钩，那么我们就可以通过FutureTask来创建

~~~java
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class CallableTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        MyThread myThread = new MyThread();
        FutureTask futureTask = new FutureTask(myThread);
        new Thread(futureTask,"A").start();
        //用futureTask.get()就可以拿到返回结果
      	//这个get()方法可能会产生阻塞，所以把他放到最后一行运行，或者使用异步通信
        String o = (String) futureTask.get();
        System.out.println(o);
    }
}

class MyThread implements Callable<String>{


    @Override
    public String call(){
        System.out.println("call()");
        return "hello";
    }
}
~~~

**细节：**

1. 结果会有缓存
2. 结果可能需要等待，会阻塞

**好处：**

Callable 接口类似于 Runnable，但是 Runnable 不会返回结果，并且无法抛出返回结果的异常，而 Callable  功能更强大一些，被线程执行后，可以返回值，这个返回值可以被Future拿到，也就是说，Future可以拿到异步执行任务的返回值，异步回调在后面15有讲到。

**[15. 异步回调](#异步回调)**

# 常用的辅助类

在java.util.concurrent中，有三个常用的辅助类

## CountDownLatch

这个是一个减法计数器，用来倒计时，比如我们想让所有线程执行完毕再执行下面的代码

![image-20200308150151745](14.png)

从上面代码看出，在线程没全部走完，就输出打印关闭线程，不符合我们的预期

所以我们可以通过加一句等待代码即可**countDownLatch.await**();

~~~java
public class CountDownLatchTest {


    public static void main(String[] args) throws InterruptedException {
        //计数器总数是6
        CountDownLatch countDownLatch=new CountDownLatch(6);
        for (int i = 1; i <=6 ; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"线程走了");
                countDownLatch.countDown();//-1
            },String.valueOf(i)).start();

        }
        countDownLatch.await();//只有在计数器为0时才会执行下面的操作

        System.out.println("关闭线程");


    }
~~~

原理：(三步曲)

**CountDownLatch countDownLatch=new CountDownLatch(6);**   参数为一个int值，设置计算次数

**countDownLatch.countDown();**    调用一次，计数减一

**countDownLatch.await();**   等待计数器归0再向下执行



## CyclicBarrier

这个是一个加法计数器，用来满足必须执行多少个线程后才会执行

![image-20200308152237139](15.png)

这里我们可以看到要么是单纯的一个计数，还有一个重载是实现runnable接口，写一个线程

~~~java
public class CyclicBarrierTest {

    public static void main(String[] args) {

        CyclicBarrier cyclicBarrier = new CyclicBarrier(7,()->{
            System.out.println("召唤神龙成功");
        });

        for (int i = 1; i <=7; i++) {
            //因为lambda实际上是新建了一个类，接收不到这个类中的i，所以要写一个final的变量
            final int temp=i;
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"集齐了"+temp+"颗龙珠");
                try {
                    cyclicBarrier.await();//等待，执行一次+1
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();

        }
    }
}

~~~

运行结果：执行了七个线程后，才会启动CyclicBarrier的线程，如果达不到满足条件，那CyclicBarrier的线程永远在等待





## Semaphore

信号量

~~~java
public class SemaphoreTest {

    public static void main(String[] args) {
     		//目的是限流，一次停车只能停三个
        Semaphore semaphore = new Semaphore(3);

        for (int i = 1; i <=6 ; i++) {
            new Thread(()->{
                try {
                    semaphore.acquire();//得到
                    System.out.println(Thread.currentThread().getName()+"抢到了车位");
                    TimeUnit.SECONDS.sleep(3);
                    System.out.println(Thread.currentThread().getName()+"离开了车位");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    semaphore.release();//释放
                }
            },String.valueOf(i)).start();
        }
    }
}

~~~

从上面这个例子就可以看出，每次只能三个线程进来，释放后才能继续进三个线程

原理：

**Semaphore semaphore = new Semaphore(3);**    限流，一次只允许三个线程运行

**semaphore.acquire();**      获得，假设已近满了，就会进入等待，直到被释放

**semaphore.release();**     释放

作用：多个共享资源的互斥，控制并发限流





# 读写锁

![image-20200308171000778](16.png)

代码示例：

~~~java
public class ReadWriteLockTest {
    public static void main(String[] args) {

        MyCacheLock myCacheLock = new MyCacheLock();
        //五个线程写入
        for (int i = 1; i < 5; i++) {
            final int temp=i;
            new Thread(()->{
               myCacheLock.write(temp+"",temp+"");
            },String.valueOf(temp)).start();
        }
        //五个线程读取
        for (int i = 1; i < 5; i++) {
            final int temp=i;
            new Thread(()->{
                myCacheLock.read(temp+"");
            },String.valueOf(temp)).start();
        }
    }
}


class MyCacheLock{

    private ReadWriteLock readWriteLock=new ReentrantReadWriteLock();
    private Map<String,String> map=new HashMap<>();

    public void write(String key,String value){
        readWriteLock.writeLock().lock();//加锁
        try {
            System.out.println(Thread.currentThread().getName()+"正在写入");
            map.put(key,value);
            System.out.println(Thread.currentThread().getName()+"写入完毕");


        }catch (Exception e){

        }finally {
            readWriteLock.writeLock().unlock();//解锁
        }

    }

    public void read(String key){
        readWriteLock.readLock().lock();//加锁
        try {
            System.out.println(Thread.currentThread().getName()+"正在读取");
            map.get(key);
            System.out.println(Thread.currentThread().getName()+"读取完毕");


        }catch (Exception e){

        }finally {
            readWriteLock.readLock().unlock();//解锁
        }

    }
}
~~~

运行结果：

![image-20200308173513210](17.png)

我们发现在写入的时候就没有人会插队，必须写完毕才会有下一个写入线程，而读线程就可以插队，一个没读取完毕，另一个就开始读。



# 阻塞队列BlockingQueue

写入：如果队列满了，必须阻塞等待

读：如果队列为空，必须阻塞等待生产

![image-20200308173513210](18.png)

什么情况下需要使用到BlockingQueue？

BlockingQueue并不是新的东西，他的常用实现类就有ArrayBlockingQueue和LinkedBlockingQueue，是与set和list的实现类一样

一般都会在多线程并发处理，线程池中使用。

![image-20200308193804145](19.png)

![image-20200308200204016](20.png)

**学会队列**

添加 add，移除 remove，先进先出

**队列的四组API**

| 方式         | 抛出异常 | 有返回值，不抛出异常 | 阻塞，等待 | 超时等待                |
| ------------ | -------- | -------------------- | ---------- | ----------------------- |
| 添加         | add      | offer()              | put        | offer(, time, TimeUnit) |
| 移除         | remove   | poll()               | take       | poll(, time, TimeUnit)  |
| 检测对首元素 | element  | peek                 | -          | -                       |



1. 抛出异常

   ~~~java
    /**
     * 抛出异常
     */
   public static void test1(){
     //定义队列大小为3
     ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
   
     System.out.println(blockingQueue.add("a"));//true
     System.out.println(blockingQueue.add("b"));//true
     System.out.println(blockingQueue.add("c"));//true
     //因为队列已满，第四个添加就会报错：IllegalStateException: Queue full
     //System.out.println(blockingQueue.add("d"));
     //查看对首是谁  a,不存在就会报NoSuchElementException错误	
     System.out.println(blockingQueue.element());
     System.out.println("==========");
     System.out.println(blockingQueue.remove());//a
     System.out.println(blockingQueue.remove());//b
     System.out.println(blockingQueue.remove());//c
     //因为队列已近空了再移除就会报错：NoSuchElementException
     //System.out.println(blockingQueue.remove());
   }
   ~~~

   

2. 不会抛出异常

   ~~~java
    /**
     * 不抛出异常
     */
   public static void test2(){
     //定义队列大小为3
     ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
   
     System.out.println(blockingQueue.offer("a"));//true
     System.out.println(blockingQueue.offer("b"));//true
     System.out.println(blockingQueue.offer("c"));//true
     //System.out.println(blockingQueue.offer("d"));//false
     System.out.println(blockingQueue.peek());//查看对首是谁  a，不存在为null
     System.out.println("==========");
     System.out.println(blockingQueue.poll());//a
     System.out.println(blockingQueue.poll());//b
     System.out.println(blockingQueue.poll());//c
     System.out.println(blockingQueue.poll());//null
   }
   ~~~

   

3. 阻塞 等待

   ~~~java
   /**
    * 阻塞 等待（一直阻塞）
    */
   public static void test3() throws InterruptedException {
     //定义队列大小为3
     ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
     //put没有返回值
     blockingQueue.put("a");
     blockingQueue.put("b");
     blockingQueue.put("c");
     //当放不进去后，程序会一直阻塞，等到能放进去
     // blockingQueue.put("d");
     System.out.println(blockingQueue.take());//a
     System.out.println(blockingQueue.take());//b
     System.out.println(blockingQueue.take());//c
     //没东西后依旧一直处于阻塞状态
     //System.out.println(blockingQueue.take());
   
   }
   ~~~

   

4. 超时等待

   ~~~java
   
   /**
    * 阻塞 等待（超时等待）
    */
   public static void test4() throws InterruptedException {
     //定义队列大小为3
     ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
   
     System.out.println(blockingQueue.offer("a"));//true
     System.out.println(blockingQueue.offer("b"));//true
     System.out.println(blockingQueue.offer("c"));//true
     //等待一段时间，如果不能加进去，程序就结束并返回false
     System.out.println(blockingQueue.offer("d",2, TimeUnit.SECONDS));
     System.out.println("=========");
     System.out.println(blockingQueue.poll());//a
     System.out.println(blockingQueue.poll());//b
     System.out.println(blockingQueue.poll());//c
     //等待一段时间依旧没有返回null
     System.out.println(blockingQueue.poll(2,TimeUnit.SECONDS));
   
   }
   ~~~

   

   > SynchronizedQueue 同步队列

   没有容量

   进去一个元素，必须等到取出来后，才能往里面再放元素







# 线程池	

线程池：3大方法，7大参数，4种拒绝策略

> 池化技术

程序的运行，本质：占用系统的资源！优化资源的使用！创建和销毁十分消耗资源=>池化技术

池化技术：事先准备好一些资源，有需要就来我这拿，用完还回来



**线程池的好处：**

1. 降低资源的消耗

2. 提高响应速度

3. 方便管理

   线程可以复用，可以控制最大并发数，管理线程





> 三大方法

三大方法代码示例：

~~~java
//Executors 工具类，三大方法
public static void main(String[] args) {
  //ExecutorService threadPoll = Executors.newSingleThreadExecutor();//单个线程
  //ExecutorService threadPoll = Executors.newFixedThreadPool(5);//固定的线程，设置5,那最多5个线程能用
  ExecutorService threadPoll =Executors.newCachedThreadPool();//可伸缩的线程池，按需求，for循环几次，最多几个

  try {
    for (int i = 0; i < 10; i++) {
      threadPoll.execute(()->{
        System.out.println(Thread.currentThread().getName()+"ok");
      });
    }
  } catch (Exception e) {
    e.printStackTrace();
  } finally {
    //线程池用完要关闭
    threadPoll.shutdown();
  }
}
~~~

> 源码分析：（七大参数）

~~~java
//三大方法底层
public static ExecutorService newSingleThreadExecutor() {
        return new FinalizableDelegatedExecutorService
            (new ThreadPoolExecutor(1, 1,
                                    0L, TimeUnit.MILLISECONDS,
                                    new LinkedBlockingQueue<Runnable>()));
    }

public static ExecutorService newFixedThreadPool(int nThreads) {
        return new ThreadPoolExecutor(nThreads, nThreads,
                                      0L, TimeUnit.MILLISECONDS,
                                      new LinkedBlockingQueue<Runnable>());
    }

public static ExecutorService newCachedThreadPool() {
        return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                                      60L, TimeUnit.SECONDS,
                                      new SynchronousQueue<Runnable>());
    }



//发现本质：new ThreadPoolExecutor()
public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory,
                              RejectedExecutionHandler handler) {
        if (corePoolSize < 0 ||
            maximumPoolSize <= 0 ||
            maximumPoolSize < corePoolSize ||
            keepAliveTime < 0)
            throw new IllegalArgumentException();
        if (workQueue == null || threadFactory == null || handler == null)
            throw new NullPointerException();
        this.acc = System.getSecurityManager() == null ?
                null :
                AccessController.getContext();
        this.corePoolSize = corePoolSize;
        this.maximumPoolSize = maximumPoolSize;
        this.workQueue = workQueue;
        this.keepAliveTime = unit.toNanos(keepAliveTime);
        this.threadFactory = threadFactory;
        this.handler = handler;
    }

//发现了有7大参数
int corePoolSize,//核心线程池大小
int maximumPoolSize,//最大核心线程池大小
long keepAliveTime,//超时没有人调用就会释放
TimeUnit unit,//超时等候的单位，比如秒
BlockingQueue<Runnable> workQueue,//阻塞队列
ThreadFactory threadFactory,//线程工厂，创建线程，一般不用动
RejectedExecutionHandler handler//拒绝策略
 
~~~

> 四种拒绝策略：

![image-20200309151055855](21.png)

1. AbortPolicy:默认的拒绝策略,超出最大可能就可能会报RejectedExecutionException错误
2. CallerRunsPolicy:哪来的回哪里,在程序中，被拒绝后这个线程就会被main线程执行
3. DiscardOldesPolicy:队列满了，不会抛出异常，尝试去和最早的线程竞争，要是最早线程快结束了立马跟上，不然也丢掉该线程
4. DiscardPolicy:队列满了，不会抛出异常，但会把线程丢掉造成丢失

7大参数银行举例：

比如银行办业务，一共5个窗口（最大核心线程池大小），3个等候区位置（阻塞队列）。有2个窗口（核心线程池大小）一直开启营业，3个窗口关闭，但当等候区满了那三个窗口就会开启，当一段时间没有调用这个三个窗口，三个窗口就会又关闭（超时，释放）。当全部窗口开启，等候区也满了，还在进来来，那么就会被拒绝策略拒绝。

**阿里巴巴开发手册约束**：

![image-20200308211526962](22.png)



> 手动创建线程池

因为不推荐用**Executors**来创建线程池，所以使用原生的**ThreadPollExecutor**

~~~java
 public static void main(String[] args) {
        ThreadPoolExecutor threadPoll = new ThreadPoolExecutor(
          			2,//核心线程数
                5,//最大线程数
                3,//等待时间3
                TimeUnit.SECONDS,//秒
                new LinkedBlockingDeque<>(3),//阻塞队列3
                Executors.defaultThreadFactory(),//默认创建线程工厂
                new ThreadPoolExecutor.AbortPolicy());//默认拒绝策略

        try {
            for (int i = 1; i <=9; i++) {
                threadPoll.execute(()->{
                    System.out.println(Thread.currentThread().getName()+"线程创建成功");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //线程池用完要关闭
            threadPoll.shutdown();
        }
    }
~~~



> 最大线程到底该如何定义：（线程并发调优）

1. CPU密集型，看一下电脑最大线程多少就定义多少，保证cpu效率最高，一般定义不写死，通过代码获取，因为每台电脑都可能不一样，服务器也会比电脑多很多

   ~~~java
   Runtime.getRuntime().availableProcessors();//获取cpu线程数
   ~~~

   

2. IO密集型，判断你的程序中十分耗IO线程的线程数，设置大于这个数就好，一般设置2倍

   



# 函数式接口（必须掌握）

新时代的程序员：lambda表达式 、链式编程 、函数式接口 、Stream流计算

四大函数接口就是Java.util.function包下的**Consumer、Function、Predicate、Supplier**

> Function 函数型接口

![image-20200309171307237](23.png)

代码测试

~~~java
/**
 * Function 函数型接口，有一个输入参数，一个输出
 * 只要是函数型接口，可以用Lambda表达式简化
 */

public class FunctionTest {

    public static void main(String[] args) {
       /* Function function = new Function<String, String>() {
            @Override
            public String apply(String str) {
                return str;
            }
        };*/

        Function function=(str)->{return str;};

        System.out.println(function.apply("123"));

    }
}
~~~

**XXXOperator**:是function的子接口，同样有输入值，一个返回值



> Predicate 断定型接口

![image-20200309172853436](24.png)

代码测试：

~~~java
/**
 * Predicate 断定型接口，一个输入参数，返回布尔值
 * 只要是函数型接口，可以用Lambda表达式简化
 */
public class PredicateTest {

    public static void main(String[] args) {
        //可以用来判断字符串是否为空
        /*Predicate<String> predicate = new Predicate<String>() {
            @Override
            public boolean test(String str) {
                return str.isEmpty();
            }
        };*/
        Predicate<String> predicate=(str)->{return str.isEmpty();};
        System.out.println(predicate.test(""));
    }
}
~~~



> Consumer 消费型接口

![image-20200309173841735](25.png)

~~~java
/**
 * Consumer 消费型接口，一个输入参数，没有返回值
 * 只要是函数型接口，可以用Lambda表达式简化
 */
public class ConsumerTest {

    public static void main(String[] args) {

       /* Consumer<String> consumer = new Consumer<String>() {
            @Override
            public void accept(String str) {
                System.out.println("购买了"+str);
            }

        };*/
        Consumer<String> consumer=(str)->{System.out.println("购买了"+str);};
        consumer.accept("手机");

    }
}
~~~



> Supplier 供给型接口

![image-20200309184126677](26.png)

~~~java
/**
 * Supplier 供给型接口，没有输入参数，只有返回值
 * 只要是函数型接口，可以用Lambda表达式简化
 */
public class SupplierTest {

    public static void main(String[] args) {
        /*Supplier<Integer> supplier = new Supplier<Integer>() {
            @Override
            public Integer get() {
                System.out.println("get()");
                return 1024;
            }
        };*/
        Supplier<Integer> supplier=()->{return 1024;};

        System.out.println(supplier.get());
    }
}
~~~





# Stream流式计算

~~~java
/**
 * 题目要求：只用一行代码实现
 * 现在有五个用户，筛选：
 * 1. ID必须为偶数
 * 2. 年龄必须大于23岁
 * 3. 用户名转为大写字母
 * 4. 用户名字母倒着排序
 * 5. 只输出一个用户
 */
public class Test {

    public static void main(String[] args) {
        User u1=new User(1,"a",21);
        User u2=new User(2,"b",22);
        User u3=new User(3,"c",23);
        User u4=new User(4,"d",24);
        User u5=new User(6,"e",25);
        //集合就是存储
        List<User> list = Arrays.asList(u1, u2, u3, u4, u5);

        //计算交给Stream流
        list.stream().filter(u->{return u.getId()%2==0;})
                .filter(u ->{return u.getAge()>23;})
                .map(u->{return u.getName().toUpperCase();})
                .sorted((uu1,uu2)->{return uu2.compareTo(uu1);})
                .limit(1)
                .forEach(System.out::println);

    }
}

~~~





# ForkJoin

> 什么是ForkJoin

ForkJoin在JDK 1.7之后出现，主要是并行执行任务，提高效率，大数据量！

大数据：Map Reduce,就是把大任务拆分成小任务

> ForkJoin的特点：工作窃取

比如有A,B两条线程，当A执行了一些任务并没有执行完，而B线程已经全部执行完了，为了不让线程等待，B线程会去偷A线程的任务，提升效率，这里面的线程维护的都是**双端队列**，这样B线程就可以从底部取任务。

**ForkJoinDemo**

~~~java
/**
 * 如何使用ForkJoin
 * 1. 继承RecursiveTask<>
 * 2. 重写compute方法
 */
public class ForkJoinDemo extends RecursiveTask<Long> {

    private Long start;
    private Long end;
    private Long temp=10000L;//临界值

    public ForkJoinDemo(Long start,Long end){
        this.start=start;
        this.end=end;
    }



    //实现接口
    @Override
    protected Long compute() {
        //如果计算的低于临界值，就没必要分开任务计算
        if ((end-start)<temp){
            Long sum=0L;
            for (Long i = start; i <=end ; i++) {
                sum += i;
            }
            return sum;
        }else {
            long middle = (start + end) / 2;
            //分成两个任务，递归计算，查看start,middle是否高于临界值，高了继续分任务
            ForkJoinDemo task1=new ForkJoinDemo(start,middle);
            task1.fork();//把任务压人线程队列
            ForkJoinDemo task2=new ForkJoinDemo(middle+1,end);
            task2.fork();//把任务压人线程队列
            return task1.join()+task2.join();//获取结果
        }

    }
}

~~~

测试类：

~~~java
//test1():普通程序员    test2():ForkJoin     test3():Stream并行流

public class Test {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        test3();
    }

    public static void test1(){
        long start = System.currentTimeMillis();
        Long sum=0L;
        for (Long i = 1L; i <=10_0000_0000L; i++) {
            sum += i;
        }
        long end = System.currentTimeMillis();
        System.out.println("sum="+sum+",执行了："+(end-start)+"ms");

    }

    public static void test2() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();

        ForkJoinPool forkJoinPool = new ForkJoinPool();
        ForkJoinTask<Long> task = new ForkJoinDemo(0L,10_0000_0000L);
        Long sum = forkJoinPool.invoke(task);//直接执行返回结果
        //forkJoinPool.execute(task);//执行任务，没有结果

        //ForkJoinTask<Long> submit = forkJoinPool.submit(task);//提交任务，有结果
        //Long sum = submit.get();//通过get获取结果

        long end = System.currentTimeMillis();
        System.out.println("sum="+sum+",执行了："+(end-start)+"ms");
    }

    public static void test3(){
        long start = System.currentTimeMillis();
        //并行流 range->(,)  rangeClosed->(,]
        long sum = LongStream.rangeClosed(0L, 10_0000_0000L)
          .parallel()//将流转换成并行流
          .reduce(0, Long::sum);
        long end = System.currentTimeMillis();
        System.out.println("sum="+sum+",执行了："+(end-start)+"ms");
    }
}


~~~





# 异步回调

> Future 设计的初衷：对将来的某个事件的结果进行建模

有了Future就可以进行三段式的编程了，1.启动多线程任务2.处理其他事3.收集多线程任务结果。从而实现了非阻塞的任务调用。在途中遇到一个问题，那就是虽然能异步获取结果，但是Future的结果需要通过isdone来判断是否有结果，或者使用get()函数来阻塞式获取执行结果。这样就不能实时跟踪其他线程的结果状态了，所以直接使用get还是要慎用，最好配合isdone来使用。

~~~java
/**
 * 异步调用：ajax，CompletableFuture
 * //异步执行
 * //成功回调
 * //失败回调
 */
public class Demo01 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //发起一个请求，没有返回结果得异步回调
        /*CompletableFuture<Void> completableFuture =CompletableFuture.runAsync(()->{
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"runAsync=>Void");
        });

        System.out.println(Thread.currentThread().getName()+"线程");
        Void aVoid = completableFuture.get();//获取结果
    }*/

        //有返回值的异步回调
        CompletableFuture<Integer> completableFuture=CompletableFuture.supplyAsync(()->{
            System.out.println(Thread.currentThread().getName()+"supplyAsync=>Integer");
            return 1024;
        });

        completableFuture.whenComplete((t,u)->{
            System.out.println("t=>"+t);//正常时返回结果1024   错误时返回null
            System.out.println("u=>"+u);//错误时返回错误信息   正确时返回null 
        }).exceptionally((e)->{
            e.getMessage();
            return 404;
        });
    }
}
~~~





# JMM

> 什么是JMM

JMM：java内存模型，不存在的东西，是一个概念，他是**用于定义数据读写的规则**，类似于一种协议。

JMM相对于物理机的缓存一致性协议来说它还要处理JVM自身特有的问题：重排序问题，这个时候就要提到一个重要的东西：**Volatitle**

> 请你谈谈你对Volatile的理解

Volatile是java虚拟机提供**轻量级的同步机制**

1. 保证可见性
2. 不保证原子性
3. 禁止指令重排

> 关于JMM的一些同步的约定

1. 线程解锁前，必须把共享变量**立刻**刷新回主存
2. 线程加锁前，必须读取主存中的最新值到工作内存中
3. 加锁和解锁是同一把锁



线程：工作内存，主内存

八种操作：（四组八操作）

![image-20200310171631359](27.png)

内存交互操作有8种，虚拟机实现必须保证每一个操作都是原子的，不可在分的（对于double和long类型的变量来说，load、store、read和write操作在某些平台上允许例外）

- lock   （锁定）：作用于主内存的变量，把一个变量标识为线程独占状态
- unlock （解锁）：作用于主内存的变量，它把一个处于锁定状态的变量释放出来，释放后的变量才可以被其他线程锁定
- read  （读取）：作用于主内存变量，它把一个变量的值从主内存传输到线程的工作内存中，以便随后的load动作使用
- load   （载入）：作用于工作内存的变量，它把read操作从主存中变量放入工作内存中
- use   （使用）：作用于工作内存中的变量，它把工作内存中的变量传输给执行引擎，每当虚拟机遇到一个需要使用到变量的值，就会使用到这个指令
- assign （赋值）：作用于工作内存中的变量，它把一个从执行引擎中接受到的值放入工作内存的变量副本中
- store  （存储）：作用于主内存中的变量，它把一个从工作内存中一个变量的值传送到主内存中，以便后续的write使用
- write 　（写入）：作用于主内存中的变量，它把store操作从工作内存中得到的变量的值放入主内存的变量中

**JMM对这八种指令的使用，制定了如下规则：**

- 不允许read和load、store和write操作之一单独出现。即使用了read必须load，使用了store必须write

- 不允许线程丢弃他最近的assign操作，即工作变量的数据改变了之后，必须告知主存
- 不允许一个线程将没有assign的数据从工作内存同步回主内存
- 一个新的变量必须在主内存中诞生，不允许工作内存直接使用一个未被初始化的变量。就是怼变量实施use、store操作之前，必须经过assign和load操作
- 一个变量同一时间只有一个线程能对其进行lock。多次lock后，必须执行相同次数的unlock才能解锁
- 如果对一个变量进行lock操作，会清空所有工作内存中此变量的值，在执行引擎使用这个变量前，必须重新load或assign操作初始化变量的值
- 如果一个变量没有被lock，就不能对其进行unlock操作。也不能unlock一个被其他线程锁住的变量
- 对一个变量进行unlock操作之前，必须把此变量同步回主内存





# 17. Volatile

> 1. 保证线程可见性

存在问题:假如线程A修改了参数，而另一线程B依旧是还是原参数，导致程序运有异常，对修改的参数不能及时可见

~~~java
public class JMMDemo {

    private static  int num=0;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            while (num == 0) {
            }
        }, "A").start();
        TimeUnit.SECONDS.sleep(1);

        num = 1;
        System.out.println(num);
    }
}
~~~

运行结果：

![image-20200310172437211](28.png)

解决办法：对参数加Volatile，保证所有线程可见性

~~~java
private volatile static  int num=0
~~~

> 2. 不保证原子性

原子性：不可分割

线程A在执行任务时，不能被打扰，也不能被分割

~~~java
public class VDemo02 {
    private volatile static int num=0;

    public static void add(){
        num++;
    }

    public static void main(String[] args) {
        for (int i = 1; i <=20 ; i++) {
            new Thread(()->{
                for (int j = 1; j <= 1000; j++) {
                    add();
                }
            }).start();
        }

        while (Thread.activeCount()>2){
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName()+" "+num);

    }
}
~~~

上述代码在运行结果法现num最后没有加到2w，就是因为num++本身就不是一个原子性操作

解决方法：

使用原子类，在java.util.concurrent.atomic包下的各种原子类，比如：AtomicBoolean，AtomicLong等

~~~java
private volatile static AtomicInteger num=new AtomicInteger();

public static void add(){
  //并不是一个简单的+1操作，使用底层的 CAS
  num.getAndIncrement();
}
~~~

原子类的底层都和操作系统挂钩，是在内存中直接修改值，底层调用了**Unsafe.class**，在**19. CAS**中进行详解

> 指令重排

什么是指令重排：**你写的程序，计算机并不是按照你写的那样去执行。**

源代码-->编译器优化的重排-->指令并行也可能会重排-->内存系统也会重排-->执行

~~~java
int x=1; //1
int y=2; //2
x=x+5;	 //3
y=x*x;	 //4

//我们说期望的：1234，但是执行时可能就变成2134或者1324、
//处理器在进行指令重排的时候，会考虑数据之间的依赖性！y会依赖于int y ，所以不会变成 4123
~~~

指令重排例子  : 假设a b x y 都是0

| 线程A | 线程B |
| ----- | ----- |
| x=a   | y=b   |
| b=1   | a=2   |

正常的结果 ：x=0；y=0；

对于单独的线程A或B来讲，不存在数据依赖，那么就可能会进行指令重排：

| 线程A | 线程B |
| ----- | ----- |
| b=1   | a=2   |
| x=a   | y=b   |

异常的结果 ：x=2；y=1；

**使用Volatile就可避免指令重排：**

内存屏障。CPU指令。作用：

1. 保证特定操作的执行顺序
2. 可以保证某些变量的内存可见性（利用这些特性Volatile实现了可见性）

在使用Volatile时，他会在该指令写操作时的上面和下面都加一层内存屏障，禁止了指令的顺序交换

> 小结

Volatile是可以保持可见性，但不保持原子性，由于内存屏障的存在，可以保证避免了指令重排的现象



# 彻底玩转单例模式

**内存屏障这种东西会在单例模式使用的最多**

饿汉式

~~~java
/**
 * 饿汉式单例 
 * 单例模式构造器私有，保证别人不能new，保证内存中就一个该对象
 * 饿汉式是程序一运行就加载该对象
 */
public class Hungry {
    
    //因为饿汉式一上来就加载，这5组数据非常耗内存资源，
    // 这些数组创建后没使用就会造成空间浪费
    private byte[] data1=new byte[1024*1024];
    private byte[] data2=new byte[1024*1024];
    private byte[] data3=new byte[1024*1024];
    private byte[] data4=new byte[1024*1024];
    private byte[] data5=new byte[1024*1024];
    
    private Hungry(){
        
    }
    
    private final static Hungry HUNGRY=new Hungry();
    
    public static Hungry getInstance(){
        return HUNGRY;
    }
}
~~~

懒汉式

~~~java
/**
 * 懒汉式单例
 * 单例模式构造器私有，保证别人不能new，保证内存中就一个该对象
 * 懒汉式是程序运行后有需要才去加载该对象
 */
public class LazyMan {
    private LazyMan(){
        System.out.println(Thread.currentThread().getName()+"线程创建了对象");
    }

    private static LazyMan lazyMan;

    public static LazyMan getInstance(){
        if (lazyMan==null){
            lazyMan=new LazyMan();
        }
        return lazyMan;
    }
}
//多线程测试懒汉式
//多线程并发,会出问题
public static void main(String[] args) {
    for (int i = 1; i <=10 ; i++) {
        new Thread(()->{
           LazyMan.getInstance();
        }).start();
    }
}
~~~

在多线程下发现输出语句多次输出，表示该对象会被多次创建，不满足单例，所以需要对懒汉式进行加锁

DCL懒汉式：双重加锁检验

双重检验的目的：

当A线程判断为空时，B线程插进来了，但A线程还没创建对象，所以B也会判断为空，就会进入同步代码块，当A线程重新插回来执行创建对象后，B线程已经进入到同步代码块中，如果不再进行一次判断，那么就会再创建一次对象

~~~java
 public static LazyMan getInstance(){
        if (lazyMan == null) {
            synchronized (LazyMan.class) {
                if (lazyMan == null) {
                    lazyMan = new LazyMan();//不是原子性操作
                   /**
                     * 经过三步骤创建
                     * 1.分配内存空间
                     * 2.执行构造方法，初始化对象
                     * 3.把这个对象指向这个分配的空间
                     */
                }

            }
        }
        return lazyMan;
    }
~~~

因为`lazyMan = new LazyMan();`不是原子性操作,步骤如上述代码的注释里，那么就会出现问题：

如果三步骤发生指令重排，变成了132，对于A线程来说不会 出现问题，当A线程先把对象指向这个分配的空间，那么**lazyMan！=null**，但是这个时候2还没执行，没有初始化对象，这时B线程插进来了执行了，虽然进不去同步代码块，但是可以判断lazyMan！=null，那么就会直接返回，但这个时候对象还没初始化，B线程要是拿去使用就会出现问题

解决办法，加volatile关键字

~~~java
private volatile static LazyMan lazyMan;
~~~



## 花式玩单例

静态内部类

~~~java
/**
 * 静态内部类
 */
public class Holder {

    private Holder(){}
    
    public static Holder GetInstance(){
        return InnerClass.HOLDER;
    }

    public static  class InnerClass{
        private static final Holder HOLDER=new Holder();
    }
}
~~~



反射破坏单例：

~~~java
//反射破坏单例
public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
  //通过getInstance正常创建第一个
  LazyMan lazyMan =  LazyMan.getInstance();
  //getDeclaredConstructor()：返回public和非public的构造器，没有参数类型的过滤
  Constructor<LazyMan> declaredConstructor = LazyMan.class.getDeclaredConstructor();
  //无视私有构造器，创建第二个
  declaredConstructor.setAccessible(true);
  LazyMan lazyMan1 = declaredConstructor.newInstance();

  System.out.println(lazyMan.hashCode());
  System.out.println(lazyMan1.hashCode());
}
~~~

解决反射破坏：

~~~java
//在构造器中再添加一把锁检测
 private LazyMan(){
        synchronized (LazyMan.class){
            if (lazyMan!=null){
                throw new RuntimeException("不要试图利用反射破坏");
            }
        }
    }
~~~

不使用getInstance()创建，反射多次创建继续破坏：

~~~java
public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
  //getDeclaredConstructor()：返回public和非public的构造器，没有参数类型的过滤
  Constructor<LazyMan> declaredConstructor = LazyMan.class.getDeclaredConstructor();
  //无视私有构造器，创建第二个
  declaredConstructor.setAccessible(true);
  LazyMan lazyMan1 = declaredConstructor.newInstance();
  LazyMan lazyMan2 = declaredConstructor.newInstance();
  System.out.println(lazyMan1.hashCode());
  System.out.println(lazyMan2.hashCode());
}
~~~

通过任意命名的变量当做标志位

~~~java
 private static boolean zhb=false;

    private LazyMan(){
        synchronized (LazyMan.class){
            if (zhb==false){
                zhb=true;
            }else{
                throw new RuntimeException("不要试图利用反射破坏");
            }
        }
    }
~~~

但通过反编译并解密获取到你的标志位名字，就可以继续破坏：

~~~java
//反射破坏单例
public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
  Field zhb = LazyMan.class.getDeclaredField("zhb");
  zhb.setAccessible(true);

  //getDeclaredConstructor()：返回public和非public的构造器，没有参数类型的过滤
  Constructor<LazyMan> declaredConstructor = LazyMan.class.getDeclaredConstructor();
  //无视私有构造器，创建第二个
  declaredConstructor.setAccessible(true);
  LazyMan lazyMan1 = declaredConstructor.newInstance();
  zhb.set(lazyMan1,false);//重新赋值
  LazyMan lazyMan2 = declaredConstructor.newInstance();
  System.out.println(lazyMan1.hashCode());
  System.out.println(lazyMan2.hashCode());
}
~~~

所以可以使用枚举，因为枚举不能破坏单例

```java
/**
 * 枚举 本身也是一个class类
 */
public enum EnumSingle {

    INSTANCE;

    public EnumSingle getInstance(){
        return getInstance();
    }
}

class Test{
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        EnumSingle instance=EnumSingle.INSTANCE;
      //用jad反编译枚举类，发现他并不是无参构造，而是一个string一个int的有参构造
      //发现是有参后，输入参数继续利用反射去创建
        Constructor<EnumSingle> declaredConstructor = EnumSingle.class.getDeclaredConstructor(String.class,int.class);
        declaredConstructor.setAccessible(true);
        EnumSingle instance1 = declaredConstructor.newInstance();
        System.out.println(instance.hashCode());
        System.out.println(instance1.hashCode());
       }
}

```

运行结果：

![image-20200311122456096](29.png)

出现真正的结果，发现枚举不能通过反射破坏单例





# 深入理解CAS

> CAS:（compareAndSet比较并交换）

~~~java
public class CASDemo {


    public static void main(String[] args) {
        //初始值2020
        AtomicInteger atomicInteger=new AtomicInteger(2020);
        //compareAndSet(int expect, int update)，expect：期望 update：更新  比较并交换
        //如果达到了期望值就更新，否则不更新
        atomicInteger.compareAndSet(2020,2021);
        //加一操作
        atomicInteger.incrementAndGet();

    }
}
~~~

深入底层学习：

进入incrementAndGet，发现有一个unsafe类，里面大部分都是加了native的方法，直接调用c++操作内存

![image-20200311124013101](30.png)

进入unsafe类，知道这个**valueOffset**是这个对象所对应的值

![image-20200311124735572](31.png)

![image-20200311125823658](32.png)

缺点：

1. 底层是自旋锁，循环会耗时

2. 一次性只能保证一个共享变量的原子性

3. 存在ABA问题

   

> CAS: ABA问题（狸猫换太子）

A线程进行操作时，B线程可能会进行捣乱，B线程先把1改为3，又改回1，当A线程执行时，并不知道这个值被动过，依旧可以继续执行，且结果无误，但这个过程并不是我们期待的，所以需要用到**原子引用**

![image-20200311130742488](33.png)



# 原子引用

>  解决ABA问题：引入原子引用，这是一个带版本号的原子操作，与乐观锁的原理一样，使用版本号

在这里因为我们测试时原子引用的泛型写了Integer，所以遇到了一个坑，我们设置初始值为2020，期望值2021，发现版本号修改失败   (但实际开发中一般都是一个对象)

```java
//两个参数 初始值，初始版本号
AtomicStampedReference<Integer> atomicInteger = new AtomicStampedReference<>(2020, 1);
//四个参数 期望值，修改的值，期望版本号，修改的版本号 
atomicInteger.compareAndSet(2020, 2021,atomicInteger.getStamp(), atomicInteger.getStamp()+1));
```

一个大坑：

**Integer使用了对象缓存机制，默认范围是-128—127，推荐使用静态工厂方法valueOf获取对象实例，而不是new，因为valueOf使用缓存，而new一定会创建新的对象分配新的内存空间**

阿里巴巴开法手册也有类似的提醒

![image-20200311142236816](34.png)

而CAS底层就是采用`==`来比较的

![image-20200311145651281](35.png)

正常结果得测试：

~~~java
public class CASDemo {


    public static void main(String[] args) {
        //带版本号的原子性操作
        //坑：Integer
        AtomicStampedReference<Integer> atomicInteger = new AtomicStampedReference<>(1, 1);
        //compareAndSet(int expect, int update)，expect：期望 update：更新  比较并交换

        new Thread(()->{
            //获得版本号
            int stamp = atomicInteger.getStamp();
            System.out.println("A1版本号："+stamp);
            //睡两秒，保证B线程先执行
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //四个参数：期望的值，修改的值，期望的版本号，修改的版本号
            System.out.println(atomicInteger.compareAndSet(1, 3,
                    stamp, stamp+1));
            //睡眠后的版本号
            System.out.println("A2版本号："+atomicInteger.getStamp());
        },"A").start();

        new Thread(()->{
            //获得版本号
            int stamp = atomicInteger.getStamp();
            System.out.println("B1版本号："+stamp);
            System.out.println(atomicInteger.compareAndSet(1, 2,
                    atomicInteger.getStamp(), atomicInteger.getStamp() + 1));
            System.out.println("B2版本号："+atomicInteger.getStamp());
            System.out.println(atomicInteger.compareAndSet(2, 1,
                    atomicInteger.getStamp(), atomicInteger.getStamp() + 1));
            System.out.println("B3版本号："+atomicInteger.getStamp());
        },"B").start();


    }
}

~~~



# 各种锁的理解

## 公平锁、非公平锁

公平锁：非常公平，不能插队，但当一个线程需要3小时排前面，另一个线程3秒，那必须等3小时执行完毕才能执行3秒这个线程，，所以默认就是采用非公平锁

非公平锁：不公平，可以插队

~~~java
//ReentrantLock底层就是非公平锁
public ReentrantLock() {
  sync = new NonfairSync();
}
~~~



## 可重入锁

也叫递归锁，拿到了外面的锁，也会拿到了里面的锁

~~~java
//synchronize默认是可重入锁
public class Demo01 {
    public static void main(String[] args) {
        Phone1 phone=new Phone1();
        new Thread(()->{
            phone.sendSms();
        },"A").start();
        new Thread(()->{
            phone.sendSms();
        },"B").start();

    }
}
class Phone1{

    public synchronized void sendSms(){

        System.out.println(Thread.currentThread().getName()+"发短信");
        call();
    }
    public synchronized void call(){
        System.out.println(Thread.currentThread().getName()+"打电话");
    }
}
~~~

执行结果永远都是A自动获得call的锁，会执行完发短信和打电话B才会执行

lock锁也是这样的，但是lock需要锁的配对，锁几次就解几次，不然会发生死锁

## 自旋锁

其实在之前学习CAS时就见过自旋转锁，原子性加1的底层操作就是自旋锁，不断循环迭代，直到成功

![image-20200311164115398](36.png)

Spinlock:

```java
/**
 * 自旋锁
 */
public class SpinLockDemo {

    //int 默认0
    //引用类型 Thread  默认null
    AtomicReference<Thread> atomicReference=new AtomicReference<>();

    //加锁
    public void MyLock(){
        Thread thread = Thread.currentThread();
        System.out.println(Thread.currentThread().getName()+"==>MyLock");
        //加锁就是无循环，直到解锁
        while (!atomicReference.compareAndSet(null,thread)){
        }
    }

    //解锁
    public void MyUnLock(){
        Thread thread = Thread.currentThread();
        System.out.println(Thread.currentThread().getName()+"==>MyUnLock");
        atomicReference.compareAndSet(thread,null);
    }

}
```

测试类：

```java
public class TestSpinLock {
    public static void main(String[] args) throws InterruptedException {
        //底层使用CAS自旋锁
        SpinLockDemo lock = new SpinLockDemo();
      //A线程
        new Thread(()->{
            lock.MyLock();
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                lock.MyUnLock();
            }
        },"A").start();
      //睡一秒保证A先进来
        TimeUnit.SECONDS.sleep(1);
		//B线程
        new Thread(()->{
            lock.MyLock();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                lock.MyUnLock();
            }
        },"B").start();
    }
}
```

运行结果：

![image-20200311174245552](37.png)

永远都是A先解锁，B才能解锁，因为A线程先进来执行到while()判断，因为是非运算，所以它的值为false，那么不会进入无限循环，B线程紧接着也进来了加锁时也执行到这个while()判断，但此时再一次非运算它的值为就变成true，就进入无限循环，等到A执行到finally{}解锁时重新进行CAS交换值，B线程他才会跳出循环执行下面的代码。这样就完成自定义锁。



## 死锁

> 死锁测试，怎么排除死锁

```java
public class DeadLockDemo {
    public static void main(String[] args) {
        String lockA="lockA";
        String lockB="lockB";
        new Thread(new MyThread(lockA,lockB),"T1").start();
        new Thread(new MyThread(lockB,lockA),"T2").start();

    }
}
class MyThread implements Runnable{

    private String lockA;
    private String lockB;

    MyThread(String lockA, String lockB) {
        this.lockA = lockA;
        this.lockB = lockB;
    }

    @Override
    public void run() {
        synchronized (lockA){
            System.out.println(Thread.currentThread().getName()+"lock:"+lockA+"get==>"+lockB);

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (lockB){
                System.out.println(Thread.currentThread().getName()+"lock:"+lockB+"get==>"+lockA);
            }
        }
    }
}
```



> 解决办法 查看堆栈信息

1. 使用 **jps -l** 定位进程号

![image-20200311183555294](38.png)

2. 使用**jstack 进程号**找到死锁问题

![image-20200311183843279](39.png)



