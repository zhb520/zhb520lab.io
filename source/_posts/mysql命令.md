---
categories: MySQL
tags: MySQL
date: 2020-03-24 13:32:42
description: MySQL的命令是最基础的，这篇讲解一下企业中几乎都会用到的一些命令，关于索引参考另一篇笔记“mysql索引”
---

# 操作数据库(了解)

1. 创建数据库

   ~~~mysql
   create database [if not exists] db01;
   #中括号中内容可写可不写，不写的话如果该数据库已存在就会报错，写了只会出现警告
   ~~~

2. 删除数据库

   ~~~mysql
   drop database [if exists] db01;
   drop table [if exists] 表A;  -- 删除表
   ~~~

3. 使用数据库

   ~~~mysql
   use db01;
   ~~~

4. 查看数据库

   ~~~mysql
   show database;
   ~~~

5. 创建表

   ![image-20200515120207831](image-20200515120207831.png)、

# 数据库的列类型

> 数值         

| 类型         | 字节数                                                     |
| ------------ | :--------------------------------------------------------- |
| tinyint      | 1个字节                                                    |
| smally       | 2个字节                                                    |
| mediumint    | 3个字节                                                    |
| int          | 4个字节   最常用                                           |
| bigint       | 8个字节                                                    |
| float        | 4个字节   会产生精度问题                                   |
| double       | 8个字节   会产生精度问题                                   |
| decimal(M,D) | M+D个字节，D为小数点后的位数，不会有精度问题，用作金融计算 |

> 字符串

| 类型     | 字符数              |
| -------- | ------------------- |
| char     | 0~255               |
| varchar  | 0~65535  常用       |
| tinytext | 2^8-1   微型文本    |
| text     | 2^16-1   保存大文本 |

> 时间日期

| 类型      | 格式                                 |
| --------- | ------------------------------------ |
| date      | YYYY-MM-DD，日期格式                 |
| time      | HH:mm:ss，时间格式                   |
| datetime  | 组合上面的格式，常用的格式           |
| timestamp | 时间戳，1970/1/1 0:0:0到现在的毫秒数 |
| year      | 年                                   |

> null

- 没有值，未知
- ==注意，不要使用null进行运算，结果为null==

## 字段属性

==Unsigned==：声明了该列不能为负数

==zerofill==：位数不够前面0填充

## 阿里巴巴手册创建字段约束

一个数据库标准的必须要有以下5个字段

- id，主键
- version，乐观锁
- is_delete，伪删除
- gmt_create，创建时间
- gmt_update，修改时间

# DML（增删改）

~~~mysql
-- 添加语句
-- 字段可写可不写，不写的时候，值必须要满足表中字段的个数,并且一一对应
insert into `表名` (`字段1`，`字段2`) values ('值1','值2');
insert into `表名` (`字段1`) values ('值1'),('值2');  -- 插入多个值（多行）

-- 删除语句
-- 不写where条件语句会删除整张表数据
delete from `表名` -- 删除数据后，不会对自增id的值产生影响，导致自增id不连续
truncate `表名`   -- 删除整张表数据，并且重置自增id的值

-- 修改语句
-- 可修改单个字段或多个字段，不指定where会修改所有
update `表名` set `字段1`='新值',`字段2`='新值' where id=1 

~~~



# DQL（查询语句）

~~~mysql
select [all | distinct]
[字段名 | *]
from table_name
	[left | right | inner join table_name2]  -- 联合查询
	[where ...]  -- 指定条件 
	[group by ...]  -- 指定结果根据什么字段来分组
	[having] -- 过滤分组的记录必须满足的次要条件
	[order by [字段名] [排序格式]]  -- 根据某字段升序或降序排序
	[limit [index],[N]]  -- 从下表index开始向下查询N条满足条件的数据
	
~~~

以上语句顺序严格要求，不能调换	

> 去重 distinct

当查询结果多条重复，我们只需要一条时可以使用这个

~~~mysql
select distinct `userid` from result
~~~

> 联表查询

![image-20200515161525232](image-20200515161525232.png)

~~~mysql
-- 语法：
select <select_list>
from 表A as <new name>
<left join,right join,inner join> 表B as <new name>
on <两表可以关联的字段>
where <可以选择额外的条件>
~~~



> 自连接

如有下表 user：

| uid（唯一） | pid（对应父id） | name |
| ----------- | --------------- | ---- |
| 2           | 6               | 张三 |
| 3           | 1               | 李四 |
| 4           | 3               | 王五 |
| 5           | 1               | 赵六 |
| 6           | 1               | 小明 |
| 7           | 5               | 小红 |
| 8           | 3               | 丁丁 |

每一个人都有一个自己独立的uid，如果pid为1代表是顶级的父类，不为1代表有个父亲

**要求：查询出所有的父子关系**

自己的表与自己连接，一张表拆成两张一样的表。

~~~mysql
select a.name as `父`,b.name as `子`
from user as a,user as b
where a.uid=b.pid
~~~

结果:

![image-20200515175200517.png](image-20200515175200517.png)



# 排序和分页

> order by

排序方式：升序 ASC  降序DESC

`where <条件> order by <字段名> <排序方式> `

> limit

limit必须写在所有的语句最后一行

`limit [index],[N]`：起始位置（下标）,显示条数

**假设每页显示5条:**

~~~mysql
-- 第一页：limit 0,5             (1-1)*5
-- 第二页：limit 5,5             (2-1)*5
-- 第三页：limit 10,5            (3-1)*5
-- 第N页：limit (N-1)*5,5        (N-1)*pageSize
-- 【pageSize：页面大小】
-- 【N：第N页】
-- 【(N-1)*pageSize：起始数据下标】

-- 查询：降序排序Java第一学年课程成绩排名前10的学生，并且分数要大于80的学生信息（学号，姓名，课程名，分数）
select s.studentNo,s.studentName,SubjetName,Score
from student s
inner join result r
on s.studentNo=r.studentNO
inner join subject sub
on r.subjectNo=sub.subjectNo
where SubjetName='Java第一学年'
and
where StudentResult>80
order by StudentResult DESC
limit 0,10
~~~



# 常用函数

> 数学运算

~~~mysql
select ABS(-8)   -- 返回8,绝对值
select CEILING(9.1)   -- 返回10,向上取整
select FLOOR(9.9)   -- 返回9,向下取证
select RAND()   -- 返回一个0~1之间的随机数
select SIGN(N)   -- 判断一个数的符号 0~0，负数~-1，正数~1
~~~

> 字符串函数

~~~mysql
select CHAR_LENGTH('即使再小的帆也能远航')  -- 10，判断字符串长度
select CONCAT('我','爱','你')   -- 我爱你，连接字符串
select INSERT('我爱编程',1，2,'超级')   -- 超级爱编程，从第1个字符开始换2个字符
select REPLACE('我就笑嘻嘻','嘻嘻','哈哈')   -- 我就笑哈哈，替换相应的字符
~~~

> 时间

~~~mysql
select current_date()  -- 获取当前日期 yyyy-MM-dd	
select now() -- 获取当前时间，日期加时间，精确到秒
select localtime() -- 获取本地时间
select sysdate() -- 获取系统时间
select year(now())  -- 获取年，还可以类似的方法获取其他具体时间
~~~



# 事务

> ACID原则

**原子性（Atomicity）**
原子性是指事务是一个不可分割的工作单位，事务中的操作要么都发生，要么都不发生。

例子：A给B转钱，A必须扣钱，B必须加相应的钱，要么一起成功要么一起失败。

**一致性（Consistency）**
事务前后数据的完整性必须保持一致。

例子：A给B的钱一共1000元，转完钱后总的还是1000元

**隔离性（Isolation）**
事务的隔离性是多个用户并发访问数据库时，数据库为每一个用户开启的事务，不能被其他事务的操作数据所干扰，多个并发事务之间要相互隔离。

例子：A给B转钱不会影响到C给B转钱

**持久性（Durability）**
持久性是指一个事务一旦被提交，它对数据库中数据的改变就是永久性的，接下来即使数据库发生故障也不应该对其有任何影响



> 事务的隔离级别

事务出现的问题：

**脏读**：指一个事务读取了另外一个事务未提交的数据

**不可重复读**：第一次读出来后，第二次读时发现与第一次的数据有差别（期间有人为对数据进行了更改，造成两次结果不一样），这个不是错误，只是某些场合不对

**虚读（幻读）**：是指一个事务读取到了别的事务插入数据，导致前后读取不一致，这里的不一致一般指行影响，比如别的事务插入了一条数据，造成多了一行

>  四种隔离级别设置

`set transaction isolation level `设置事务隔离级别
`select @@tx_isolation `查询当前事务隔离级别

| 设置             | 描述                                             |
| ---------------- | ------------------------------------------------ |
| Serializable     | 可避免脏读、不可重复读、虚读情况的发生（串行化） |
| Repeatable read  | 可避免脏读、不可重复读情况的发生（可重复读）     |
| Read committed   | 可避免脏读情况发生（读已提交）                   |
| Read uncommitted | 最低级别，以上情况均无法保证 （读未提交)         |

mysql的默认隔离级别就是**可重复读**，这也是因为默认的加锁方式是行锁的缘故，如果要解决幻读的问题就需要变成表锁。

> 事务相关语句

mysql默认开启事务，若想自己设置事务，需要关闭事务，在自定义一个事务并执行，为了安全，最后还需要把mysql的事务开启回来。

~~~mysql
set autocommit = 0 -- 关闭事务
set autocommit = 1 -- 开启事务
start transaction  -- 标记一个事务的开始
-- 标记事务开始后就可以编写该事物的sql语句
commit     -- 最后提交事务
rollback   -- 回滚事务

-- 以下了解即可
savepoint <保存点名字> -- 设置一个事务的保存点
rollback to <保存点名字> -- 回滚到保存点
release savepoint <保存点名字> -- 删除保存点

~~~

# MySQL索引

见另一份笔记：**[mysql索引]**





# 插入100w条语句

我们首先得去创建一个插入语句的函数，但mysql默认是不允许创建函数的，我们首先得解决这个问题。

==解决办法1==

`SET GLOBAL log_bin_trust_function_creators = 1;`

这个方法在mysql重启后就会失效；

注意： 有主从复制的时候 从机必须要设置  不然会导致主从同步失败

==解决办法2==

在my.cnf里面设置

log-bin-trust-function-creators=1

不过这个需要重启服务



设置完成后我们就可以创建函数去执行了。

==生成随机数的方法==

以下是生成6位随机数的函数：

` floor(rand()*(999999-100000)+100000)`

rand函数随机生成0~1的数字，小数点后有15位，floor为向下取整。rand()要是0.000001，乘以一个6位数，向下取整肯定也还是0，所以要加100000保证结果一定为6位数，但rand()要是0.999999，是最接近1的数，因为加了100000，所以必须保证乘后的值不大于899999，这样才能保证不会变成7位数。

**函数体：**

~~~mysql
delimiter $$  -- 写函数之前必须要写的
create function mock_data()
returns int   -- 用returns
begin
	declare num int default 1000000;
	declare i int default 0;
	while i<num do
		-- 插入语
		insert into user(name,pwd,age,gender) values(CONCAT('用户',i),
    floor(rand()*(999999-100000)+100000),  -- 随机生成6位数密码
    floor(rand()*100),  -- 最高100岁
    floor(rand()*2));   -- 要么0，要么1
		set i=i+1;
	end while;
    return i;
end;
~~~

运行一遍函数进行创建，再执行该函数，`select mock_data();`

这个时候不用主键（比如通过name）查询靠后的数据时，就会发现明显慢很多，我们可以创建索引

~~~mysql
-- 新建索引
CREATE INDEX indexName on `表名`(`字段名`)
-- 删除索引
alter table `表名` drop index indexName
~~~

没有索引就是从头开始遍历，有索引就是通过b+tree查找，大大提高查询速度。

> 索引原则

- 索引不是越多越好

- 不要对经常变动的数据添加索引
- 不要对小数据量添加索引
- 索引一般加在常用查询的字段上

