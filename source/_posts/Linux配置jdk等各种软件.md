---
categories: Linux
tags: Linux环境配置
date: 2020-02-17 12:13:50
---

# 配置JDK1.8

1. 下载 jdk-8u162-linux-x64.tar.gz（版本任意）到/opt目录下

2. 解压缩并重新命名

   ~~~shell
   cd /opt
   tar -zxvf jdk-8u162-linux-x64.tar.gz
   mv jdk1.8.0_162/ jdk1.8
   ~~~

   <!--more-->

3. 配置环境变量

   ~~~she
   vim /etc/profile
   ~~~

   ~~~java
   //在末尾添加
   JAVA_HOME=/opt/jdk1.8
   PATH=$JAVA_HOME/bin:$PATH
   export JAVA_HOME PATH //导出配置
   ~~~

4. 输入命令使环境变量生效，要么重启系统也行

   ~~~shell
   source /etc/profile
   ~~~

# 配置TomCat

1. 下载apache-tomcat-8.5.27.tar.gz（版本任意）到/opt目录下

2. 解压缩

   ~~~shell
    tar -zxvf apache-tomcat-8.5.27.tar.gz 
   ~~~

3. 检查是否安装成功

   1. 进入解压缩后的文件apache-tomcat-8.5.27

   2. 再进入bin，用ll查看文件，可以看到有一个startup.sh

   3. 直接运行该文件

      ~~~shell
      ./startup.sh
      ~~~

      ![image-20200228131021435](1.png)



# 安装MySQL

1. 下载MySQL-client-5.5.54-1.linux2.6.x86_64.rpm和MySQL-server-5.5.54-1.linux2.6.x86_64.rpm两个文件到/opt目录下（版本任意，但要兼容）

2. 进入到目录安装

   ~~~shell
   rpm -ivh MySQL-server-5.5.54-1.linux2.6.x86_64.rpm
   rpm -ivh MySQL-client-5.5.54-1.linux2.6.x86_64.rpm
   ~~~

3. 若是在安装server碰到问题

   ~~~shell
   error: Failed dependencies:
   	libaio.so.1()(64bit) is needed by MySQL-server-5.5.54-1.linux2.6.x86_64
   	libaio.so.1(LIBAIO_0.1)(64bit) is needed by MySQL-server-5.5.54-1.linux2.6.x86_64
   	libaio.so.1(LIBAIO_0.4)(64bit) is needed by MySQL-server-5.5.54-1.linux2.6.x86_64
   #只要碰到这个问题说明缺少依赖，安装依赖即可
   yum install libaio
   ~~~

5. 安装完毕输入命令查看版本即可说明安装成功

   ![image-20200228133400440](2.png)

6. 通过命令启动服务,并设置密码

   ~~~shell
   service mysql start
   mysqladmin -u root password '123456'  
   //登录
   mysql -uroot -p123456
   ~~~

7. 设置编码，重新打开一个终端

   ~~~shell
   cp /usr/share/mysql/my-huge.cnf /etc/my.cnf
   vim /etc/my.cnf
   ~~~

   ~~~java
   [client]//在这个下面添加一行
   default-character-set=utf8
   [mysqld]//在这个下面添加三行
   character_set_server=utf8
   character_set_client=utf8
   collation-server=utf8_general_ci
   [mysql]//在这个下面添加一行
   default-character-set=utf8
   
   ~~~

   ctrl+c或命令exit;退出客户端，再重启mysql服务

   ~~~shell
   service mysql restart
   ~~~

   

8. 输入命令查看mysql中的用户表，来查看谁能连接该数据库，默认的root用户只允许本机访问

   ~~~shell
   mysql> select host,user,password,select_priv from mysql.user;
   ~~~

   ![image-20200228144312518](3.png)

   

9. 添加一个权限，使得远程可以连接该数据库，”%“代表所有ip，只要在同一网段都可以访问，如过是远程虚拟机，设置了本地也访问不了

   ~~~shell
   grant all privileges on *.* to root@'%' identified by '123456'
   ~~~

   

   # 安装Redis

   1. 下载redis-3.2.5.tar.gz（版本任意）到/opt目录下

   2. 解压缩

      ~~~shell
      tar -zxvf redis-3.2.5.tar.gz
      ~~~

   3. 进入解压缩后的目录，接着在该目录下执行**make**命令进行编译文件

   4. 有的虚拟机会没有gcc和c++的支持，make就会报错说没有c的支持，那就要安装gcc和c++

      ~~~shell
      yum install gcc
      yum install gcc-c++
      ~~~

   5. 安装完毕后要先清理一边之前make所留下的残余文件之后再执行make，不然还是编译不了

      ~~~shell
      make distclean
      make
      ~~~

   6. 编译完成后最后再执行安装即可，默认安装的路径在 **usr/local/bin**

      ~~~shell
      make install
      ~~~

   7. 查看文件

      ![image-20200228150205907](4.png)

      redis-bebchmark：性能测试工具，看看自己本子的性能如何（服务器启动后运行）

      redis-check-aof：修复有问题的AOF文件，AOF持久化

      redis-check-rdb：修复有问题的RDB文件，RDB持久化

      redis-sentinel：rendis集群使用，哨兵

      redis-sercer：启动redis服务

      redis-cli：客户端启动入口

   8. 启动命令：在任何目录下都可以执行**redis-server**来启动服务，默认为前台启动，会占用当前进程而没法执行操作，应该设置成后台启动

   9. 启动前应做的操作：

      首先复制一份配置文件redis.conf到别的目录下（复制到/opt/myRedis/目录下），原先的配置文件不能动当做备份

      进入配置文件的第一件事就是把redis修改为后台启动

      进入配置文件通过查找命令“/daemonize” 把改参数的no改为yes

      启动命令：

      先启动服务器：redis-server /opt/myRedis/redis.conf

      在启动客户端 **redis-cli **-h 127.0.0.1 -p 6379（在本机上参数可省略）

   10. 关闭：**ctrl+c或exit**：退出客户端，在客户端输入**shutdown**会使服务器断开，当只开启服务端是，可以通过redis -cli 

       

       

   



