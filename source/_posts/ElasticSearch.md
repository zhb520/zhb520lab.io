---
categories: ElasticSearch
tags: 搜索引擎
date: 2020-04-20 8:21:11
description: 搜索就要用专门的技术，这篇讲解了ElasticSearch的入门知识，让你能在企业中完全会用，拓展了ELK技术的一些讲解，后期出一篇es+vue前后端分离实现访京东搜索的一个简单小项目。
---

# ElasticSearch概述

ElasticSearch，简称es，是一个开源的高扩展的**分布式全文检索搜索引擎**，它可以近乎**实时的存储、检索数据**；本身扩展性很好，可以扩展到上百台服务器，处理PB级别的数据。es也使用Java开发并使用Lucene作为其核心来实现所有索引和搜索功能，但是它的目的是通过简单的RESTful API来隐藏Lucene的复杂性，从而让全文索引变得更简单。

**ELK技术：Elasticsearch+Logstash+Kibana**

## ES和Slor的区别

他们都是使用JAVA开发的，并使用Lucene作为核心来实现所有索引和搜索功能，Lucene可以被认为迄今为止最为先进、性能最好的、功能最全的搜索引擎。但是Lucene只是一个库，想要使用它必须使用java作为开发语言将其集成到项目中，但Lucene非常复杂，需要深入了解检索的相关知识来理解它是如何工作的。

现在ES就被开发出来了，ES是通过RESTful API来隐藏Lucene的复杂性，从而让全文索引变得简单。

再这之前还有一个搜索引擎，就是Solr，但Solr对外提供的是类似于Web-service的API接口，用户可以通过http的post请求，向搜索引擎服务器提交一定格式的文件，生成索引或修改索引，通过http的get请求提出查找请求，并对返回的xml、json等格式查询结果进行解析，由于现在无论是SSM还是SpringBoot采用的都是RESTful风格，所有ES更为流行。

## ES和Solr的搜索优劣势

![image-20200526121256091](image-20200526121256091.png)

![image-20200526121314361](image-20200526121314361.png)

![image-20200526121332715](image-20200526121332715.png)

综上所述，Solr的架构不适合实时搜索的应用。



# ElasticSearch安装

声明：JDK1.8 是最低要求，安装ES客户端，界面工具！

> 下载

官网：https://www.elastic.co/cn/

下载地址：https://www.elastic.co/cn/downloads/elasticsearch

下载后直接解压，直接在bin目录下运行.bat（Windows）或.sh（Linux）文件，他的默认端口为9200，运行即可去网页查看

![image-20200526131004698](image-20200526131004698.png)



> 安装可视化界面

安装插件：head，他依赖于node.js

下载地址：https://github.com/mobz/elasticsearch-head/

安装根据github的操作执行即可，因为是国外的服务器所以install时可能会很慢，所以可以使用淘宝镜像

~~~shell
#克隆文件
git clone git://github.com/mobz/elasticsearch-head.git
#进入文件
cd elasticsearch-head
#安装淘宝镜像，输入密码即可
sudo npm install -g cnpm --registry=https://registry.npm.taobao.org --verbose
#查看安装的版本号
cnpm -v
#安装镜像不使用github上的npm install，而使用下面命令
cnpm install
#运行
npm run start
~~~

启动后他的端口是9100，而ES的默认端口是9200，就会产生跨域的问题，所以得去解决

去config文件下的`elasticsearch.yml`最后面添加配置，支持跨域就可以了

~~~yml
http.cors.enabled: true
http.cors.allow-origin: "*"
#配置玩后需重新启动es
~~~

运行localhost:9100查看结果，成功支持跨域

![image-20200526133526504](image-20200526133526504.png)

> 安装kibana

下载完成后先启动ES，再去Kibana的bin目录下启动kibana，他的默认端口是5601，进入后默认是英文，但Kibana是有提供中文的，可以去config文件中修改`Kibana.yml`文件在末尾添加上`i18n.locale: "zh-CN"`即可

运行结果：

![image-20200526145336267](image-20200526145336267.png)



# ES核心概念

> 概述

前面已经了解了什么是es，以及他的安装和启动，那么es是如何去存储数据，数据结构是什么，又是如何实现搜索的呢？

**集群，节点，索引，类型，文档，分片，映射是什么**

> 关系行数据库和es客观的对比

| Relational DB | Elasticsearch   |
| ------------- | --------------- |
| 数据库        | 索引            |
| 表            | types ：类型    |
| 行            | documents：文档 |
| 字段          | field           |

> 类型

类型是文档的逻辑容器，就想关系型数据库，表格是行的容器。类型中对于字段的定义称为映射，比如name映射为字符串类型。

> 文档

就是一条条数据

elasticsearch是面向文档的，那就意味着索引和搜索数据的最小单位是文档，elasticsearch中，文档有几个重要的属性：

- 自我包含，一篇文档同时包含字段和对应的值，也就是同时包含key:value
- 可以是层次型的，一个文档中包含自文档，复杂的逻辑实体就是这么来的 {就是Json对象}
- 灵活的结构，文档不依赖预先定义的模式，我们知道关系型数据库中，要提前定义字段才能用，而在 es中，对于字段非常灵活，有时候，我们可以忽略该字段，或者动态添加一个字段

> 索引

索引就相当于数据库，包含了多个文档，，比如文档1，文档2，当我们索引一篇文档时，可以通过这样一个顺序找到它：索引-->类型-->文档ID，通过这个组合就能索引到某个具体的文档。注：ID不必是整数，实际上它是个字符串。

索引存储了映射类型的字段和其他设置。然后被保存在各个分片中，接下来研究分片是如何工作的

**物理设计：节点和分片  如何工作？**

一个集群至少有一个节点，而一个节点就是一个elasticsearch进程，节点可以有多个索引默认的，如果你创建索引，那门索引将会有5个分片构成的（又称主分片），每一个主分片会有一个副本（又称复制分片）

> 倒排索引

es使用的是一种称为倒排索引的结构，也就方向索引，采用Lucene倒排索引作为底层。这种结构适用于快速的全文索引。

为了创建倒排索引首先要将每个文档拆分成独立的词，然后创建一个包含所有不重复的词条的排序列表，然后列出每个词条出现在哪个文档，说白了就是把内容作为key，文档ID作为value，最后查找某个词的时候找到对应的文档ID即可。



# IK分词器

分词：即把一段中文或者别的划分成一个个关键字，我们在搜索的时候会把自己的信息进行分词，会把数据库中或者索引库只能怪的数据进行分词，然后进行一个匹配的操作。

ik分词器提供了两种算法：ik_smart和ik_max_word，其中ik_smart是最少切分，ik_max_word为最细粒度划分！

>安装

1. 下载地址：https://github.com/medcl/elasticsearch-analysis-ik/releases

2. 下载完毕后，解压放入elasticsearch插件文件夹（plugins）中即可，可以为解压后的文件重新命名为`ik`

3. 重启es和kibana，进入kibana测试

4. ik_smart最少切分，所有可能的字段不会重复，最多按字隔开变成单个字

   ![image-20200527133625839](image-20200527133625839.png)

5. ik_max_word最细粒度切分,找出所有的可能性，所有字段可能用到相同的字，按所有可能会出现的词汇来分

   ![image-20200527133738015](image-20200527133738015.png)

> 输入 超级喜欢迪丽热巴

![image-20200527134309650](image-20200527134309650.png)

发现问题：迪丽热巴四个字给分开了，因为他的字典中没有这个词汇的存在，所有我们对于有些词汇需要自己加到字典中

> ik分词器增加自己的配置

ik的config文件中有`IKAnalyzer.cfg.xml`这个文件，我们可以在里面添加自己的字典，首先写一个自己的字典，以dic结尾，其实就是文本文档，在里面输入迪丽热巴，接着在配置文件中添加自己的文档

![image-20200527134828400](image-20200527134828400.png)

![image-20200527135042196](image-20200527135042196.png)

> 重新启动并测试

![image-20200527135513862](image-20200527135513862.png)



# 关于索引的操作

**Rest风格基本命令**

| method |                     url地址                     |          描述          |
| :----: | :---------------------------------------------: | :--------------------: |
|  PUT   |     localhost:9200/索引名称/类型名称/文档id     | 创建文档（指定文档id） |
|  POST  |        localhost:9200/索引名称/类型名称         | 创建文档（随机文档id） |
|  POST  | localhost:9200/索引名称/类型名称/文档id/_update |        修改文档        |
| DELETE |     localhost:9200/索引名称/类型名称/文档id     |        删除文档        |
|  GET   |     localhost:9200/索引名称/类型名称/文档id     |   查询文档通过文档id   |
|  POST  |    localhost:9200/索引名称/类型名称/_search     |      查询所有数据      |

> 添加索引

1. 创建一个索引

   ~~~yml
   PUT /索引名/类型名/文档id
   {请求体，json格式}
   ~~~

   ![image-20200527144311874](image-20200527144311874.png)

   上图的右边可以看到：_version版本号为1，result为created；

   在这里我们没有为其参数赋上类型，我们可以也可以创建索引插入文档前定义索引的规则

   ~~~http
   PUT /text2
   {
     "mappings": {
       "properties": {
         "name": {
           "type": "text"
         },
         "age": {
           "type": "long"
         },
         "birthday": {
           "type": "date"
         }
       }
     }
   }
   ~~~

   **这里需要说到两个mapping的索引类型，即type的类型**

   - text：会被分词器分开
   - keyword：不会被分词器分开

2. 在es7.0之后，在put时候已经渐渐弃用设置type，而是有默认参数`_doc`

   ~~~yml
   PUT /索引名/_doc/文档id
   {请求体，json格式}
   ~~~

> 修改索引

有两种方法：

1. 覆盖原来的数据，这个时候要注意，要是参数不对应，没写的参数就会被删除，不建议这种写法

   ~~~http
   PUT /test1/_doc/1
   {
     "name": "张三"
     "age": 23
   }
   ~~~

2. 用POST更新，用该方法，只修改doc里面写了的参数，没写的不会修改

   ~~~http
   #过时的写法
   POST /text1/_doc/1/_update
   {
     "doc": {
       "name": "法外狂徒"
     }
   }
   #新的语法
   POST /text1/_update/1
   {
     "doc": {
       "name": "法外狂徒"
     }
   }
   ~~~

> 删除索引

~~~http
DELETE test1
~~~



# 关于文档的操作（查找）

> 基本操作

~~~http
#根据id查找
GET /zhb/user/1
#添加查询语句，直接可以进行模糊查询
GET /zhb/user/_search?q=name:张三
~~~

> 复杂查询

~~~http
GET /zhb/user/_search
{
  "query": {
    "match": {
      "name": "张"
    }
  }
}
~~~

查询结果返回json

![image-20200527205906502](image-20200527205906502.png)

> 查询指定的字段

使用`_source`查询指定字段

~~~http
GET /zhb/user/_search
{
  "query": {
    "match": {
      "name": "张三"
    }
  },
  "_source": ["name","age"]
}
~~~

> 查询结果进行排序

使用`sort`参数进行排序，同样是desc（降序）和asc（升序），由于使用了排序，那么结果得_score的分值就为null了

~~~http
GET /zhb/user/_search
{
  "query": {
    "match": {
      "name": "张三"
    }
  },
  "_source": ["name","age"],
  "sort": [
    {
      "age": {
        "order": "desc"
      }
    }
  ]
}
~~~

> 分页

跟上from和size参数，这两个参数就跟mysql的limit的两个参数，from是从第几条开始，size是显示几条

~~~http
"from": 0,
"size": 1
~~~

> 布尔值查询

`must`命令：所有的条件必须满足，类似于mysql的and，

~~~http
GET /zhb/user/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "name": "张三"
          }
        },
        {
          "match": {
            "age": "65"
          }
        }
      ]
    }
  }
}
~~~

`should`命令：相当于mysql的or

`must_not`命令：相当于mysql的not

> 过滤

使用`filter`进行过滤

- gt，gte：大于，大于等于
- lt，lte：小于，小于等于

~~~http
GET /zhb/user/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "name": "张三"
          }
        }
      ],
      "filter": [
        {
          "range": {
            "age": {
              "gte": 10,
              "lte": 30
            }
          }
        }
      ]
    }
  }
}

~~~

> 单个字段匹配多个值

匹配的时候分词器也会有作用，输入单个字会把含有该字的词也查出来，而且可以用空格查询多个值

~~~http
GET /zhb/user/_search
{
  "query": {
    "match": {
      "tags": "男 技术"
    }
  }
}
~~~

> 精确查询

`term`命令，直接是通过倒排索引进行查询

**关于分词**

- term：精确查询，文档中的词汇要是进行了分词就会查不到，比如`term：超级喜欢`，而当文档存入超级喜欢时会被分词器分成`超级`和`喜欢`，那么使用term就会查询不到这条数据，除非这个字段使用keyword类型，这样就不会被分词器分开
- match：会使用分词器！

> 高亮查询

![image-20200527222156141](image-20200527222156141.png)

发现匹配出来的都要一个叫highlight的字段，里面满足的每一个结果都加一个<em>的html标签。

**自定义高亮条件**

~~~http
"highlight": {
	"pre_tags": "<p class='key' style='color:red'>",
	"post_tags": "</p>", 
	"fields": {
	"name":{}
}
  
~~~



# 集成SpringBoot

> 找文档

在官方找到文档

![image-20200528102431934](image-20200528102431934.png)

只要找到REST API的文档，根据文档进行使用即可

> 添加依赖

~~~xml
<dependency>
    <groupId>org.elasticsearch.client</groupId>
    <artifactId>elasticsearch-rest-high-level-client</artifactId>
    <version>x.x.x</version>
</dependency>
~~~

当然如果直接在勾选依赖时勾选了NoSQL里的elasticsearch就不用添加该依赖了，但是要注意的是添加的是自己需要的版本，保证和本地使用的es版本一致。

![image-20200528111333495](/image-20200528111333495.png)

因为我本地使用的是es版本时7.7.0，所以要自定义一下版本号

~~~xml
<properties>
  <java.version>1.8</java.version>
  <!--自定义es的版本，保证和本地一致-->
  <elasticsearch.version>7.7.0</elasticsearch.version>
</properties>
~~~

最后保证同步完成不报错即可。

> 创建对象实例

![image-20200528102921288](image-20200528102921288.png)

~~~java
@Configuration
public class ElasticsearchClientConfig {
    @Bean
    public RestHighLevelClient restHighLevelClient() {
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));
        return client;
    }
}

~~~

> 测试索引API

~~~java
@Autowired
@Qualifier("restHighLevelClient")
private RestHighLevelClient client;

//创建索引 
@Test
    void testCreatIndex() throws IOException {
        CreateIndexRequest request = new CreateIndexRequest("zhb_index");
        CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
    }

//查找有无改索引
@Test
	void testExistIndex() throws IOException {
  		GetIndexRequest request = new GetIndexRequest("text1");
  		boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
  		System.out.println(exists);
  }
//删除索引
@Test
	void  testDeleteIndex() throws IOException{
  		DeleteIndexRequest request = new DeleteIndexRequest("text1");
  		AcknowledgedResponse delete = client.indices().delete(request, RequestOptions.DEFAULT);
  		System.out.println(delete.isAcknowledged());
}
~~~



> 创建文档

~~~java
@Test
void testAddDocument() throws IOException {
  //创建对象
  User user = new User("zhb", 24);
  //创建请求
  IndexRequest request = new IndexRequest("user_index");
  request.id("1");
  request.timeout(TimeValue.timeValueSeconds(1));

  request.source(JSON.toJSONString(user), XContentType.JSON);

  //客户端发送请求,获取相应结果
  IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
  System.out.println(indexResponse.toString());
  System.out.println(indexResponse.status());//返回命令的状态，第一次就会显示CREATED
}
~~~

> 测试文档是否存在

~~~java
//判断是否存在文档
@Test
void testIsExists() throws IOException {
  GetRequest getRequest = new GetRequest("user_index", "1");
  //不获取返回的_source 的上下文效率更高
  getRequest.fetchSourceContext(new FetchSourceContext(false));
  getRequest.storedFields("_none_");
  boolean exists = client.exists(getRequest, RequestOptions.DEFAULT);
  System.out.println(exists);
}
~~~

> 更新文档

~~~java
//更新文档
@Test
void testUpdate() throws IOException {
  UpdateRequest request = new UpdateRequest("user_index", "1");
  request.timeout("1s");
  User user = new User("new name", 18);
  request.doc(JSON.toJSONString(user), XContentType.JSON);
  UpdateResponse update = client.update(request, RequestOptions.DEFAULT);
  System.out.println(update.status());//查看更新状态
}
~~~

> 删除文档

~~~java
//删除文档
@Test
void testDelete() throws IOException {
  DeleteRequest request = new DeleteRequest("user_index", "1");
  request.timeout("1s");
  DeleteResponse delete = client.delete(request, RequestOptions.DEFAULT);
  System.out.println(delete.status());//查看更新状态
}
~~~

> 批量操作

```java
//批量插入
@Test
void testBulkRequest() throws IOException{
    BulkRequest bulkRequest = new BulkRequest();
    bulkRequest.timeout("10s");
    ArrayList<User> users=new ArrayList<>();
    users.add(new User("zhb1",1));
    users.add(new User("zhb2",2));
    users.add(new User("zhb3",3));
    users.add(new User("zhb4",4));
    users.add(new User("zhb5",5));

    for (int i = 0; i <users.size() ; i++) {
      //批量删除和更新就在这里修改相应的add方法里的内容即可，这是一个重载的方法
        bulkRequest.add(
                //插入到哪个索引
                new IndexRequest("user_index")
          			//如果不设置id，会默认生成随机id
                .id(""+(i+1))
                .source(JSON.toJSONString(users.get(i)),XContentType.JSON));
    }
    BulkResponse bulk = client.bulk(bulkRequest, RequestOptions.DEFAULT);
    System.out.println(bulk.hasFailures());//输出是否失败，返回false表示成功
}
```

> 查找

~~~java
/**
   * 查找
   * new SearchRequest() 创建搜索请求
   * SearchSourceBuilder() 创建搜索条件
   * QueryBuilders.termQuery() 精确查找
   * QueryBuilders.matchAllQuery() 匹配所有
   */
void testSearch() throws IOException {
  SearchRequest request = new SearchRequest("user_index");
  //构建搜索条件
  SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
  //查询条件，我们可以使用QueryBuilders工具来实现

  TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "zhb1");
  sourceBuilder.query(termQueryBuilder);
  sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

  request.source(sourceBuilder);
  SearchResponse result = client.search(request, RequestOptions.DEFAULT);
  System.out.println(JSON.toJSONString(result.getHits()));
  System.out.println("============");
  for (SearchHit hit : result.getHits()) {
    System.out.println(hit.getSourceAsMap());
  }
}
~~~



