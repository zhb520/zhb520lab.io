---
categories: JAVA
tags: 网络会话技术
date: 2020-02-07 12:45:25
---



# Cookie技术

**什么是会话呢？**

​    所谓的会话过程就是指从打开浏览器到关闭浏览器的过程。

**Cookie和Session指的又是什么呢？**

​	Cookie和Session是域对象。所谓域就相当于给存储的内容设置一个边界，将存储的内容存储到这片区域内，那么在一次会话中，我们就可以从域中读取信息，而且读取速度快，而不用去数据库读，增大io压力。

<!--more-->

​	只在一次会话中有用：浏览器第一次给服务器发送请求，会话建立，直到有一方断开，会话结束（不同浏览器属于不同会话）

- 使用步骤：

  1. 创建Cookie对象，绑定数据

     **new Cookie(String name,String value)**

  2. 发送Cookie对象

     **response.addCookie(Cookie cookie)**

  3. 获取Cookie，拿到数据,最后进行遍历即可

     **cookie[] request.getCookies()**

当客户端发送一次请求，服务器会响应并把cookie保存在**响应头**里发送到客户端：**set-cookie:key=value**

当客户端第二次发送请求，客户端会在**请求头**里存放cookie信息发送到服务器：**cookie:key=value**

- cookie的细节

  1. 一次可以发送多个cookie

     会在响应头里有多个**set-cookie:key=value**

     并且在第二次请求的请求头里有**cookie:key1=value1;key2=value2**

  2. cookie在浏览器中能保存多长时间？

     默认情况下，浏览器关闭，Cookie数据被销毁

     可以设置Cookie的生命周期，持久化储存：

     **setMaxAge(int seconds)**

     - 正数：将Cookie数据写到硬盘中，单位为秒。
     - 负数：默认值
     - 零：立马删除Cookie信息，一般用作于用户登出

  3. cookie共享问题？

     - 假设在一个TomCat服务器部署了多个web项目，默认情况cookie不会在另一个web项目里共享

       但可以设置**setPath(String path)**设置cookie的取值范围，默认值就是当前项目目录

       当设置为**setPath(“/”)**就会使根目录下的localhost:8080/的所有项目都共享

     - 多个TomCat部署的项目也可以共享

       可以通过**setDomain(String Path)**设置一级域名相同，那多个服务器就可以共享了

       比如设置了**setDomain(“.baidu.com”)**，那么tieba.baidu.com(贴吧)和news.baidu.com(x新闻)就可以共享了

       

# Session技术

待补充。。。。