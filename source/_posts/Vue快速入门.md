---
categories: Vue
tags: 前端框架
date: 2021-06-04 16:48:02
description: Vue是一门前端框架，实现了前后端分离和快速开发，本篇blog快速带你入门vue，并能看懂大部分企业的Vue前端项目。
---

# 聊聊前端UI框架

1. Ant-Design是阿里巴巴出品，基于React的UI框架
2. iView，ElementUI，ice都是饿了吗出品的一个基于vue的UI库
3. VanUI是有赞前端团队基于有赞统一规范实现的vue组件库，提供了一整套UI基础组件和业务组件，可以快速搭建出风格统一的页面
4. Bootstrap是Twitter退出的前端开发的开源工具包
5. AmazeUI，一款轻松上手的HTML5跨屏前端框架

# vue的安装

文档：https://cn.vuejs.org/v2/guide/installation.html

1. 可以在文档中找到下载路径，有开发版本和生产版本。

2. 如果不想下载可以使用CDN

   ~~~html
   <!-- 开发环境版本，包含了有帮助的命令行警告 ,使用最新版本-->
   <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
   <!-- 生产环境版本，优化了尺寸和速度 -->
   <script src="https://cdn.jsdelivr.net/npm/vue"></script>
   <!--对于生产环境，我们推荐链接到一个明确的版本号和构建文件，以避免新版本造成的不可预期的破坏：-->
   <script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
   ~~~

## Vue Devtools

在使用 Vue 时，推荐在你的浏览器上安装 [Vue Devtools](https://github.com/vuejs/vue-devtools#vue-devtools)。它允许你在一个更友好的界面中审查和调试 Vue 应用。

如果使用github下载并安装的话可以参考网上的一些教程，当然不安装也不影响学习。

当然还有简单的方法，直接通过火狐或者chrome插件搜索，直接安装即可。

火狐插件：https://addons.mozilla.org/zh-CN/firefox/addon/vue-js-devtools/?src=search

谷歌插件：https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd

安装完毕后按f12打开开发者模式就会看到多了一个Vue的调试框。

# 基本语法使用

Vue.js 的核心是一个允许采用简洁的模板语法来声明式地将数据渲染进 DOM 的系统

**下列代码建议新建一个空文件，并新建html文件复制代码进行测试，根据浏览器显示的内容更好理解！**

> hello vue

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <meta charset="UTF-8">
    <title>Title</title>
</head> 
  
<body>
<div id="app">
    {{ message }}  
</div>
</body>
<script>
    var app = new Vue({
        el: '#app',   //使用el绑定当前<div>元素
        data: {
            message: 'Hello Vue!'
        }
    })
</script>
</html>
~~~

> v-bind标签使用

~~~html
<!DOCTYPE html>
<!--加上这个xmlns命名空间-->
<html lang="en" xmlns:v-bind="http://www.w3.org/1999/xhtml">
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
  
<body>
<div id="app">
  <span v-bind:title="message">
    鼠标悬停几秒钟查看此处动态绑定的提示信息！
  </span>
</div>
</body>
<script>

    var app = new Vue({
        el: '#app',
        data: {
            message: '页面加载于 ' + new Date().toLocaleString()
        }
    })
</script>
</html>
~~~

> 判断   v-if

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <meta charset="UTF-8">
    <title>Title</title>

</head>
<body>
<div id="app">
    <h1 v-if="number===1">1</h1>
    <h1 v-else-if="number===2">2</h1>
    <h1 v-else>NAN</h1>
</div>
</body>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            number: 1
        }
    })
</script>
</html>
~~~

> 循环   v-for

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div id="app">
<ol>
    <li v-for="item in items">
        {{item.message}}
    </li>
</ol>

</div>

<script>
    var app=new Vue({
        el: "#app",
        data: {
            items: [
                {message: "java"},
                {message: "python"},
                {message: "php"}
            ]
        }

    });
</script>
</body>
</html>
~~~

> 事件   methods

通过编写methods方法，在使用`v-on`来绑定方法

~~~html
<!DOCTYPE html>
<html lang="en" xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div id="app">
    <button v-on:click="sayHi">点击出现弹窗</button>
</div>

<script>
    var app=new Vue({
        el: "#app",
        data: {
            message: "hello alter"
        },
        methods: {
            sayHi: function () {
                alert(this.message)
            }
        }
    });
</script>
</body>
</html>
~~~



# Vue双向绑定

> 什么是双向绑定

Vue.js是一个MVVM（Model-View-ViewModel）框架，视图与数据不会进行直接的交互，只能通过ViewModel中间件来实现数据的传输，双向绑定就是当数据发生改变，视图立马发生改变，而Vue的精髓就是当视图发生改变时，数据也会发生改变。

值得注意的是，所说的数据双向绑定是对于UI控件来说的，非UI控件不会涉及到数据双向绑定。

> 怎么双向绑定

可以使用`v-model`指令在表单`<input>`、`<textarea>`及`<select>`元素上创建双向绑定。它会根据控件类型自动选取正确的方法来更新元素。

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div id="app">
    请输入你要输入的内容：<input type="text" v-model="message">  文本框中的内容：{{message}}
</div>

<div id="app2">
    爱好：
    <input type="radio" name="aihao" value="看书" v-model="hobby">看书
    <input type="radio" name="aihao" value="运动" v-model="hobby">运动
    <input type="radio" name="aihao" value="游戏" v-model="hobby">游戏
    <br>
    你选择了：{{hobby}}
</div>

<script>
    var app=new Vue({
        el: "#app",
        data: {message: ""}
    });
    var app2=new Vue({
        el: "#app2",
        data: {hobby: ""}
    });
</script>
</body>
</html>
~~~

# Vue组件   component

组件就是可以抽取了公共部分，比如一个页面的标题，侧边栏，定义一个组件能达到代码复用的效果。使用Compent实现

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div id="app">
    <zhb v-for="item in items" v-bind:list="item"></zhb>
</div>
<script>
    Vue.component("zhb",{
        props: ["list"],
        template: "<li>{{list}}</li>"
    });
    var app=new Vue({
        el: "#app",
        data: {
            items: ["java","php","linux"]
        }
    });
</script>
</body>
</html>
~~~

上述例子组件参数说明：

**基本格式：Vue.component("组件名称",{键值对形式填写参数})**

props：负责接收v-bind绑定过来的数据

template：模板样式，实际就是一些标签

## 组件名大小写

定义组件名的方式有两种：

### 使用短横线分隔命名

```
Vue.component('my-component-name', { /* ... */ })
```

当使用短横线分隔命名 定义一个组件时，你也必须在引用这个自定义元素时使用短横线分隔命名，例如 `<my-component-name>`。

### 使用首字母大写命名

```
Vue.component('MyComponentName', { /* ... */ })
```

当使用首字母大写命名定义一个组件时，你在引用这个自定义元素时两种命名法都可以使用。也就是说 `<my-component-name>` 和 `<MyComponentName>` 都是可接受的。注意，尽管如此，直接在 DOM (即非字符串的模板) 中使用时只有短横线分隔命名是有效的。



# Axios异步通信

Axios是一个开源的可以用在浏览器端和NodeJS的异步通信框架，它主要作用就是实现AJAX异步通信，器功能特点如下：

- 从浏览器中创建XMLHttpRequests
- 从node.js创建http请求
- 支持Promise API【JS中的链式编程】
- 拦截请求和响应
- 转换请求数据和响应数据
- 取消请求
- 自动转换Json格式
- 客户端支持防御XSRF（跨站请求伪造）

中文文档：http://www.axios-js.com

使用前得先安装

~~~shell
 npm install axios 
~~~

**要是与vue结合的还得参考文档来使用：http://www.axios-js.com/zh-cn/docs/vue-axios.html**

下面完成一个实例使用异步获取一个文件夹

1. 新建一个data.json的文件夹，这个文件只是存放一些信息，我们要使用异步去获取到该文件夹，并进行读取

   ```json
   {
     "name": "想吃饼干吗",
     "url": "zhb520.gitlab.io",
     "page": 1,
     "isNoProfit": true,
     "address": {
       "street": "小田村",
       "city": "浙江台州",
       "country": "中国"
     },
     "links": [
       {
         "name": "百度",
         "url": "https://www.baidu.com"
       },
       {
         "name": "vue",
         "url": "https://cn.vuejs.org"
       }
     ]
   }
   ```

2. 使用钩子函数编写方法

   ~~~html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <!--
   		v-clock解决页面刷新信息加载时闪烁问题（当没获取到信息时会显示{{info.name}}）
   		实际上就是当还没有加载出来，我们设置为隐藏即可
   		-->
       <style>
           [v-clock]{
               display: none;
           }
       </style>
   </head>
   <body>
   <div id="app" v-clock>
       {{info.name}}
   </div>
   
   <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
   <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
   <script>
       var vm =new Vue({
           el: "#app",
           data(){ //返回数据，可以自定义模板
               return{
                   info: {
                       name: null,
                       address: {
                           street: null,
                           city: null,
                           country: null
                       },
                       url: null
                   }
               }
           },
           mounted(){ //钩子函数
               axios
                   .get("./data.json")//获取到当前目录下的改文件
                   .then(response=>(this.info=response.data));
           }
       });
   </script>
   </body>
   </html>
   ~~~

## 钩子函数

上面我们用到了一个新的知识：钩子函数，那什么是钩子函数呢？

![Vue什么周期及钩子函数](1.jpg)

从上图中我们可以看到Vue的生命周期，我们可以使用钩子函数可以在生命周期的某个阶段对数据进行操作。



# 计算属性

计算属性重点突出在`属性`上，首先，他是个属性，其次是这个属性必须具有计算能力，这里的计算就是一个`函数`，他的作用就是讲计算结果缓存起来，转化为静态属性，类似于缓存。

我们使用计算当前时间来体会一下

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div id="app">
    使用普通方法获取当前的时间戳为：{{currentTime1()}}<br>
    使用计算属性获取当前的时间戳为：{{currentTime2}}
</div>

<script>
    var app=new Vue({
        el: "#app",
        data:{
          message: "hello"
        },
        methods:{
            currentTime1: function () {
                return new Date().toLocaleString();
            }
        },
        computed: {
            currentTime2: function () {
              	this.message;
                return new Date().toLocaleString();
            }
        }
    });
</script>
</body>
</html>

~~~

**方法和计算属性两者区别：**

在上面`currentTime1`是方法，`currentTime2`是计算属性

1. 在我们调用方法时，必须使用`currentTime1()`来得到结果，需要加上小括号，如果你没有加小括号那么就会报`function(){[native code]}`错误，而计算属性`currentTime2`不需要小括号。

2. 两种方法不能重名，如果重名，会优先获取方法。

3. 计算属性只要不刷新就只会获取一次，而调用方法时，会实时更新，从下图我们可以看出，计算属性因为有缓存，时间一直没变

   ![image-20200610160116802](image-20200610160116802.png)

   **注意：**

   当计算属性中有其他参数，比如例子中的message，但我们在控制台修改了该参数，时间就会重新计算，这个原理跟缓存是一样的。

**结论：**

​	调用方法时，每次都需要计算，既然是计算肯定会产生开销，如果这个结果不需要经常的变化，那么我们就可以采用计算属性来把它变成缓存，以节约我们的开销。



# Vue插槽slot

插槽的标签就是`<slot></slot>`，需要写在组件里，它的功能就是把html页面中需要经常变动的数据，或者需要动态获取的数据，比如列表，利用插槽来占位，再通过另外的组件进行替代，有点像代码的抽取，以达到复用的地步。

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div id="app">
    <todo>
        <todo-title slot="todo-title" v-bind:title="subject"></todo-title>
        <todo-list slot="todo-list" v-for='items in list' v-bind:item="items"></todo-list>
    </todo>

</div>

<script>
    Vue.component("todo",{
        template: '<div>' +
            '<slot name="todo-title"></slot>' +
                '<ul>' +
                '<slot name="todo-list"></slot>' +
                '</ul>' +
            '</div>'
    });

    Vue.component("todo-title",{
        props: ["title"],
        template: "<h2>{{title}}</h2>"
    })
    Vue.component("todo-list",{
        props: ["item"],
        template: "<li>{{item}}</li>"
    })

    var app=new Vue({
        el: "#app",
        data: {
            list: ["java","PHP","python"],
            subject: "科目列表"
        }
    });
</script>
</body>
</html>
~~~

结果展示：

![image-20200610191600672](image-20200610191600672.png)



# 自定义事件分发

> 需求

自定义事件是一个比较绕的地方，因为他是三个地方的数据绑定，通过view来做桥梁绑定component和viewModel。

比如在上面的例子中，为每一个列表后面添加一个删除按钮，实现删除当前元素，界面展示如下：

![image-20200610191714210](image-20200610191714210.png)

> 分析

因为前端只能绑定`var app=new Vue({})`中的数据，而我们for循环内容却在组件`Vue.component("todo-list",{})`中，这两者之间是不能调用方法的，前面我们知道了使用`v-on`可以使界面跟Vue函数绑定，而且又可以通过`v-bind`使界面跟组件绑定，这样数据我们就可以通过前端视图view来进行传递，接下来就是调用函数的问题，至于函数的调用vue有专门的方法就是this.$emit()

> 实现

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<div id="app">
    <todo>
        <todo-title slot="todo-title" v-bind:title="subject"></todo-title>
        <todo-list slot="todo-list" v-for='(items,index) in list'
                   :item="items"  @remove="removeList(index)">  
        </todo-list>
    </todo>

</div>

<script>
  //总组件
    Vue.component("todo",{
        template: '<div>' +
            '<slot name="todo-title"></slot>' +
                '<ol>' +
                '<slot name="todo-list"></slot>' +
                '</ol>' +
            '</div>'
    });
	//标题组件
    Vue.component("todo-title",{
        props: ["title"],
        template: "<h2>{{title}}</h2>"
    })
  //列表组件
    Vue.component("todo-list",{
        props: ["item"],
        template: "<li>{{item}}<button @click='MyRemove' >删除</button></li>",
        methods: {
            MyRemove: function (index) {
                this.$emit('remove',index);//注意：这里参数remove就会对应前端的@remove
            }
        }
    })
	//Vue实例函数
    var app=new Vue({
        el: "#app",
        data: {
            list: ["java","PHP","python"],
            subject: "科目列表"
        },
        methods: {
            removeList: function (index) {
                this.list.splice(index,1);//参数0不删除，1删除当前，2删除当前加后面的
            }
        }
    });
</script>
</body>
</html>
```

# Vue入门小结

至此我们vue的入门算是学完了，核心就是数据驱动和组件化。

常用的属性：

- v-if
- v-else-if
- v-else
- v-for
- v-on    绑定事件（methods），简写成`@`
- v-model   数据的双向绑定
- v-bind  给组件绑定参数，简写成`：`

组件化：

- 组合组件slot插槽
- 组件内部绑定事件需要使用`this.$emit("事件名",参数)`
- 计算属性的特色：缓存

使用vue就可以完成了前后端分离：后端人员只需要关注Vue实例函数中的data属性，可以通过Axios异步通信传递数据，使用`data(){}`自定义模板选择需要返回的数据，根本不需要关注前端张什么样，你只需要传递相应的参数即可。

# 真正开发

​		Vue的开发都是基于NodeJS，实际开发采用vue-cli脚手架开发，vue-router路由，vuex做状态管理；Vue UI界面我们一般使用ElemenyUI（饿了吗出品）,或者ICE（阿里巴巴出品）来快速搭建前端项目

官网：

- https://element.eleme.cn/#/zh-CN
- https://ice.work/



# 第一个Vue-cli项目

vue-cli是官方提供的一个脚手架，用于快速生成一个vue的项目模板

**主要功能**：

- 统一的目录结构
- 本地调试
- 热部署
- 单元测试
- 集成打包上线

## 安装

~~~shell
#安装淘宝镜像加速cnpm
sudo npm install -g cnpm --registry=https://registry.npm.taobao.org --verbose
#使用淘宝镜像下载vue-cli
cnpm install vue-cli -g
#测试是否安装成功
vue list
#弹出下面内容则代表安装成功
Available official templates:

  ★  browserify - A full-featured Browserify + vueify setup with hot-reload, linting & unit testing.
  ★  browserify-simple - A simple Browserify + vueify setup for quick prototyping.
  ★  pwa - PWA template for vue-cli based on the webpack template
  ★  simple - The simplest possible Vue setup in a single HTML file
  ★  webpack - A full-featured Webpack + vue-loader setup with hot reload, linting, testing & css extraction.
  ★  webpack-simple - A simple Webpack + vue-loader setup for quick prototyping.
~~~

## 创建项目

我们可以选择一个文件夹采用shell命令进行创建

~~~shell
#选择一个目录进行创建vue程序，myvue是项目名
vue init webpack myvue
~~~

期间会有很多选择，比如是否一键安装路由这些，我们设置为No，自己一步一步安装体会一下安装过程。

![image-20200611154548957](image-20200611154548957.png)

我们进入myvue目录查看

![image-20200611154655250](image-20200611154655250.png)

在该目录的终端下执行安装依赖包的命令，安装的东西根据`package.json`来决定的

~~~shell
npm install
~~~

这些安装完毕后都在`node_modules`里面。

**运行**

可以在当前目录的终端输入`npm run dev`运行，默认端口是8080，也可以用IDEA打开这个项目，在左下角的Terminal输入该命令，输入该命令能运行项目的前提是IDEA是用管理员权限打开的，windows是需要在属性中设置，mac默认为管理员。

**运行结果**

![image-20200611174236378](image-20200611174236378.png)

> 看一下文件具体怎么排版

<img src="image-20200611174544889.png" alt="image-20200611174544889" style="zoom:50%;" />

1. 让我们先进入`index.html`看一下主界面是什么样子！

   ![image-20200611210100015](image-20200611210100015.png)

   我们可以看到整个界面只有一个`<div id="app"></div>`，那么是怎么实现我们界面的显示的呢，其实主要就是通过组件。

2. 主要的文件就在`src`目录下，我们通过`main.js`进入查看程序

   ![image-20200611210149599](image-20200611210149599.png)

   从图中我们看到主要两个东西：

   - components: { App }，意思为引入一个叫APP的组件，组件可以写在别的文件中，也可以写在本文件中，而APP这个组件是哪里来的？

     该主程序使用`import APP from './App'`表示从App.vue中获取到template并命名为APP。

   - template: '<App/>'，表示用组件里的模板替换`index.html`里面的`<div id="app"></div>`

3. 那么App.vue里面是什么？

   ![image-20200611210816602](image-20200611210816602.png)

   上面看到export default 定义了一个name: 'APP'，这个目的就是向外暴露当前模板并命名为APP，为了调试的时候能准确找到当前模板，因为这里有太多命名为App，所以假如我们把它改成App01，我们用**Vue Devtools**看下调试结果：

   ![image-20200611212405505](image-20200611212405505.png)

   我们看到一个App01的模板，所以当模板比较多了以后，我们就可以准确定位到我们需要的模板。

就是这样一个模板嵌套一个，所有的组装起来就是一个完整的页面。

**至于我们如何修改端口号一些配置可以去config文件的index.js文件里修改，至于一些其他配置，这里不做过多解释，可以自己去文件中查找。**



# webpack学习与使用

在vue程序中有一个文件特别的重要就是在`build`文件下的`webpack.js`文件，那什么是webpack？

本质上，webpack是一个javaScript应用程序的静态模块打包器，当webpack处理应用程序时，他会递归的构建一个依赖关系图，其中包含应用程序需要的每个模块，然后将所有的这些模块打包成一个或多个bundle。

> 安装   ，-v检查

~~~shell
npm install webpack -g
npm install webpack-cli -g
~~~

![image-20200612121555630](image-20200612121555630.png)

> 配置

创建`webpack.config.js`配置文件

- entry：入口文件，指定WebPack用哪个文件作为项目的入口
- output：输出，指定WebPack把处理完成的文件放置到指定路径
- module：模块，用于处理各种类型的文件
- plugins：插件，如热更新、代码重用
- resolve：设置路径指向
- watch：监听，用于设置文件改动后直接打包

> 使用

1. 创建一个空文件夹 `webpack-study`作为项目文件

2. 用idea打开并创建一个modules的文件目录，用于存放js模块等资源文件

3. 在modules下创建模块文件，如hello.js，用于编写JS模块的相关代码

   ![image-20200612133837441](image-20200612133837441.png)

   main.js为主入口，引用了hello.js的一个个方法，这就是前端的模块化开发，如果是以前只能使用`<script src=""></script>`

4. 在根目录下编写打包文件：webpack.config.js

   ~~~js
   "use strict"
   module.exports={
       entry: './modules/main.js',   //程序入口
       output: {
           filename: "./js/bundle.js" //打包输出地址
       }
   }
   ~~~

5. 在Terminal控制台输入`webpack`进行打包，他会自己去寻找webpack.config.js文件按要求打包

   ![image-20200612134907785](image-20200612134907785.png)

6. 打包过后就会多一个dist文件，里面就是打包的内容。

   ![image-20200612144750512](image-20200612144750512.png)

7. 打包成功编写index.html去使用

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
   </head>
   <body>
   <script src="./dist/js/bundle.js"></script>
   </body>
   </html>
   ```

   在界面中我们只引入了`<script>`，其他什么都没写，运行查看结果

   ![image-20200612145455408](image-20200612145455408.png)

   发现浏览器可以直接输出这两行字 了，这就是前端的模块化开发，html界面已经不需要写界面了，只需要导入一个一个组件即可，这也是vue通过webpack帮我们干的事。

   # vue路由 vue-router

   > 安装

   我们去第一个Vue-cli项目myvue修改我们的项目，首先需要在项目的控制台中安装，或者在终端跳到当前项目目录进行安装

   ~~~shell
   npm install vue-router --save-dev
   ~~~

   安装完毕后，我们就可以在`node_modules`文件中找到vue-router。

   > 使用前的准备

   我们清理一下项目，把HelloWord.vue和那张在assets文件下的图片删除，并删除APP.vue中一些不必要的代码，结果如下图

   ![image-20200612151957881](image-20200612151957881.png)

   就留了main.js入口和App.vue。

   > 使用 实现：首页与内容页之间的跳转

   1. 在components文件下新建一个组件Content.vue当做内容页

      ~~~vue
      <template>
          <h2>内容页</h2>
      </template>
      
      <script>
          export default {
              name: "Content"
          }
      </script>
      
      <style scoped>
      
      </style>
      
      ~~~

   2. components文件下新建一个组件Main.vue当做首页

      ~~~vue
      <template>
        <h1>首页</h1>
      </template>
      
      <script>
          export default {
              name: "Main"
          }
      </script>
      
      <style scoped>
      
      </style>
      
      ~~~

   3. 在src下新建包router，并新建index.js文件，用来配置路由

      ~~~js
      import Vue from 'vue'
      import VueRouter from 'vue-router'
      import Content from '../components/Content'
      import Main from '../components/Main'
      
      //安装路由
      Vue.use(VueRouter);
      
      //配置导出路由
      export default new VueRouter({
        routes: [
          {
            //路由路径
            name: 'content',
            path: '/content',
            //跳转的组件
            component: Content
          },
          {
            //路由路径
            name: 'main',
            path: '/main',
            //跳转的组件
            component: Main
          }
        ]
      });
      
      ~~~

      **在这有个注意点：routes内的是component，而不是components**

      **component：一般用来全局注册组件，用 `Vue.component{}` 来创建组件，上述代码中是在单个路由中注册组件；**

      **components：一般都是用来引入或导出，会用在新创建的 Vue 根实例 (`new Vue`) 的模板中，因为可能导入多个组件，所以带s，或者用在export default中向外暴露多个组件**

      详细的关于组件的可以参考文档：https://cn.vuejs.org/v2/guide/components-registration.html

   4. 编写App.vue

      ~~~vue
      <template>
        <div id="app">
          <h1>vue-router</h1>
          <router-link to="/main">首页</router-link>
          <router-link to="/content">内容页</router-link>
          <router-view></router-view>
      
        </div>
      </template>
      
      <script>
      import Content from './components/Content'
      export default {
        name: 'App',
        components: {
          Content
        }
      }
      </script>
      
      
      <style>
      #app {
        font-family: 'Avenir', Helvetica, Arial, sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        text-align: center;
        color: #2c3e50;
        margin-top: 60px;
      }
      </style>
      
      ~~~

      <router-view></router-view>：是一个渲染的标签，如果没有这个标签不能把组件内容渲染到当前界面。

   5. 在main.js中我们还需要引入自定义的router

      ~~~js
      'use strict'
      // The Vue build version to load with the `import` command
      // (runtime-only or standalone) has been set in webpack.base.conf with an alias.
      import Vue from 'vue'
      import App from './App'
      import router from './router'  //自动扫描router目录下的路由配置
      
      Vue.config.productionTip = false;
      
      
      /* eslint-disable no-new */
      new Vue({
        el: '#app',
        router: router,
        components: { App },
        template: '<App/>'
      });
      
      ~~~

      

   6. 运行结果

      ![image-20200612174724204](image-20200612174724204.png)

      当我们把鼠标放在链接上，会出现跳转的链接，点击后，就可以把该组件的内容渲染到当前界面，比如我们点击首页

      ![image-20200612174836528](image-20200612174836528.png)

      这个渲染就是通过`<router-view>`来实现的。

# Vue+elementUI

## 创建项目

~~~shell
#初始化项目
vue init webpack hello-vue
#进入项目
cd hello-vue
#安装路由
npm install vue-router --save-dev
#安装element-ui
npm i element-ui -S
#安装依赖
npm install
#安装SASS加载器
cnpm install sass-loader node-sass --save-dev
#用idea打开后启动测试
npm run dev
~~~

修改项目目录结构，如图修改创建文件，一共需要5个文件即可，把原先的helloword都删除

<img src="image-20200613135543468.png" alt="image-20200613135543468" style="zoom:50%;" />



1. 先编写Main.vue和Login.vue，Main.vue就一个<h1>首页</h1>>标题，作为跳转页面，Login.vue去element网站中随便选取一个表单代码作为测试

   ~~~vue
   <template>
     <div>
   
       <el-form :model="ruleForm" status-icon :rules="rules" ref="ruleForm" label-width="100px" class="demo-ruleForm">
         <el-form-item label="密码" prop="pass">
           <el-input type="password" v-model="ruleForm.pass" autocomplete="off"></el-input>
         </el-form-item>
         <el-form-item label="确认密码" prop="checkPass">
           <el-input type="password" v-model="ruleForm.checkPass" autocomplete="off"></el-input>
         </el-form-item>
         <el-form-item label="年龄" prop="age">
           <el-input v-model.number="ruleForm.age"></el-input>
         </el-form-item>
         <el-form-item>
           <el-button type="primary" @click="submitForm('ruleForm')">提交</el-button>
           <el-button @click="resetForm('ruleForm')">重置</el-button>
         </el-form-item>
       </el-form>
     </div>
   </template>
   
   <script>
       export default {
           name: "Login",
         data() {
           var checkAge = (rule, value, callback) => {
             if (!value) {
               return callback(new Error('年龄不能为空'));
             }
             setTimeout(() => {
               if (!Number.isInteger(value)) {
                 callback(new Error('请输入数字值'));
               } else {
                 if (value < 18) {
                   callback(new Error('必须年满18岁'));
                 } else {
                   callback();
                 }
               }
             }, 1000);
           };
           var validatePass = (rule, value, callback) => {
             if (value === '') {
               callback(new Error('请输入密码'));
             } else {
               if (this.ruleForm.checkPass !== '') {
                 this.$refs.ruleForm.validateField('checkPass');
               }
               callback();
             }
           };
           var validatePass2 = (rule, value, callback) => {
             if (value === '') {
               callback(new Error('请再次输入密码'));
             } else if (value !== this.ruleForm.pass) {
               callback(new Error('两次输入密码不一致!'));
             } else {
               callback();
             }
           };
           return {
             ruleForm: {
               pass: '',
               checkPass: '',
               age: ''
             },
             rules: {
               pass: [
                 { validator: validatePass, trigger: 'blur' }
               ],
               checkPass: [
                 { validator: validatePass2, trigger: 'blur' }
               ],
               age: [
                 { validator: checkAge, trigger: 'blur' }
               ]
             }
           };
         },
         methods: {
           submitForm(formName) {
             this.$refs[formName].validate((valid) => {
               if (valid) {
                 this.$router.push("/main")
               } else {
                 console.log('error submit!!');
                 return false;
               }
             });
           },
           resetForm(formName) {
             this.$refs[formName].resetFields();
           }
         }
       }
   </script>
   <style scoped>
   </style>
   
   ~~~

   通过 `this.$router.push("/main")`进行验证正确后的跳转

2. 编写路由index.js

   ~~~js
   import Vue from 'vue'
   import VueRouter from 'vue-router'
   import Login from '../views/Login'
   import Main from '../views/Main'
   
   
   Vue.use(VueRouter);
   
   export default new VueRouter({
     routes: [
       {
         name: 'login',
         path: '/',
         component: Login
       },
       {
         name: 'main',
         path: '/main',
         component: Main
       }
     ]
   })
   
   ~~~

3. 编写main.js

   ~~~js
   import Vue from 'vue'
   import App from './App'
   import router from './router'
   import ElementUI from 'element-ui';
   import 'element-ui/lib/theme-chalk/index.css'
   
   Vue.config.productionTip = false
   
   Vue.use(ElementUI);
   
   new Vue({
     el: '#app',
     router: router,
     render: h => h(App)
   })
   
   ~~~

   这里重要的一点是引入`import ElementUI from 'element-ui';`和`import 'element-ui/lib/theme-chalk/index.css';`，并加上`Vue.use(ElementUI);`，最后不需要写components和template，直接换成`render: h => h(App)`

4. App.vue最为简单，就一个渲染标签即可

   ~~~vue
   <template>
     <div id="app">
       <router-view></router-view>
     </div>
   </template>
   <script>
   export default {
     name: 'App'
   }
   </script>
   <style>
   </style>
   ~~~

## 传递参数 三步骤

1. 在我们前端当然不可能只有界面的跳转，这时我们就需要传递参数，在之前我们跳转时使用`<router-link to='/login'>XXX</router-link>`，这样实现单纯的跳转，而想要传递参数，就需要加额外的参数：

~~~vue
<!--前端传-->
<router-link v-bind:to="{name: 'UserProfile',params: {id: 1}}"></router-link>
~~~

2. 这是链接传递参数的方法，那怎么接收呢？

~~~js
//路由接收，传给另一个界面
export default new VueRouter({
  routes: [
    {
      name: 'user',
      path: '/user',
      component: User,
      children: [
        {path: '/user/:id',name:'UserProfile',component: UserProfile} //这里的name要和前端v-bind绑定的name一致
      ]
    }
  ]
})
~~~

3. 在组件取出数据展示：

~~~vue
<!--前端接收-->
<template>
<div>
  <h1>个人信息</h1>
	{{$route.params.id}}
</div>
</template>
~~~

**注意：在template模板中，所有东西都必须在一个标签里，不能直接写在template中，所以我们包裹了一个大的div标签**

## props方法传递参数

1. 在路由添加一个参数

   ~~~js
   {path: '/user/:id',name:'UserProfile',component: UserProfile,props: true}
   ~~~

   

2. 模板接收

   ```vue
   <template>
   <div>
     <h1>个人信息</h1>
   	{{id}}
   </div>
   </template>
   
   <script>
       export default {
         props: ['id'],
           name: "UserProfile"
       }
   </script>
   
   <style scoped>
   
   </style>
   ```

## 重定向

```js
export default new VueRouter({
  routes: [
    {
      name: 'login',
      path: '/',
      component: Login
    },
    {
      path: 'goHome',
      redirect: '/main'
    }
  ]
})
```

# 跑别人的Vue项目

在工作中，我们常要接手别人的vue项目，那我们从svn、git上拿到别人的vue项目后，要如何跑起来呢？

1. 将项目里的`node_modules`文件夹删除，这是vue项目的依赖包。不过由于“node_modules”文件夹太大，一般不会打包上传到svn、git上的，所以没有这个文件夹就不用删。

2. 删除`package-lock.json`。package-lock.json记录了整个node_moudles文件夹的树状结构，还记录了模块的下载地址，但是它是基于项目作者的npm版本库生成的，若不删掉这个依赖文件，容易出现npm版本差异导致的报错。

3. 然后打开cmd，cd到项目目录，具体操作：先输入该盘符，然后再用cd命令切换目录，不懂的自行度娘。

4. 运行`npm install`，重新安装依赖。

5. 运行`npm run build `打包。

6. 最后运行`npm run dev`后项目成功运行。

注：建议不要用 cnpm 安装 会有各种诡异的bug 可以通过如下操作解决 npm 下载速度慢的问题
`npm install --registry=https://registry.npm.taobao.org`

# 总结

至此Vue的常用知识都学习了一遍，想要更加熟练使用的建议根据官方文档从头学一遍，学了上面这些知识，官方文档也基本都看的懂了。

