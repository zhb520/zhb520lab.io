---
categories: SpringCloud
tags: 微服务
date: 2020-03-12 20:17:20 
---



# SpringCloud

微服务架构的4个核心问题：

1. 服务很多，客户端怎么去访问？
2. 这么多服务，服务之间如何通信？
3. 这么多服务，如何治理？
4. 服务挂了怎么办

<!--more-->

解决方案：

1. Spring Cloud  NetFlix  一站式解决

   api网关：zuul组件

   Feign ---HttpClinet ---Http通信方式，同步，阻塞

   服务注册与发现：Eureka

   熔断机制：Hystrix

2. Apache Dubbo Zookeeper  半自动，需要整合别人

   api网关：没有，找第三方组件，或自己实现

   Dubbo：RPC通信框架

   服务注册与发现：Zookeeper

   熔断机制：没有，借助Hystrix

   

   Dubbo这个方案并不完善~

   

3. Spring Cloud Alibaba  一站式解决！更简单

4. 目前又提出一种方案：

   服务网格：下一代微服务标准，Server Mesh

   代表解决方案：istio



万变不离其宗

1. API：解决服务路由问题
2. HTTP，RPC：通信
3. 注册和发现
4. 熔断机制

​			

# 常见的面试题

1. 什么是微服务？

   **微服务**强调的是一种服务，服务的大小，是具体解决某一个问题的服务，**微服务架构**是一种架构思想，或者说是一个架构风格，**提倡将单一的应用程序划分成一组小的服务**，模块化，服务之间采用轻量级通信机制进行沟通（HTTP），每一个服务都相当于一个进程，而不是线程，可以用任何语言来编写服务，也可以使用不同的数据存储。

   总结起来就是：微服务的核心就是讲传统的一站式服务拆分成一个一个服务，彻底去耦合，每个服务提供单个业务功能，能够自行单独启动或销毁，拥有独立的数据库。

   

2. 微服务之间是如何建立通讯的？

3. SpringCloud和Dubbo有哪些区别？

4. SpringBoot和SpringCloud，请你谈谈对他们的理解

   - SpringBoot专注于快速方便的开发单个个体微服务。

   - SpringCloud是关注全局的微服务协调整理治理框架，他将SpringBoot开发的一个个单体微服务整合并管理起来，为各个微服务之间提供：配置管理，服务发现，断路由，路由，微代理，事件总线，全局锁，决策竞选，分布式会话等等集成服务。
   - SpringBoot可以离开SpringCloud独立使用，开发项目，但是SpringCloud离不开SpringBoot，属于依赖关系

5. 什么是服务熔断？什么是服务降级

6. 微服务优缺点分别是什么

   优点：

   - 单一职责原则

   - 每个服务足够内聚，足够小，代码容易理解，这样能聚焦一个指定的业务功能或业务需求

   - 开发简单，开发效率提高，一个服务可能就是专一的只干一件事

   - 微服务能够被小团队开发，这个小团队是2-5个人的开发人员组成

   - 微服务是松耦合的，具有功能意义的服务，无论是在开发阶段或部署阶段都是独立的

   - 微服务能使用不同语言开发

   - 易于和第三方集成，微服务允许容易且灵活的方式集成自动部署，通过持续集成工具，如jenkins，Hudson，bamboo

   - 微服务易于被一个开发人员理解，修改和维护，这样小团队能更好的关注自己的工作成果，无需通过合作才能体现价值。

   - 微服务允许你利用最新的技术

   - **微服务只是业务逻辑代码，不会和HTML,CSS或其他界面混合**

   - **每个微服务都有自己的存储能力，可以有自己的数据库，也可以由统一的数据库**

     

     

   缺点：

   - 开发人员要处理分布式系统的复杂性
   - 多服务运维难度，随着服务的增加，运维的压力也在增大
   - 系统部署依赖
   - 服务之间通信成本
   - 数据的一致性
   - 系统集成测试
   - 性能监控....

7. 你知道微服务的技术栈有哪些？请列举一、二

   

   | 微服务条目                             | 落地技术                                                     |
   | -------------------------------------- | ------------------------------------------------------------ |
   | 服务开发                               | SpringBoot，Spring，SpringMVC                                |
   | 服务配置与管理                         | Netflix公司的Archaius、阿里的Diamond等                       |
   | 服务注册与发现                         | Eureka，Consul，Zookeeper                                    |
   | 服务调用                               | Rest，RPC，gRPC                                              |
   | 服务熔断器                             | Hystrix，Envoy等                                             |
   | 负载均衡                               | Ribbon，Nginx等                                              |
   | 服务接口调用(客户端调用服务的简化工具) | Feign等                                                      |
   | 消息队列                               | Kafka，RabbitMQ，ActiveMQ等                                  |
   | 服务中心配置管理                       | SpringCloudConfig，Chef等                                    |
   | 服务路由（API网关）                    | Zuul等                                                       |
   | 服务监控                               | Zabbix，Nagios，Metrics，Specatator等                        |
   | 全链路追踪                             | Zipkin，Brave，Dapper等                                      |
   | 服务部署                               | Docker，OpenStack，Kubernetes等                              |
   | 数据流开发包                           | SpringCloud Stream（封装与Redis，Rabbit，Kafak等发送接收消息） |
   | 事件消息总线                           | SpringCloud Bus                                              |

   

8. eureka和Zookeeper都可以提供服务注册与发现功能，说说两个的区别？

   # Eyreka对比zookeeper
   
   **回顾CAP原则**
   
   RDBMS（Mysql，Oracle，sqlServer）===>ACID
   
   NoSQL（redis，mongdb）===>CAP
   
   **ACID是什么**
   
   - A（Atomicity）原子性
   - C（Consistency）一致性
   - I（Isolation）隔离性
   - D（Durability）持久性
   
   **CAP是什么**
   
   - C（Consistency）强一致性：保持多台服务器数据保持一致
   - A（Availability）可用性：当用户向其中一台发送请求，必须给予回应
   - P（Partition tolerance）分区容错性：当其中一台服务出现问题，不能使整个系统崩盘
   
   [CAP详细解释博客](http://www.ruanyifeng.com/blog/2018/07/cap.html)
   
   CAP总结：因为现在大型网络几乎都是分布式的所以p是肯定要保证的，那门只能在C或A取舍，因为当保持C时，就是向其中一台服务器进行写时，其他服务器必须锁定等到数据更新后才能通信，锁定时，用户就不能从该服务区获取数据，那么可用性就不成立。当保持可用性时，当其中一台数据修改，而用户访问另一台服务器时为了必须使服务器获得反应，所获取的值还是旧的值，那么一致性就不成立。
   
   
   
   - zookeeper保持的是CP；
   - Eureka保持的是AP；
   
   **zookeeper保证的是CP**
   
   ​	当向注册中心注册查询服务时，我们能容忍返回的信息是几分钟以前的，但不能容忍服务器直接down掉，也就是说可用性是高于一致性的。zk会出现这么一种情况，当master节点因为故障而与其他节点失去联系，剩余节点就会重新选取leader，问题在于选取leader就会花费很多的时间，30~120s，甚至更长，这期间整个zk集群处于不能使用状态，在云部署情况下，因为网络问题导致zk失去master节点的大概率会发生，虽然服务器最终都会恢复，但是这个漫长的等待是用户不能容忍的
   
   **Eureka保证的AP**
   
   ​	Eureka就是看明白了这一点，设计的就是保持可用性，各个节点平等，几个节点挂掉不会影响正常节点工作，剩余节点依然可以进行注册和服务。在进行注册时，发现连接失败，就会切换至其他节点，只要有一台Eureka还在，就能保证注册成功。Eureka还有一种自我保护机制，在15分钟内超过85%的节点没有正常心跳，那么Eureka就认为客户端与注册中心出现了网络故障，就会出现以下几种情况：
   
   1. Eureka不在从注册列表中移除因为长时间没有收到心跳而应该过期的服务
   2. Eureka仍然能够接受新服务的注册与查询请求，但不会同步到其他节点（保证当前节点的依然可用）
   3. 当网络稳定后，当前实例新的注册信息会被同步到其他节点上
   
   
   
   **因此，Eureka可以很好的应对网络故障而导致部分节点失去联系的情况，而不会像zookeepre一样使整个服务瘫痪**
   
   
   
   
   
   





参考书：

- Netflix:https://www.springcloud.cc/spring-cloud-netflix.html
- SpringCloud中文文档：https://www.springcloud.cc/spring-cloud-dalston.html
- SpringCloud中文网：https://www.springcloud.cc/
- SpringCloud中国社区：http://www.springcloud.cn/

