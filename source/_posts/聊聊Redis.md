---
title: 聊聊Redis
date: 2021-04-02 18:28:05
tags: [Linux,redis]
categories: Redis
---



# 什么是Redis

Redis（**Re**mote **Di**ctionary **S**erver）即远程字典服务，是以c语言编写的，开源的高性能非关系型键值对数据库，他读的速度达到110000次/s，写的速度达到81000次/秒。

Redis会周期性的吧更新的数据写入磁盘或者把修改的操作写入追加文件，并且在此基础上实现master-slave（主从）同步。是当下最热门的NoSQL技术之一，也被人们称为结构化数据。

<!--more-->

# Linux(Cent OS)下Redis安装与启动

1. 下载任意版本redis，这里我们下载了 `redis-5.0.8.tar.gz`,上传到远程服务器或虚拟机里

2. 解压缩再移动到`/opt/`目录下，再进入该目录查看文件

   ![image-20200403123429829](1.png)

   这里有一个最重要的文件**redis.conf**，这是redis的配置文件，后面都会用到。

3. 进入该目录执行 `make`命令进行编译，

   【坑】如果报错说没有c语言的支持，那么就要安装gcc和c++，可以先通过`gcc -v`查看有无环境，如果没有执行以下命令安装环境

   ~~~shell
   yum install gcc
   yum install gcc-c++
   ~~~

   再进入安装的redis-5.0.8文件下

   **如果在没有c语言支持时执行make报错了， 那么在安装c++后需要执行make distclean清除之前make后留下的缓存文件**

   4. 执行make编译结束后再执行`make install`安装即可 

      ![image-20200403124802923](2.png)

   5. 默认安装路径 usr/local/bin，我们进入该目录查看：

      ![image-20200403125040165](3.png)

      看到以上几个文件：

      redis-bebchmark：性能测试工具，看看自己本子的性能如何（服务器启动后运行）

      redis-check-aof：修复有问题的AOF文件

      redis-check-rdb：修复有问题的RDB文件

      redis-cli：客户端启动入口

      redis-sentinel：rendis集群使用,也就是哨兵

      redis-sercer：启动redis服务

   6. 接下来就是配置了

      首先复制一份配置文件redis.conf到别的目录下（复制到/opt/myRedis/目录下），原先的配置文件不能动当做备份

      进入配置文件的第一件事就是把redis修改为后台启动

      通过查找命令“/daemonize” 把改参数的no改为yes

      

      启动命令：

      先启动服务器：redis-server /opt/myRedis/redis.conf

      在启动客户端 redis-cli -h 127.0.0.1 -p 6379

   7. 如何测试启动成功

      在redis-cli客户端里面输入ping，输出pong即说明成功

      或者通过`ps -ef |grep redis`查看**redis-server**和**redis-cli**有没有启动成功

   8. 退出命令

      ~~~shell
      #在redis客户端里面，使redis服务和客户端一起关闭
      shutdown
      #只退出客户端三种方法都可以
      exit
      quit
      ctrl(control)+c
      #若没客户端只有服务端,关闭服务端
      redis-cli shutdown
      #或者通过杀进程强行退出
      ~~~

   # 性能测试

   `redis-bebchmark`后面可以跟的性能测试参数

   | 序号 | 选项      | 描述                                       | 默认值    |
   | ---- | --------- | ------------------------------------------ | --------- |
   | 1    | **-h**    | 指定服务器主机名                           | 127.0.0.1 |
   | 2    | **-p**    | 指定服务器端口                             | 6379      |
   | 3    | **-s**    | 指定服务器 socket                          |           |
   | 4    | **-c**    | 指定并发连接数                             | 50        |
   | 5    | **-n**    | 指定请求数                                 | 10000     |
   | 6    | **-d**    | 以字节的形式指定 SET/GET 值的数据大小      | 2         |
   | 7    | **-k**    | 1=keep alive 0=reconnect                   | 1         |
   | 8    | **-r**    | SET/GET/INCR 使用随机 key, SADD 使用随机值 |           |
   | 9    | **-P**    | 通过管道传输 <numreq> 请求                 | 1         |
   | 10   | **-q**    | 强制退出 redis。仅显示 query/sec 值        |           |
   | 11   | **--csv** | 以 CSV 格式输出                            |           |
   | 12   | **-l**    | 生成循环，永久执行测试                     |           |
   | 13   | **-t**    | 仅运行以逗号分隔的测试命令列表。           |           |
   | 14   | **-I**    | Idle 模式。仅打开 N 个 idle 连接并等待。   |           |

   ~~~shell
   #测试:100个并发，10w个请求,使用这个命令要保证redis服务器启动着
   redis-benchmark -c 100 -n 100000
   ~~~

   测试结果一部分截图：

   ![image-20200403133527221](4.png)

   # Redis配置

   daemonize no：前台启动，需改为yes后台启动

   bind 127.0.0.1：ip地址，其他主机想要连接需要把这句话注释掉
   
   protected-mode yes：保护模式开启，把主机地址注释掉后如果这个开启，Redis依旧只允许本机访问，ip还是127.0.0.1，所以要改为no
   
   pidfile /var/run/redis.pid：配置pid文件路径，这个就为默认路径，pid就是进程id，ppid是他父进程的进程id
   
   dbfilename dump.rdb：rdb文件名
   
   dir ./：rdb文件存放目录，默认当前目录，即在哪启动redis文件就会在哪生成，所以建议修改
   
   appendonly no：默认关闭AOF持久化，开启改为yes即可文件保存路径与rdb一致
   
   appendfilename “appendonly.aof” ：aof默认名称
   
   
   
   # 基础知识
   
   ## 库的基本操作
   
   ~~~shell
   #跳到下标为index的库 一共16个库0~15
   select <index> 
   #查询当前所有的键
   key *  
   #判断某个键是否存在
   exists<key> 
   #查看键的类型
   type<key> 
   #删除某个键
   del<key> 
    #设置某个键的过期时间，单位为秒
   expire <key> <seconds>
   #查看还有多久时间过期 -1永不过期 -2已过期
   ttl <key>  
   #查看当前数据库的key的数量
   dbsize  
   #清空当前库
   Flushdb 
   #通杀全部库
   Flushall  
   ~~~
   

  

## Redis为什么单线程还这么快？

1. 误区1：高性能服务器一定是多线程？

2. 误区2：多线程一定比单线程效率高？

   首先要对速度的比较是cpu>内存>硬盘有所了解

   核心：Redis采用的是单线程多路IO复用，redis的所有数据都放到内存中的，所以说单线程效率就是最高的，多线程（CPU上下文切换，是耗时间的操作），对于内存来说，如果没有上下文效率就是最高的，多次读写都是在一个cpu下。



## 什么是多路IO复用呢？

​	我们得先了解**传输协议和Socket**：

​	网络由下往上分为 物理层 、数据链路层 、 网络层 、 传输层 、 会话层 、 表现层 和 应用层。

​	IP协议对应于网络层，TCP协议对应于传输层，而HTTP协议对应于应用层。

​	TCP/IP协议是传输层协议，主要解决数据如何在网络中传输，而HTTP协议是应用层协议，主要解决如何包装数据。

​	而什么是Socket呢，实际上socket是对TCP/IP协议的封装，它的出现只是使得程序员更方便地使用TCP/IP协议栈而已。socket本身并不是协议，它是应用层与TCP/IP协议族通信的中间软件抽象层，是一组调用接口（TCP/IP网络的API函数），**说的明白点就是程序员做网络开发用来调用TCP/IP协议的接口**，我们进行IO请求时，Socket默认创建的是阻塞的。

​	接着我们要理解**下面四种模式**，同步和异步是针对客户端，而阻塞非阻塞是针对服务端

1. **同步阻塞模型（阻塞IO）**：当用户线程向内核发送IO请求时，只有等到数据返回才能取回数据并返回继续执行，这个期间会一直在内核中等待不会做任何事。

2. **同步非阻塞模型（非阻塞IO）**：当用户线程向内核发送IO请求时，非阻塞IO要求Socket被设置为NONBLOCK，这时请求送到后会立马返回，但这个时候并没有得到任何数据，然后用户线程就会进入线程轮询，不断发起IO请求，直到拿到数据才继续执行。

3. **IO多路复用（异步阻塞IO）**：IO多路复用模型是建立在内核提供的多路分离函数select基础之上的，使用select函数可以避免同步非阻塞IO模型中轮询等待的问题，因为select是阻塞的。用户首先将需要进行IO操作的socket添加到select中，然后阻塞等待select系统调用返回。当数据到达时，socket被激活，select函数返回。用户线程正式发起read请求，读取数据并继续执行。

   从流程上来看，使用select函数进行IO请求和同步阻塞模型没有太大的区别，甚至还多了添加监视socket，以及调用select函数的额外操作，效率更差。但是，使用select以后最大的优势是用户可以在一个线程内同时处理多个socket的IO请求。用户可以注册多个socket，然后不断地调用select读取被激活的socket，即可达到在同一个线程内同时处理多个IO请求的目的，也就解决来了高并发的问题。而在同步阻塞模型中，必须通过多线程的方式才能达到这个目的。

   然而，使用select函数的优点并不仅限于此。虽然上述方式允许单线程内处理多个IO请求，但是每个IO请求的过程还是阻塞的（在select函数上阻塞），平均时间甚至比同步阻塞IO模型还要长。如果用户线程只注册自己感兴趣的socket或者IO请求，然后去做自己的事情，等到数据到来时再进行处理，也就是类似于异步，则可以提高CPU的利用率。

   **IO多路复用模型使用了Reactor设计模式（反应器设计模式）实现了这一机制**

   用户线程注册事件处理器之后可以继续执行做其他的工作（异步），而Reactor线程负责调用内核的select函数检查socket状态，这里的阻塞IO指的是select，而不是socket，Socket也会被设置为NONBLOCK,而轮询的操作交给Reactor来做。

   具体过程如下图

   ![image-20200403161251544](5.png)

   

4. **异步非阻塞模型**：使用了Proactor设计模式（前摄器设计模式），“真正”的异步IO需要操作系统更强的支持。在IO多路复用模型中，事件循环将文件句柄的状态事件通知给用户线程，由用户线程自行读取数据、处理数据。而在异步IO模型中，当用户线程收到通知时，数据已经被内核读取完毕，并放在了用户线程指定的缓冲区内，内核在IO完成后通知用户线程直接使用即可。如果是Reactor模式，那么，反应器会通知我们 “可以读取数据了”，然后调用回调函数，利用recv函数从Socket读取数据，是个仿异步。而如果是Proactor模式，那么会先调用WSARecv函数注册读事件，反应器会通知我们 “数据已经读取了”，回调函数触发时，数据已经被接收到事先提供的缓冲区中，整个IO过程是由操作系统完成的，而不需要我们自己调用recv函数来读取数据，直接在事先提供的缓冲区取数据就可以了。



# 五大数据类型

 Redis是内存中的数据节后存储系统，它可用作**数据库**、**缓存**和**消息中间件MQ**，支持多种数据结构，如字符串（String）、哈希（hashes）、列表（lists）、集合（sets）、有序集合（sorted sets）与范围查询，bitmaps，hyperloglogs和地理空间（geospatial）索引半径查询。Redis具有内置的复制、Lua脚本、LRU逐出、事务和不同级别的磁盘持久性（RDB和AOF），并通过哨兵（Redis Sentinel）和带有Redis集群的自动分区提供高可用性。

## String

~~~shell
#基本命令
#查看所有key
keys *
#添加与查找
set <key><value>
get <key>
#删除
del <key>
#判断值是否存在，存在返回1，否则为0
exists <key>
#添加同时设置过期时间单位为秒
setex <key> <过期时间> <value>  
#查看当前key的剩余过期时间过期为-2，永久为-1 ，有过期时间返回剩余时间
ttl <key>
#往key中的值进行追加，不会覆盖原先的value
append <key> <value>  
#查询值的长度
strlen <key> 
#只有在键不存在的时候才会生效，进行添加
setnx <key> <value>  
#将key对应的value自动增加1/减1，所以value必须为数值
incr<key>   
decr<key> 
#为增加和减少设置步长
incrby/decrby <key> <步长> 
#添加设置多个值
mset <key1> <value1> <key2> <value2>…
#获取多个值
mget <key1> <key2> <key3>…
#添加多个，键不能已存在,添加多个成功返回1，如果有一个失败就返回0，全部添加失败
msetnx <key1> <value1> <key2> <value2>
#对这个键所对应的值进行截取，起始位置和结束位置都包括
getrange <key> <起始位置> <结束位置>
#从起始位置（包含起始位置）覆盖重写value
setrange <key> <起始位置> <value>
#为该键值以新换旧，返回旧值
getset <key><value>

~~~

注：对于ttl命令其实还有一个特殊的返回值：0

   **那什么时候会返回0呢？**

   如果一个redis作为slave，且将配置slave-read-only设置为off，并写入了一个带有TTL的key时，当key过期后，该key是不会被Redis删除的，且TTL在过期后永远为0，并且该key依然存在，使用setnx会失败返回0

   ## List

   list是一个双端列表，你可以往左边或右边进行添加或删除

~~~shell
#往左边/右边插入一个值或多个值
lpush/rpush  <key> <value1><value2><value3>…
#从左边/右边吐出一个值并返回，值在键在，值光键亡，返回nil
lpop/rpop <key>
#从key1的右边弹出一个值加到key2的左边
rpoplpush <key1> <key2>
#按照下标索引从左到右查询，从头到尾查询就是0到-1
lrange <key> <start> <stop>
#安装索引查询(从左到右)
lindex <key> <index>
#获得列表长度
llen<key>
#往固定下标某个值进行更新覆盖，若下标不存在则报错
lest <key> <index> <value>
#往某个键中的某个值前面或后面添加新的value,返回添加后，列表值的个数
linsert <key> after/befer <value> <newvalue>
#从左边开始删除n个某个值，若n为负数，则从右往左删除几个，n为0时代表删除所有
lrem <key> <n> <value>
#截取指定下标的内容，其余删除
ltrim <key> <start> <stop>
#小技巧：删除list，值光键亡 start(数值)>stop，且都为正数
ltrim key 1 0 
   
~~~

   

   ## Set

   ~~~shell
#将一个或多个值添加到set集合里面，它的底层是value为null的hash表，把值存在key中，所以无序不重复
sadd <key> <value1><value2><value3>..
#查看该集合所有的值
smembers <key> 
#判断该集合是否含有value，有则返回1，无则返回0
sismember <key> <value>
#返回该集合的元素个数
scard <key> 
#删除集合中的某个元素
srem <key> <value1> <value2>..
#随机弹出一个元素，会在该 集合中删除
spop <key>
#从集合中随机选出n个元素返回，但这些元素不会从集合中删除
srandmeber <key> <n> 
#特有方法
#移动成员将key1中的某个元素移到key2
smove <key1><key2><value>
#返回两个集合的交集
sinter <key1> <key2>
#返回两个集合的并集
sunion <key1> <key2>
#返回两个集合的差集 key1的集合减去key1与key2的交集（返回key1中key2没有的值）
sdiff <key1> <key2>
   ~~~

   

   ## Hash

   hash最为特殊，有两个key，一个是redis的key，一个是hash的key，一般hash的key用field表示，对于field的命名一般见名识意，用冒号当做下划线分离单词，他的命令是h开头，与string类型的命令类似

   例如user:1001:uid，user:1001:username，user:1001:password 表示1001用户的ID，用户名，密码

   ~~~shell
#给key集合中的field键赋值value
hset <key> <field> <value>
#只有field不存在时，才能给key集合中创建field键并赋值value
hsetnx <key> <field> <value>
#查询key集合
hget <key> <field>
#插入多个值
hmset <key> <field1> <value1> <field2> <value2>…
#查看哈希表key中，给定域field是否存在
hexists key <field>
#列出该key中所有的field
hkeys <key>
#列出hash集合中所有的值
hvals<key>
#获取该键中所有的hash键值对,键一行，值一行，一行一行返回所有键值对
hgetall <key>
#为所对应的值加相应的数值，做加法运算，increment若为负数，就变成了减法,不存在hdecrby
hincrby <key> <field> <increment>
   ~~~

   

   ## Zset

   不重复有序set集合，通过设置score来进行排序

   ~~~shell
#将一个或多个元素绑定一个分数进行添加,分数从小到大排序
zadd <key> <score1> <value1> <score2> <value2>…
#相同元素，不同分数，会将该元素的分数替换掉，不同元素，相同分数，会将
#根据下标查询元素，最后输入withscores，可以让分数和值一起返回到结果集，先返回值再返回分数
zrange <key> <start> <stop> [withscores]
#根据下标改为从大到小排序
zrevrange <key> <start> <stop> [withscores]
#通过分数查找分数之间的所有元素，包括min和max,如果想要开区间可以写成“(1 (2” ，这个表示（1，2）正负无穷表示 +inf -inf
zrangebyscore <key> <min> <max> [withscores]
#根据分数从高到低，先写max参数再写min
zrevrangebyscore <key> <min> <max> [withscores]
#为一个元素的分数进行加减
zincrby <key> <increment> <value>
#删除元素
zrem <key> <value>
#统计元素个数
zcard <key>
#统计分数区间的元素
zcount<key> <min> <max>
#返回该值在集合中的排名，从0开始（即下标）
zrank <key> <value>
   ~~~

   

   # 三种特殊数据类型

   ## geospatial-存储经纬度

   一共6个命令

   ~~~shell
#添加指定地理位置
geoadd <key> <经度> <纬度> <member>
#查找指定地点的经度纬度
geopos <key> <member>
#返回两个定位之间的直线距离，单位有m(米),km(千米),mi(英里),ft(英尺)，不写默认为米
geodist <key> <member1> <member2> <单位>
#探索我附近的人，根据经纬度通过半径来查询集合中范围内的所有地理位置
#如果跟上参数withcoord返回就会带上经纬度，跟上参数withdist返回距离，跟上参数count限制返回数量
georadius <key> <经度> <纬度> <半径大小> <单位> [withcoord] [withdist] count <N>
#根据成员来探索附近的人
georadiusbymember <key> <member> <半径大小> <单位>
#将二维经纬度返回一个11字符串的Geohash字符串,也是返回距离，换了一种方式，用到的较少
geohash <key> <member1> <member2>
   ~~~

   geo命令底层的实现原理就是Zset！我们可以利用zset命令操作geo

   ~~~shell
#查看所有menber
zrange <key> 0 -1
#删除某个定位信息（关闭定位）
zrem <key> <member>
   ~~~

   

   ## Hyperloglog-统计基数

   > 简介

   Redis 2.8.9 版本更新了Hyperloglog数据结构！

   是用来做基数统计的方法

   > 什么是基数

   基数是一个集合中去重复值后的个数，比如：A{1,2,3,4,1,2,5,6},他的基数就是6，但这个数据大了后就可以接受误差

   >  应用场景

   **网页的uv（一个人访问网站多次，还是算一个人）**

   传统的方式，set保存用户id，然后就可以统计set中元素数量作为标准！

   这个方式保存了大量的用户id，就会消耗内存空间，我们的目的不是为了去保存用户id而是去计数！

   使用Hyperloglog的优点：占的内存是固定的，2^64不同的元素的基数，只需要费12kb内存！如果从内存角度比较的话，这个技术就是成为首选，但他有0.81%的错误率，但作为统计这些uv数据来说，或者统计时**我们允许容错的时候**这个技术就是我们的首选。

   他就三个命令

   ~~~shell
#添加一个或多个元素
pfadd <key> <element1> <element2>
#统计基数
pfcount <key>
#合并key1和key2中的元素放入key1中，或者放入一个新的key3中
pfmerge <key1> <key2>
pfmerge <key3> <key1> <key2>
   ~~~

   

   ## Bitmaps-位存储

   > 位存储

   是一种数据结构，操作二进制位进行记录，只有0和1两个状态,1字节=8bit

   > 常用的使用场景

   统计用户信息，活跃，不活跃！登录，未登录！365天打卡！两个状态的这种类似的都可以使用Bitmaps！

   比如打卡我们正常设计的话会在数据库中设置一个表，字段有：用户id，状态，打卡日期

   在使用Bitmap时365天=365bit  ，那一年下来一个用户就需要46个字节存储即可，一个字节可以计算完8天的打卡，省内存空间。

   > 测试

   插入命令

   ~~~shell
   setbit <key> <bit> <0或1>
   ~~~

   打卡一年就bit一直输到365即可，`setbit sign 365 1`

   比如一星期的打卡情况，1代表打卡，0代表未打卡

   ![image-20200404175748664](6.png)

   查看某一天是否打卡

   ![image-20200404175901284](7.png)

   统计打卡（他会统计所有位数为1的个数）

   ~~~shell
#无参数，统计所有位数
bitcount sign
#带start stop参数，start和stop参数是以字节为计数，比如4个字节编号就为[0-3]，一共有4*8=32位，可以打卡32天
bitcount sign <start> <stop>
#比如统计32天的打卡次数
bitcount sign 0 3
   
   ~~~

   

   # 事务

   Multi：开启事务，从输入这个命令开始，输入的命令依次会进入队列中，但不会执行

   Exec：执行事务，依次执行输入Multi后队列中的命令

   Discard：取消事务，可以中途取消组队

   在关系型数据库事务中：当其中一条事务出现问题，整个事务就会回滚

   而在redis中，当还在组队中出现报告（编译）错误（语法错误，单词错误），整个队列都将会被取消，不存在事务

   而当单条命令没有问题，即编译没问题的时候，输入Exec执行后，该成功的成功，该失败的失败，不会影响其他命令

   在事务开启之前可以对某个key进行监视（乐观锁机制）

   即watch <key>

   若在之后的事务执行之前有对key做出修改或删除，事务会直接取消exec返回（nil），监控一直持续到执行exec

   或者可以执行unwatch去解除监视

   # 原生Java连接redis

java连接linux下的redis（前提是把redis配置文件的bind注释掉，并protected-mode保护模式设置成no关闭掉）

原生Java连接

   1. 导入jar包：jedis-2.8.1.jar
   
   2. 主函数中：

   ![image-20200404221128593](8.png)

   

# springboot连接redis

说明：在SpringBoot2.x之后，原来使用的jedis被替换为了lettuce

jedis：采用直连，多个线程操作的话，是不安全的，有点类似于BIO模式（同步阻塞），如果要避免不安全就需要使用jedis pool连接池

lettuce：底层采用ety，实例可以在多个线程共享，不存在线程不安全的情况！可以减少线程数据！类似于NIO模式（同步非阻塞）。

   1. 先添加启动器依赖：

   ~~~xml
   <dependency>
   	<groupId>org.springframework.boot</groupId>
   	<artifactId>spring-boot-starter-data-redis</artifactId>
   </dependency>
   ~~~

   2. 在application.properties中修改配置文件

   ~~~yml
spring:
  redis:
    host: *.*.*.*(服务器的IP地址，如果redis在当前主机下，即可以使用localhost或127.0.0.1)
    port: 6379 (redis服务器端口号)
   ~~~

      3. 测试

   ~~~java
@SpringBootTest
class RedisSpringbootApplicationTests {
		//在SpringBoot中已经为我们准备了操作redis的工具模板，即Redistemplate
    @Autowired
    RedisTemplate redisTemplate;
    @Test
    void contextLoads() {
        redisTemplate.opsForValue().set("name","zhb");
        System.out.println(redisTemplate.opsForValue().get("name"));
        redisTemplate.opsForValue().set("hello","world");
        //操作list
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        redisTemplate.opsForList().leftPush("num","a",list);
    }
}

   ~~~

测试结果：(出现问题)

![image-20200405213114208](9.png)

**我们发现一个很奇怪的事，key的前面有一串字符的存在，那这是什么原因我们分析一波源码！**



## RedisTemplate源码分析

1. 在SprinBoot中，所有的配置类都会有一个类似的名字`XXXAutoConfiguration`,而所有的配置类都会绑定一个`properties`，我们可以看一下redis的工具类模板的源码，即RedisAutoConfiguratio

~~~java
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(RedisOperations.class)
//在这里就可以看到绑定的properties，我们可以配置的redis参数就可以从里面查看
@EnableConfigurationProperties(RedisProperties.class)
@Import({ LettuceConnectionConfiguration.class, JedisConnectionConfiguration.class })
public class RedisAutoConfiguration {

 
	@Bean 
  //ConditionalOnMissingBea:假如name参数的这个bean不存在，
  //那下面的bean就会生效，你说明我们可以自定义工具bean，而不使用下面的bean
	@ConditionalOnMissingBean(name = "redisTemplate")
	public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory)
			throws UnknownHostException {
		RedisTemplate<Object, Object> template = new RedisTemplate<>();
		template.setConnectionFactory(redisConnectionFactory);
		return template;
	}

  //因为String是最多使用的，专门写了一个string类型专用的template
	@Bean
	@ConditionalOnMissingBean
	public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory)
			throws UnknownHostException {
		StringRedisTemplate template = new StringRedisTemplate();
		template.setConnectionFactory(redisConnectionFactory);
		return template;
	}

}
~~~

2. 我们发现默认使用的是`RedisTemplate<Object, Object>`,默认传入的是两个对象，而我们正常一般都是设置key为String类型的，所以呆会我们自定义一个配置文件。我们先进入RedisTemplate看看：

   看到一个序列化的参数

![image-20200405202128887](10.png)

​	那我们往下翻看看他是怎么赋值的

![image-20200405202308693](11.png)

​	我们发现他默认的是jdk序列化，jdk序列化就会让我的字符串转义，所以才会在redis中出现一堆乱码那样的问题。

3. 在**RedisAutoConfiguratio**配置文件中，还有一个**StringRedisTemplate**，其实他是官方帮我们写好调用另外的一种序列化的方法，我们进入源码看看

![image-20200405211550232](12.png)

​	原来底层就是string类型的序列化方式

![image-20200405211409445](13.png)

4. 我们通过接口`RedisSerializer<T>`源码发现他有的序列化方式：

![image-20200405212002517](14.png)

5. 因为我们用的是RedisTemplate，那我们可以自定义一个配置来代替他的jdk序列化，比如采用json来序列化，上面RedisAutoConfiguratio源码已经告诉我们如果我们自己写了一个叫redisTemplate的bean，那么原先的就会失效，所以我们自己写一个config

~~~java
@Configuration
public class RedisConfig {

    @Bean
    @SuppressWarnings("all")
  //我们依旧用redisTemplate名字，使原先配置失效
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        //我们为了开发方便，直接采用String，Object
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setConnectionFactory(redisConnectionFactory);
        //配置具体的序列化方式
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om=new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        //String的序列化
        StringRedisSerializer stringRedisSerializer=new StringRedisSerializer();


        //key采用string
        template.setKeySerializer(stringRedisSerializer);
        //hash的key采用string
        template.setHashKeySerializer(stringRedisSerializer);
        //value采用jackson
        template.setValueSerializer(jackson2JsonRedisSerializer);
        //hash的value采用jackson
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();


        return template;
    }

}
~~~

6. 测试相同代码结果

   ![image-20200405222327998](15.png)

   发现结果已经正常了



 

# Redis发布与订阅

模型

![image-20200407112735289](16.png)



下表列出了 redis 发布订阅常用命令

| 序号 | 命令及描述                                                   |
| ---- | ------------------------------------------------------------ |
| 1    | PSUBSCRIBE pattern pattern ... 订阅一个或多个符合给定模式的频道。 |
| 2    | PUBSUB subcommand [argument [argument ...\]]  查看订阅与发布系统状态。 |
| 3    | PUBLISH channel message  将信息发送到指定的频道。            |
| 4    | PUNSUBSCRIBE [pattern [pattern ...\]]退订所有给定模式的频道。 |
| 5    | SUBSCRIBE channel [channel ...\] 订阅给定的一个或多个频道的信息。 |
| 6    | UNSUBSCRIBE [channel [channel ...\]] 指退订给定的频道。      |

例子

在我们订阅端订阅了一个频道后（步骤1），新建一个连接当做信息的发布者，去发布一条hello信息（步骤2），在我们不做任何的操作，订阅端自动弹出mession信息（步骤3）

![image-20200407113633737](17.png) 

> 原理

Redis是使用C实现的，通过分析Redis源码里的pubsub.c文件，了解发布和订阅底层的机制，Redis通过PUBLISH和SUBSCRIBE和PSUBSCRIBE等命令实现发布和订阅功能。

SUBSCRIBE订阅频道后，redis-server里维护了一个字典，字典的键就是一个个channel，而字典的值则是一个链表，链表中保存了所有订阅这个channel的客户端。

PUBLISH向订阅者发布信息，redis-server会使给定的频道作为键，在它维护的channel字典中查找记录了订阅这个频道的所有客户端的链表，遍历这个链表，将消息发布给所有订阅者。



# Redis的持久化

> RDB持久化

默认保存名字：`dump.rdb`

默认保存路径： `./` (当前服务启动路径下，及启动的redis.conf同级目录下)

rdb自动保存策略：

save 900 1       （900秒内只对数据更改了一次，执行一次rdb，保存到dump.rdb文件里）

save 300 10      （300秒修改了10次）

save 60   10000

当redis正常关闭（通过在客户端shutdown命令），也会自动持久化，当通过kill杀进程属于非正常关闭不会持久化

配置文件：`rdbcompression  yes` 一定要开启，将rdb文件进行压缩，不然文件会很大

> AOF持久化

默认保存名字：`appendonly.aof`

默认保存路径：与rdb目录同级

要想使用AOF持久化，首先去配置文件

appendonly no：默认关闭AOF持久化，开启改为yes即可 文件保存路径与rdb一致

**aof和rdb都开启，redis听aof的**

# 主从复制

> 查看服务器信息

通过命令`info replication`来查看当前服务器是从机还是主机，和一些其他的信息。

>  什么是主从复制？

一个主服务器 只负责写（也可读），多个从机只能读,这样就可以减轻服务器的压力

配置：

我们需要开启多个服务器，这里我们可以进行模拟，新建多个redis.conf文件进行重新命名：redis6379.conf（主），redis6380.conf（从），redis6381.conf（从）

原文件开启后台启动 daemonize yes

原文件需关闭aof持久化文件，因为aof持久化的文件特别大，如果都开启的话，会消耗大量的IO读取

> conf的配置文件需要写的内容：

~~~shell
#导入原文件配置：(这样很多配置都不用再写了)
include /opt/myRedis/redis.conf

#修改pid文件名字pidfile：(三个服务器根据端口号修改6379) 
pidfile /var/run/redis_6379.pid

#指定端口：依次修改端口号）
port 6379

#修改Dump.rdb文件：（依次修改6379）
dbfilename dump6379.rdb
~~~

（一般写上面4行即可）

> 配置文件设置好后启动：

通过命令：`info replication`打印出主从服务器的相关信息

**这时我们查看3台服务器都是主服务器**，role：master（角色为主服务），因为他们并没有关联

通过命令：`slaveof <ip> <port>`设置自己成为某个服务器的从服务器，

例如在redis6380服务器下输入：`slaveof 127.0.0.1 6379`即可成为端口号为6379服务器的从服务器，role：slave（角色为从服务器）

![image-20200407144643767](22.png)

当我们配置完后，从机就只能读,无法输入；主机可以读写，但读我们都会去从机读。当我们主机进行写入后，从机会自动与主机进行同步，进行信息的复制，保持信息的一致性。

> 服务宕机引发的效果

1. 从服务器shutdown后重新启动后又会变回主服务器，重新设置成为从服务器后，里面的数据就会立马更新，跟主服务器数据保持一致

2. 主服务器shutdown后，从机保持原地待命

3. 当主从服务器有多级别时，例如6379为主服务器，6380为从服务器，6381为6380的从服务器，当6379down机后可以在6380服务器输入命令1`SLAVEOF NO ONE`，来唤醒6380作为主服务器

> 永久配置主从关系

在真实的开发环境中我们都是进入配置文件进行永久的配置，比如进入redis6380.conf中

通过命令`/slaveof`查找设置，将`# slaveof <masterip> <masterport>`改为`slaveof 127.0.0.1 6379`，这样子每次启动后他永远是6379的从机，而不用每次重新启动都需要重新设置。

# 哨兵模式

自定义的/myRedis/目录下新建sentinel.conf文件

在配置文件中填写内容：

`sentinel monitor <自定义主服务名字> 127.0.0.1 6379 1`，最后一个1代表至少有几个哨兵认为主服务器down机了才算down机

最后在redis默认安装路径`usr/local/bin`下启动哨兵，

执行：`redis-sentinel  opt/myRedis/sentinel.conf`

可以通过在配置文件中slave-priority 100

 

#  布置集群

开启集群首先要注释掉：bind 127.0.0.1 和关掉保护：protected-mode no来实现其他主机能连接的上redis

redis集群实现了对redis的水平扩容，即启动N个redis节点，将整个数据库分存储在这N个节点中，每个节点存储总数据的1/N

创建集群步骤：

1. 安装集群的环境ruby

   yum install ruby

   yum install rubygems

2. 把集群配置文件redis-3.2.0.gem放到/opt目录下

   在opt目录下执行gem  

   在opt目录下执行gem install --local redis-3.2.0.gem

3. 制作6个实例，6379,6380,6381,6389,6390,6391

   创建多个相应的配置文件每个大致如下：

   ![image-20200407140447929](18.png)

4. 启动所有节点的服务再通过下面命令进行合体

   ![image-20200407140548808](19.png)

   一个集群至少三个主节点。

   --replicas 1 意思是希望每个集群中的每个主节点创建一个从节点

   至此集群配置完毕

5. 检验

   在进行合体时屏幕自动输出如下配置，同意即可，配置完成后用命令cluster nodes也可看到类似如下的数据

   ![image-20200407140632341](20.png)

   **6379,6380,6381做为主服务器，另外三个从服务器 ，赋予6379插槽0-5460，其他也进行插槽分配，一共有16384个插槽，就是0-16383**

   随便打开一个主服务器进行如下操作：（-c 自动实现重定向）

   ![image-20200407140726342](21.png)

   发现插入一个值后他会自动随机重定向到一个插槽为15495的插槽上，对应的是6381服务器





