---
categories: SpringCloud
tags: [分布式,微服务]
date: 2021-06-03 18:41:02
---

# 测试例子

在这里我们新建两个项目`consumer-server`和`provider-server`一个作为生产者，一个作为消费者。

<!--more-->

2. 在`provider-server`中写一个service接口和他的实例化对象

   ~~~java
   //这是dubbo包下的@Server，在项目启动后会自动注册到注册中心
   //虽然这个是在实现类上写注解，但是在zookeeper上会以接口来注册
   @Service
   @Component//使用了dubbo后尽量不要使用@Server注解，为了与dubbo区分
   public class TicketServiceImpl implements TicketService {
       @Override
       public String getTicket() {
           return "《hello zookeeper》";
       }
   }
   ~~~

3. 在`consumer-server`中写一个class

   ~~~java
   @Service//这里的@Service是spring下的注解，目的是放到bean容器中
   public class UserService {
   
       //想拿到provider的服务，要去注册中心拿到服务 
       // 要想要拿到什么服务，在实际开发中有两种方法  1.Pom坐标  2.定义路径相同的接口名（在相同路径写一个相同的接口）
     //这里采用第二种，写好接口后获取即可
       @Reference //使用引用注解，dubbo包下的注解 
       TicketService ticketService;
       
       
       //买票服务
       public void buyTicket(){
           String ticket =ticketService.getTicket();
           System.out.println("在注册中心拿到一张票"+ticket);
       }
       
   ~~~

   

4. 消费者和提供者都导入相同依赖，注意ZkClient要导入github下的包

   ~~~html
   <!--导入dubbo和zookeeper的依赖-->
           <!-- https://mvnrepository.com/artifact/org.apache.dubbo/dubbo-spring-boot-starter -->
   <dependency>
     <groupId>org.apache.dubbo</groupId>
     <artifactId>dubbo-spring-boot-starter</artifactId>
     <version>2.7.5</version>
   </dependency>
   <!--ZkClient-->
   <!-- https://mvnrepository.com/artifact/com.github.sgroschupf/zkclient -->
   <dependency>
     <groupId>com.github.sgroschupf</groupId>
     <artifactId>zkclient</artifactId>
     <version>0.1</version>
   </dependency>
   <!--导入zookeeper服务端和服务端依赖-->
   <dependency>
     <groupId>org.apache.curator</groupId>
     <artifactId>curator-framework</artifactId>
     <version>2.12.0</version>
   </dependency>
   <dependency>
     <groupId>org.apache.curator</groupId>
     <artifactId>curator-recipes</artifactId>
     <version>2.12.0</version>
   </dependency>
   <!--zookeeper会日志会冲突，需要排除slf4j-log4j12的日志包-->
   <dependency>
     <groupId>org.apache.zookeeper</groupId>
     <artifactId>zookeeper</artifactId>
     <version>3.4.14</version>
     <exclusions>
       <exclusion>
         <groupId>org.slf4j</groupId>
         <artifactId>slf4j-log4j12</artifactId>
       </exclusion>
     </exclusions>
   </dependency>
   ~~~

5. `provider-server`配置

   ~~~properties
   server.port=8081
   
   #服务应用的名字
   dubbo.application.name=provider-server
   #注册中心地址
   dubbo.registry.address=zookeeper://127.0.0.1:2181
   #哪些服务要被注册,把这个包下的服务都注册进去
   dubbo.scan.base-packages=com.zhb.service
   ~~~

6. `consumer-server`配置

   ~~~properties
   server.port=8082
   
   #消费者要去哪里拿需要暴露自己名字
   dubbo.application.name=consumer-server
   #注册中心的地址
   dubbo.registry.address=zookeeper://127.0.0.1:2181
   
   ~~~

7. 在消费者测试类中写测试代码

   ~~~java
   @Autowired
   UserService userService;
   
   @Test
   void contextLoads() {
   
     userService.buyTicket();
   }
   ~~~

   

8. 测试：先运行**zookeeper**的服务端，在运行**provider-server**这个项目，最后可以启动一下**dubbo-admin**登录localhost:7001查看即可。

9. 总结

   提供者需要提供自己服务的名字，去zookeeper注册一个地址，和报上自己提供的服务

   消费者则需要提供自己的名字和zookeeper的地址即可，至于用什么服务则在写一个类表示需要使用的服务即可

   