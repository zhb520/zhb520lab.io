---
categories: SpringCloud
tags: [分布式,微服务]
date: 2021-06-03 10:32:15
---

# 环境的配置

## zookeeper和dubbo的关系

1. Zookeeper 的作用：

Zookeeper用来注册服务和进行负载均衡，哪一个服务由哪一台机器来提供必需让调用者知道，简单来说就是IP地址和服务名称的对应关系。<!--more-->当然也可以通过硬编码的方式把这种对应关系在调用方业务代码中实现，但是如果提供服务的机器挂掉，调用者无法知晓，如果不更改代码会继续请求挂掉的机器提供服务。Zookeeper通过心跳机制可以检测挂掉的机器的IP和服务对应关系从列表关系中删除。至于支持高并发，简单来说就是横向扩展，在不更改代码的情况通过增加机器数量提高运算能力。通过添加新的机器向Zookeeper注册服务，服务的提供者多了能服务的客户就多了。

2. dubbo：

是管理中间层的工具，在业务层到数据仓库间有非常多服务的接入和提供者需要调度，dubbo提供一个框架解决这些问题。注意这里的dubbo只是一个框架，至于你在框架上放什么东西，完全取决于你，就像一个汽车骨架，你需要配你的轮子引擎。这个框架中要完成调度必须需要一个分布式的注册中心，存储所有服务的元数据，你可以用Zookeeper，你也可以用别的。

3. dubbo和Zookeeper的关系

dubbo将注册中心进行抽象，使得它可以外接不同的存储媒介给注册中心提供服务，有Zookeeper，memcached，redis等。

引入了Zookeeper作为存储媒介，也就把Zookeeper的特性引进来了。首先是负载均衡，但注册中心的承载能力是有限的，在流量达到一定程度的时候要进行分流，负载均衡就是为分流而生的，一个Zookeeper群配合相应的web应用就可以很容易达到负载均衡；资源同步，单单有负载均衡还不够，节点之间的数据和资源需要同步，Zookeeper集群就天然具备有这样的功能；命名服务，将树状结构用于维护全局的服务地址列表，服务提供者在启动的时候，向Zookeeper上指定的节点/dubbo/${serviceName}/providers目录下写入自己的url地址，这个操作就完成了服务的发布。其他特性还有master选举，分布式锁等。

## zookeeper

1. 下载zookeeper

   [下载地址](http://mirror.bit.edu.cn/apache/zookeeper/zookeeper-3.4.14/)

2. 解压缩后在`conf`目录下复制`zoo_sample.cfg`,重命名为`zoo.cfg`

3. 进入解压目录(`zookeeper-3.4.14`)，执行以下命令：

   ```shell
   ## 启动ZooKeeper    .sh文件必须加./才能找的到
   ./bin/zkServer.sh start
   ## 停止ZooKeeper
   ./bin/zkServer.sh stop
   ```

4. zookeeper的端口号：2181

5. 测试

   ~~~shell
   cd zookeeper-3.4.14/bin
   ./zkCli.sh -server 127.0.0.1:2181
   
   /////////////////////官方测试命令////////////////////////
    
   [zk: 127.0.0.1:2181(CONNECTED) 2] ls /
   [zookeeper]
   [zk: 127.0.0.1:2181(CONNECTED) 3] create /zk_test my_data#创建节点
   Created /zk_test
   [zk: 127.0.0.1:2181(CONNECTED) 4] ls /
   [zookeeper, zk_test]
   [zk: 127.0.0.1:2181(CONNECTED) 5] get /zk_test#获取该节点的值
   my_data
   [zk: 127.0.0.1:2181(CONNECTED) 6] set /zk_test junk#插入值
   [zk: 127.0.0.1:2181(CONNECTED) 7] get /zk_test
   junk
   [zk: 127.0.0.1:2181(CONNECTED) 8] delete /zk_test#删除节点
   [zk: 127.0.0.1:2181(CONNECTED) 9] ls
   [zk: 127.0.0.1:2181(CONNECTED) 10] ls /
   [zookeeper]
   ~~~





## Dubbo-admin

1. 下载Dubbo-admin（这是一个后台管理网页），选择Branch:master版本

   [下载地址](https://github.com/apache/dubbo-admin/tree/master)

2. 解压缩后是一个叫`dubbo-admin-master`的文件，进入resources目录下就可以发现这是一个springboot项目，进入配置文件可以查看一些默认配置，端口号是7001，注册中心的地址就是zookeeper的默认端口号2181

   ![image-20200303125508919](1.png)

3. 进入`dubbo-admin-master`目录执行以下命令进行打包

   ~~~shell
   mvn clean package -Dmaven.test.skip=true #后面的指令代表不走测试
   ~~~

4. 打包完成后，在`dubbo-admin`文件夹下一个`target`目录就有打包的内容，一个jar包,用`java -jar`命令开启zookeeper之后就可以对其进行测试  **control+c停止**

   ![image-20200303134958215](2.png)

5. 进入`localhost:7001/`需要登录，默认账号密码root--root即可进入主页

   ![image-20200303135309621](3.png)

   ![image-20200303135554474](4.png)

6. zookeeper是一个注册中心（核心），dubbo-admin是一个监控管理后台，可以查看我么注册了哪些服务，哪些服务被消费了（可以不需要）

   